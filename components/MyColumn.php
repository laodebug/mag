<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 9/18/18
 * Time: 6:43 PM
 */

namespace app\components;

use Yii;
use yii\grid\ActionColumn;
use yii\grid\SerialColumn;
use yii\helpers\Html;

class MyColumn extends ActionColumn
{
    protected function initDefaultButtons()
    {
        $this->initDefaultButton('view', 'eye');
        $this->initDefaultButton('update', 'pencil');
        $this->initDefaultButton('delete', 'trash', [
            'data-confirm' => Yii::t('yii', 'ທ່ານຕ້ອງການລຶບແທ້ບໍ?'),
            'data-method' => 'post',
        ]);
    }

    protected function initDefaultButton($name, $iconName, $additionalOptions = [])
    {
        if (!isset($this->buttons[$name]) && strpos($this->template, '{' . $name . '}') !== false) {
            $class = 'default';
            $this->buttons[$name] = function ($url, $model, $key) use ($name, $iconName, $additionalOptions, $class) {
                switch ($name) {
                    case 'view':
                        $title = 'ເບິ່ງຂໍ້ມູນ';
                        $class = 'primary';
                        break;
                    case 'update':
                        $title = 'ແກ້ໄຂຂໍ້ມູນ';
                        $class = 'warning';
                        break;
                    case 'delete':
                        $title = 'ລຶບຂໍ້ມູນ';
                        $class = 'danger';
                        break;
                    default:
                        $title = ucfirst($name);
                }
                $options = array_merge([
                    'class' => "btn btn-$class text-right",
                    'title' => $title,
                    'aria-label' => $title,
                    'data-toggle' => "tooltip",
                    'data-placement' => "top",
                    'data-pjax' => '0',
                ], $additionalOptions, $this->buttonOptions);
                $icon = Html::tag('span', '', ['class' => "fa fa-$iconName"]);
                return Html::a($icon, $url, $options);
            };
        }
    }

    public static function action($view = true, $update = true, $delete = true)
    {
        $template = '';
        if ($view) $template .= '{view}';
        if ($update) $template .= '{update}';
        if ($delete) $template .= '{delete}';
        return [
            'class' => MyColumn::className(),
            'template' => $template,
            'options' => [
                'style' => 'width: 140px'
            ]
        ];
    }

    public static function serial()
    {
        return [
            'class' => SerialColumn::className(),
            'options' => [
                'style' => 'width: 20px'
            ]
        ];
    }
}