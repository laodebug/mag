<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 10/15/18
 * Time: 10:42 PM
 */

namespace app\components;


class MyColor
{
    public static function day($durations, $value)
    {
        foreach ($durations as $i => $duration)
            if ($duration->min <= $value && $value <= $duration->max)
                return '<div class="color" style="background-color: '
                    . $duration->color . '">' . $value . ' (' . $duration->remark . ')</div>';

    }

    public static function s($statuses, $value)
    {
        foreach ($statuses as $i => $status)
            if ($status->id == $value)
                return '<div class="color" style="background-color: ' . $status->color . '">' . $status->name . '</div>';

    }
}