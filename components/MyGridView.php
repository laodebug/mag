<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 9/18/18
 * Time: 7:29 PM
 */

namespace app\components;


use yii\grid\GridView;
use function array_merge;

class MyGridView
{
    public static function rNoAdd($dataProvider, $searchModel, $columns, $view = 1, $update = 1, $delete = 1, $labels = null)
    {
        return self::r($dataProvider, $searchModel, $columns, $view, $update, $delete, 0, $labels);
    }

    public static function r($dataProvider, $searchModel, $columns, $view = 1, $update = 1, $delete = 1, $add = true, $labels = null)
    {
        foreach ($columns as $c => $column) {
            if (isset($column['attribute']) && !isset($column['label'])) {
                if (isset($labels)) {
                    $columns[$c]['label'] = $labels[$column['attribute']];
                } else {
                    $label = $searchModel->attributeLabels()[$column['attribute']];
                    $columns[$c]['label'] = $label;
                }
                if (isset($column['filter']))
                    $columns[$c]['filterInputOptions'] = [
                        'class' => 'form-control',
                        'prompt' => 'ທັງໝົດ'
                    ];
                else {
                    if (isset($columns[$c]['filterInputOptions']))
                        $columns[$c]['filterInputOptions'] = array_merge($columns[$c]['filterInputOptions'], [
                            'placeholder' => 'ຊອກຫາ'
                        ]);
                    else
                        $columns[$c]['filterInputOptions'] = [
                            'class' => 'form-control',
                            'placeholder' => 'ຊອກຫາ'
                        ];
                }
            }
        }

        $html = '<div class="box">' .
            '<div class="box-body">';
        if ($add)
            $html .= MyButton::add();
        $html .= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'headerRowOptions' => [
                'style' => 'background-color: #cccccc'
            ],
            'emptyText' => 'ບໍ່ມີຂໍ້ມູນ',
            'summary' => "ສະແດງ {begin} ຫາ {end} ຈາກທັງໝົດ {totalCount}",
            'columns' => array_merge([MyColumn::serial()], $columns, [MyColumn::action($view, $update, $delete)])
        ]);
        $html .= '</div >' .
            '</div >';
        return $html;
    }
}