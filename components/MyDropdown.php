<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 9/18/18
 * Time: 7:24 PM
 */

namespace app\components;


use yii\helpers\ArrayHelper;
use function array_merge;
use function str_replace;
use function ucwords;

class MyDropdown
{
    public static function r2($form, $model, $attribute, $list, $id = 'id', $name = 'name')
    {
        $options = ['class' => 'form-control select2'];
        return self::r($form, $model, $attribute, $list, $id, $name, $options);
    }

    public static function r($form, $model, $attribute, $list, $id = 'id', $name = 'name', $options = [])
    {
        $label = str_replace('_id', '', $attribute);
        $label = preg_replace("/[^A-Za-z0-9 ]/", ' ', $label);
        $options = array_merge($options, [
            'class' => 'form-control input-lg',
            'prompt' => 'ກະລຸນາເລືອກ'
        ]);
        return $form->field($model, $attribute)
            ->dropDownList(ArrayHelper::map($list, $id, $name), $options)
            ->label(ucwords($label));
    }
}