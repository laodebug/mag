<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 9/18/18
 * Time: 11:18 PM
 */

namespace app\components;


use Yii;
use function array_merge;

class MyTextInput
{
    public static function rMap($form, $model, $attribute, $onclick = "alert('ok')", $class = 'danger')
    {
        $fieldparam = [
            'template' => '{label} 
              <div class="input-group">
                <div class="input-group-btn">
                  <button type="button" class="btn btn-' . $class . '" onclick="' . $onclick . '" data-id="' . $attribute . '">
                  <i class="fa fa-map-marker"></i>
                  </button>
                </div>
                {input}
              </div>'
        ];
        return self::r($form, $model, $attribute, false, true, [], $fieldparam);
    }

    public static function r($form, $model, $attribute, $autofocus = false, $maxlength = true, $options = [], $fieldparam = [])
    {
        $options = array_merge($options, [
            'class' => isset($options['class']) ? $options['class'] : 'form-control input-lg',
            'maxlength' => $maxlength,
            'autofocus' => $autofocus,
            'placeholder' => $model->attributeLabels()[$attribute]
        ]);
        return $form->field($model, $attribute, $fieldparam)
            ->textInput($options)
            ->label($model->attributeLabels()[$attribute]);
    }

    public static function number($form, $model, $attribute, $options = [])
    {
        $label = preg_replace("/[^A-Za-z0-9]/", ' ', $attribute);
        $options = [
            'type' => 'number',
            'placeholder' => Yii::t('app', ucwords($label))
        ];

        return self::r($form, $model, $attribute, 0, 1, $options);
    }
}