<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 9/19/18
 * Time: 10:08 PM
 */

namespace app\components;


use Yii;

class MyDatePicker
{
    public static function r($form, $model, $attribute)
    {
        $label = preg_replace("/[^A-Za-z0-9 ]/", ' ', $attribute);
        $options = ['class' => 'form-control datepicker input-lg', 'placeholder' => Yii::t('app', ucwords($label))];
        return MyTextInput::r($form, $model, $attribute, 0, 0, $options);
    }

    public static function time($form, $model, $attribute)
    {
        $label = preg_replace("/[^A-Za-z0-9 ]/", ' ', $attribute);
        $options = ['class' => 'form-control datetimepicker', 'placeholder' => Yii::t('app', ucwords($label))];
        return MyTextInput::r($form, $model, $attribute, 0, 0, $options);
    }
}