<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 9/18/18
 * Time: 7:42 PM
 */

namespace app\components;


use function array_merge;

class MyForm
{
    public static function rNoBox($viewclass, $model, $name = '_form', $params = null)
    {
        return self::r($viewclass, $model, $name, $params, 0);
    }

    public static function r($viewclass, $model, $name = '_form', $params = null, $box = 1)
    {
        $html = '';
        if ($box) $html .= '<div class="box">' .
            '<div class="box-body">';

        $p = ['model' => $model];
        if (isset($params))
            $p = array_merge($p, $params);
        $html .= $viewclass->render($name, $p);

        if ($box) $html .= '</div>' .
            '</div>';

        return $html;
    }

    public static function rParams($viewclass, $model, $params, $box = 1)
    {
        return self::r($viewclass, $model, '_form', $params, $box);
    }
}