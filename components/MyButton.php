<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 9/18/18
 * Time: 7:13 PM
 */

namespace app\components;


use yii\helpers\Html;

class MyButton
{
    public static function add()
    {
        return '<p class="pull-right">' .
            Html::a('<i class="fa fa-plus"></i>', ['create'], [
                'class' => 'btn btn-info btn-lg',
                'data-toggle' => "tooltip",
                'data-placement' => "top",
                'title' => "ເພີ່ມ"
            ]) .
            '</p>';
    }
}