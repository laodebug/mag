<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 9/18/18
 * Time: 9:59 PM
 */

namespace app\components;


use Yii;
use yii\helpers\Html;
use yii\widgets\DetailView;

class MyView
{
    public static function detail($model, $columns, $box = true, $password = false)
    {
        foreach ($columns as $c => $column)
            if (isset($column['attribute']) && !isset($column['label'])) {
                $label = str_replace('_id', '', $column['attribute']);
                $label = preg_replace("/[^A-Za-z0-9 ]/", ' ', $label);
                $columns[$c]['label'] = ucwords($label);
            }
        $output = "";
        if ($box)
            $output .= '<div class="box">' .
                '<div class="box-office">' .
                '<p class="pull-right">';
        if ($password)
            $output .= Html::a('<i class="fa fa-key"></i> ' . Yii::t('app', 'Change Password'), ['user/password', 'id' => $model->id], ['class' => 'btn btn-warning']) . ' ';
        $output .=
            Html::a('<i class="fa fa-pencil"></i> ' . Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) .
            ' ' .
            Html::a('<i class="fa fa-trash"></i> ' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) .
            '</p>';

        $output .= DetailView::widget([
            'model' => $model,
            'attributes' => $columns,
        ]);
        if ($box)
            $output .= '</div>' .
                '</div>';

        return $output;
    }
}