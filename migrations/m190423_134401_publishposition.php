<?php

use yii\db\Migration;

/**
 * Class m190423_134401_publishposition
 */
class m190423_134401_publishposition extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `mag`.`publish` 
CHANGE COLUMN `last_update` `last_update` DATETIME NOT NULL DEFAULT current_timestamp ;

ALTER TABLE `mag`.`publish_detail` 
ADD COLUMN `position` INT(11) NULL DEFAULT NULL AFTER `publish_id`;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190423_134401_publishposition cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190423_134401_publishposition cannot be reverted.\n";

        return false;
    }
    */
}
