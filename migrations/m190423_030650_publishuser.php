<?php

use yii\db\Migration;

/**
 * Class m190423_030650_publishuser
 */
class m190423_030650_publishuser extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "-- MySQL Workbench Synchronization
-- Generated: 2019-04-23 10:01
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Adsavin

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `mag`.`publish` 
DROP FOREIGN KEY `fk_publish_employee1`;

ALTER TABLE `mag`.`publish` 
DROP COLUMN `employee_id`,
CHANGE COLUMN `last_update` `last_update` DATETIME NOT NULL DEFAULT current_timestamp ,
ADD COLUMN `user_id` INT(11) NOT NULL AFTER `last_update`,
ADD INDEX `fk_publish_user1_idx` (`user_id` ASC),
DROP INDEX `fk_publish_employee1_idx` ;

ALTER TABLE `mag`.`publish` 
ADD CONSTRAINT `fk_publish_user1`
  FOREIGN KEY (`user_id`)
  REFERENCES `mag`.`user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190423_030650_publishuser cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190423_030650_publishuser cannot be reverted.\n";

        return false;
    }
    */
}
