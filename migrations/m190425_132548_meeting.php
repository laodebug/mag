<?php

use yii\db\Migration;

/**
 * Class m190425_132548_meeting
 */
class m190425_132548_meeting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "-- MySQL Workbench Synchronization
-- Generated: 2019-04-25 19:52
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Adsavin

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `mag`.`publish` 
CHANGE COLUMN `last_update` `last_update` DATETIME NOT NULL DEFAULT current_timestamp ;

CREATE TABLE IF NOT EXISTS `mag`.`meeting` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `created_date` DATETIME NOT NULL,
  `meeting_date` DATETIME NOT NULL,
  `remark` TEXT NULL DEFAULT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_meeting_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_meeting_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `mag`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `mag`.`student` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(255) NOT NULL,
  `lastname` VARCHAR(255) NULL DEFAULT NULL,
  `dob` DATETIME NULL DEFAULT NULL,
  `tel` VARCHAR(255) NULL DEFAULT NULL,
  `code` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `mag`.`participants` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `checkin` DATETIME NOT NULL,
  `checkout` DATETIME NULL DEFAULT NULL,
  `meeting_id` INT(11) NOT NULL,
  `student_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_participants_meeting1_idx` (`meeting_id` ASC),
  INDEX `fk_participants_student1_idx` (`student_id` ASC),
  CONSTRAINT `fk_participants_meeting1`
    FOREIGN KEY (`meeting_id`)
    REFERENCES `mag`.`meeting` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_participants_student1`
    FOREIGN KEY (`student_id`)
    REFERENCES `mag`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190425_132548_meeting cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190425_132548_meeting cannot be reverted.\n";

        return false;
    }
    */
}
