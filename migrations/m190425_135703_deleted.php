<?php

use yii\db\Migration;

/**
 * Class m190425_135703_deleted
 */
class m190425_135703_deleted extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `mag`.`publish` 
CHANGE COLUMN `last_update` `last_update` DATETIME NOT NULL DEFAULT current_timestamp ;

ALTER TABLE `mag`.`meeting` 
ADD COLUMN `deleted` INT(11) NOT NULL AFTER `user_id`,
ADD INDEX `fk_meeting_user1_idx` (`user_id` ASC),
DROP INDEX `fk_meeting_user1_idx` ;

ALTER TABLE `mag`.`student` 
ADD COLUMN `deleted` INT(11) NOT NULL DEFAULT 0 AFTER `code`;

ALTER TABLE `mag`.`participants` 
ADD COLUMN `deleted` INT(11) NOT NULL DEFAULT 0 AFTER `student_id`,
ADD INDEX `fk_participants_meeting1_idx` (`meeting_id` ASC),
ADD INDEX `fk_participants_student1_idx` (`student_id` ASC),
DROP INDEX `fk_participants_student1_idx` ,
DROP INDEX `fk_participants_meeting1_idx` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190425_135703_deleted cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190425_135703_deleted cannot be reverted.\n";

        return false;
    }
    */
}
