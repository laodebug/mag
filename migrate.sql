-- MySQL Workbench Synchronization
-- Generated: 2021-04-04 13:51
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Adsavin

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER SCHEMA `mag`  DEFAULT CHARACTER SET utf8  DEFAULT COLLATE utf8_general_ci ;

CREATE TABLE IF NOT EXISTS `mag`.`chapter` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(500) NOT NULL,
  `content` TEXT NOT NULL,
  `chapter_author_id` INT(11) NOT NULL,
  `category_id` INT(11) NULL DEFAULT NULL,
  `user_id` INT(11) NOT NULL,
  `deleted` TINYINT(1) NOT NULL DEFAULT 0,
  `hidden` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_chapter_chapter_author1_idx` (`chapter_author_id` ASC),
  INDEX `fk_chapter_category1_idx` (`category_id` ASC),
  INDEX `fk_chapter_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_chapter_chapter_author1`
    FOREIGN KEY (`chapter_author_id`)
    REFERENCES `mag`.`chapter_author` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_chapter_category1`
    FOREIGN KEY (`category_id`)
    REFERENCES `mag`.`category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_chapter_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `mag`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `mag`.`chapter_author` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(500) NOT NULL,
  `last_name` VARCHAR(500) NOT NULL,
  `section_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_chapter_author_section1_idx` (`section_id` ASC),
  CONSTRAINT `fk_chapter_author_section1`
    FOREIGN KEY (`section_id`)
    REFERENCES `mag`.`section` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `mag`.`magazine` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(500) NOT NULL,
  `content` TEXT NOT NULL,
  `cover_image` VARCHAR(500) NOT NULL,
  `index_image` VARCHAR(500) NOT NULL,
  `content_file` VARCHAR(500) NULL DEFAULT NULL,
  `for_month` INT(11) NOT NULL,
  `for_year` INT(11) NOT NULL,
  `deleted` TINYINT(1) NOT NULL,
  `hidden` TINYINT(1) NOT NULL,
  `created_date` DATETIME NOT NULL DEFAULT current_timestamp,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_magazine_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_magazine_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `mag`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `mag`.`chapter` 
DROP COLUMN `chapter_author_id`,
ADD COLUMN `author_id` INT(11) NOT NULL AFTER `hidden`,
ADD INDEX `fk_chapter_category1_idx` (`category_id` ASC),
ADD INDEX `fk_chapter_user1_idx` (`user_id` ASC),
ADD INDEX `fk_chapter_author1_idx` (`author_id` ASC),
DROP INDEX `fk_chapter_user1_idx` ,
DROP INDEX `fk_chapter_category1_idx` ,
DROP INDEX `fk_chapter_chapter_author1_idx` ;
;

CREATE TABLE IF NOT EXISTS `mag`.`author` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(500) NOT NULL,
  `last_name` VARCHAR(500) NOT NULL,
  `section_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_chapter_author_section1_idx` (`section_id` ASC),
  CONSTRAINT `fk_chapter_author_section1`
    FOREIGN KEY (`section_id`)
    REFERENCES `mag`.`section` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `mag`.`magazine` 
ADD COLUMN `author_id` INT(11) NOT NULL AFTER `user_id`,
CHANGE COLUMN `created_date` `created_date` DATETIME NOT NULL DEFAULT current_timestamp ,
ADD INDEX `fk_magazine_user1_idx` (`user_id` ASC),
ADD INDEX `fk_magazine_author1_idx` (`author_id` ASC),
DROP INDEX `fk_magazine_user1_idx` ;
;

DROP TABLE IF EXISTS `mag`.`chapter_author` ;

ALTER TABLE `mag`.`chapter` 
ADD CONSTRAINT `fk_chapter_author1`
  FOREIGN KEY (`author_id`)
  REFERENCES `mag`.`author` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `mag`.`magazine` 
ADD CONSTRAINT `fk_magazine_author1`
  FOREIGN KEY (`author_id`)
  REFERENCES `mag`.`author` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `mag`.`magazine` 
DROP FOREIGN KEY `fk_magazine_author1`;

ALTER TABLE `mag`.`chapter` 
ADD INDEX `fk_chapter_category1_idx` (`category_id` ASC) VISIBLE,
ADD INDEX `fk_chapter_user1_idx` (`user_id` ASC) VISIBLE,
ADD INDEX `fk_chapter_author1_idx` (`author_id` ASC) VISIBLE,
DROP INDEX `fk_chapter_author1_idx` ,
DROP INDEX `fk_chapter_user1_idx` ,
DROP INDEX `fk_chapter_category1_idx` ;
;

ALTER TABLE `mag`.`author` 
ADD INDEX `fk_chapter_author_section1_idx` (`section_id` ASC) VISIBLE,
DROP INDEX `fk_chapter_author_section1_idx` ;
;

ALTER TABLE `mag`.`magazine` 
CHANGE COLUMN `created_date` `created_date` DATETIME NOT NULL DEFAULT current_timestamp ,
CHANGE COLUMN `author_id` `author_id` INT(11) NULL DEFAULT NULL ,
ADD INDEX `fk_magazine_user1_idx` (`user_id` ASC) VISIBLE,
ADD INDEX `fk_magazine_author1_idx` (`author_id` ASC) VISIBLE,
DROP INDEX `fk_magazine_author1_idx` ,
DROP INDEX `fk_magazine_user1_idx` ;
;

ALTER TABLE `mag`.`magazine` 
ADD CONSTRAINT `fk_magazine_author1`
  FOREIGN KEY (`author_id`)
  REFERENCES `mag`.`author` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
