<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 9/29/18
 * Time: 1:26 PM
 */

/*CREATE SCHEMA `mag` DEFAULT CHARACTER SET utf8 ;

-- MySQL Workbench Synchronization
-- Generated: 2018-09-29 13:31
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Adsavin

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `mag`.`essay` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `barcode` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `filename` VARCHAR(255) NOT NULL,
  `source` VARCHAR(255) NOT NULL,
  `tel` VARCHAR(255) NOT NULL,
  `created_date` DATETIME NOT NULL,
  `received_date` DATETIME NOT NULL,
  `deleted` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `barcode_UNIQUE` (`barcode` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `mag`.`history` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `created_date` DATETIME NOT NULL,
  `essay_id` INT(11) NOT NULL,
  `deleted` INT(11) NOT NULL DEFAULT 0,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_history_essay_idx` (`essay_id` ASC),
  INDEX `fk_history_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_history_essay`
    FOREIGN KEY (`essay_id`)
    REFERENCES `mag`.`essay` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_history_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `mag`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `mag`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `level_id` INT(11) NOT NULL,
  `deleted` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  INDEX `fk_user_level1_idx` (`level_id` ASC),
  CONSTRAINT `fk_user_level1`
    FOREIGN KEY (`level_id`)
    REFERENCES `mag`.`level` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `mag`.`level` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `deleted` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `mag`.`setting` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(255) NOT NULL,
  `value` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


*/


/*
-- MySQL Workbench Synchronization
-- Generated: 2018-09-29 13:40
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Adsavin

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `mag`.`essay`
ADD COLUMN `user_id` INT(11) NOT NULL AFTER `deleted`,
ADD COLUMN `level_id` INT(11) NOT NULL AFTER `user_id`,
ADD INDEX `fk_essay_user1_idx` (`user_id` ASC),
ADD INDEX `fk_essay_level1_idx` (`level_id` ASC);

ALTER TABLE `mag`.`history`
ADD COLUMN `level_id` INT(11) NOT NULL AFTER `user_id`,
ADD INDEX `fk_history_level1_idx` (`level_id` ASC);

ALTER TABLE `mag`.`essay`
ADD CONSTRAINT `fk_essay_user1`
  FOREIGN KEY (`user_id`)
  REFERENCES `mag`.`user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_essay_level1`
  FOREIGN KEY (`level_id`)
  REFERENCES `mag`.`level` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `mag`.`history`
ADD CONSTRAINT `fk_history_level1`
  FOREIGN KEY (`level_id`)
  REFERENCES `mag`.`level` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

 */