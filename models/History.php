<?php

namespace app\models;

use yii\web\UploadedFile;

/**
 * This is the model class for table "history".
 *
 * @property int $id
 * @property string $created_date
 * @property int $essay_id
 * @property int $deleted
 * @property int $user_id
 * @property int $employee_id
 * @property string $comment
 * @property string $file_name
 * @property string $return_receiver
 * @property int $status_id
 * @property string $sr s: send r: receive
 *
 * @property Employee $employee
 * @property Essay $essay
 * @property Status $status
 * @property User $user
 * @property Photo[] $photos
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * @var UploadedFile file attribute
     */
    public $file;
    public $direction;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * {@inheritdoc}
     * @return HistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new HistoryQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_date', 'essay_id', 'user_id', 'status_id', 'sr'], 'required'],
            [['created_date'], 'safe'],
            [['essay_id', 'deleted', 'user_id', 'employee_id', 'status_id'], 'integer'],
            [['comment'], 'string'],
            [['file_name', 'return_receiver'], 'string', 'max' => 255],
            [['sr'], 'string', 'max' => 1],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['essay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::className(), 'targetAttribute' => ['essay_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_date' => 'ວັນທີ',
            'essay_id' => 'ບົດຄວາມ',
            'deleted' => 'Deleted',
            'user_id' => 'ຜູ້ບັນທຶກ',
            'level_id' => 'ຂັ້ນ',
            'employee_id' => 'ບກ',
            'comment' => 'ຄຳຄິດເຫັນ',
            'file_name' => 'ຊື່ຟາຍ',
            'return_receiver' => 'ຊື່ຜູ້ຮັບບົດຄືນ',
            'status_id' => 'ສະຖານະ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEssay()
    {
        return $this->hasOne(Essay::className(), ['id' => 'essay_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['history_id' => 'id']);
    }
}
