<?php

namespace app\models;

/**
 * This is the model class for table "source".
 *
 * @property int $id
 * @property string $name
 * @property string $job
 */
class Source extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'source';
    }

    /**
     * {@inheritdoc}
     * @return SourceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SourceQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'job'], 'required'],
            [['name', 'job'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'job' => 'Job',
        ];
    }
}
