<?php

namespace app\models;

/**
 * This is the model class for table "student".
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $dob
 * @property string $tel
 * @property string $code
 * @property string $photo
 * @property int $deleted
 *
 * @property Participants[] $participants
 */
class Student extends \yii\db\ActiveRecord
{
    public $uploader;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * {@inheritdoc}
     * @return StudentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StudentQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname', 'code'], 'required'],
            [['dob'], 'safe'],
            [['deleted'], 'integer'],
            [['uploader'], 'file'],
            [['firstname', 'lastname', 'tel', 'code', 'photo'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'ຊື່ແທ້',
            'lastname' => 'ນາມສະກຸນ',
            'dob' => 'ວັນເດືອນປີເກີດ',
            'tel' => 'ໂທ',
            'code' => 'ລະຫັດ',
            'photo' => 'ຮູບ',
            'deleted' => 'ລຶບ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipants()
    {
        return $this->hasMany(Participants::className(), ['student_id' => 'id']);
    }
}
