<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Duration]].
 *
 * @see Duration
 */
class DurationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Duration[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Duration|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
