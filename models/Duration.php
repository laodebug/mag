<?php

namespace app\models;

/**
 * This is the model class for table "duration".
 *
 * @property int $id
 * @property int $min
 * @property int $max
 * @property string $remark
 * @property string $color
 */
class Duration extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'duration';
    }

    /**
     * {@inheritdoc}
     * @return DurationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DurationQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['min', 'max'], 'required', 'message' => 'ກະລຸນາປ້ອນ {attribute}'],
            [['min', 'max'], 'integer', 'message' => 'ກະລຸນາປ້ອນ {attribute} ເປັນຕົວເລກ'],
            [['remark', 'color'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'min' => 'ເລີ່ມ',
            'max' => 'ເຖິງ',
            'remark' => 'ເວລາ',
            'color' => 'ສີ',
        ];
    }
}
