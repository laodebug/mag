<?php

namespace app\models;

/**
 * This is the model class for table "participants".
 *
 * @property int $id
 * @property string $checkin
 * @property string $checkout
 * @property int $meeting_id
 * @property int $student_id
 * @property int $deleted
 *
 * @property Meeting $meeting
 * @property Student $student
 */
class Participants extends \yii\db\ActiveRecord
{
    public $code;
    public $firstname;
    public $lastname;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'participants';
    }

    /**
     * {@inheritdoc}
     * @return ParticipantsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ParticipantsQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['checkin', 'meeting_id', 'student_id'], 'required'],
            [['checkin', 'checkout'], 'safe'],
            [['meeting_id', 'student_id', 'deleted'], 'integer'],
            [['meeting_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meeting::className(), 'targetAttribute' => ['meeting_id' => 'id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'checkin' => 'ເວລາເຂົ້າ',
            'checkout' => 'ເວລາອອກ',
            'meeting_id' => 'ກອງປະຊຸມ',
            'student_id' => 'ນັກຮຽນ',
            'deleted' => 'ລຶບ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeeting()
    {
        return $this->hasOne(Meeting::className(), ['id' => 'meeting_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_id']);
    }
}
