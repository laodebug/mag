<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "chapter".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property int|null $category_id
 * @property int $user_id
 * @property int $deleted
 * @property int $hidden
 *
 * @property Author $author
 * @property Category $category
 * @property User $user
 */
class Chapter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chapter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content', 'user_id'], 'required'],
            [['content'], 'string'],
            [['category_id', 'user_id', 'deleted', 'hidden'], 'integer'],
            [['title', 'author'], 'string', 'max' => 500],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'ຫົວຂໍ້',
            'content' => 'ເນື້ອໃນ',
            'category_id' => 'ປະເພດ',
            'user_id' => 'ຜູ້ໃຊ້',
            'deleted' => 'ລຶບ',
            'hidden' => 'ສະແດງ',
            'author' => 'ຜູ້ຂຽນ',
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
