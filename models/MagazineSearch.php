<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Magazine;

/**
 * MagazineSearch represents the model behind the search form of `app\models\Magazine`.
 */
class MagazineSearch extends Magazine
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'for_month', 'for_year', 'deleted', 'hidden', 'user_id'], 'integer'],
            [['title', 'content', 'cover_image', 'index_image', 'content_file', 'created_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Magazine::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'for_month' => $this->for_month,
            'for_year' => $this->for_year,
            'deleted' => $this->deleted,
            'hidden' => $this->hidden,
            'created_date' => $this->created_date,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'subtitle', $this->content])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'cover_image', $this->cover_image])
            ->andFilterWhere(['like', 'index_image', $this->index_image])
            ->andFilterWhere(['like', 'content_file', $this->content_file]);

        return $dataProvider;
    }
}
