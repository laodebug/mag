<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "content".
 *
 * @property string $code
 * @property string|null $content_text
 */
class Content extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['content_text'], 'string'],
            [['code'], 'string', 'max' => 100],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Code',
            'content_text' => 'Content Text',
        ];
    }
}
