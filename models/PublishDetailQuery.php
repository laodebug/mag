<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PublishDetail]].
 *
 * @see PublishDetail
 */
class PublishDetailQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PublishDetail[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PublishDetail|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
