<?php

namespace app\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property int $deleted
 *
 * @property Essay[] $essays
 * @property History[] $histories
 */
class User extends ActiveRecord implements IdentityInterface
{

    public static function tableName()
    {
        return 'user';
    }

    public static function findIdentity($id)
    {
        return User::find()->where(['id' => $id, 'deleted' => 0])->one();
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    public function rules()
    {
        return [
            [['username', 'password'], 'required', 'message' => 'ກະລຸນາປ້ອນ {attribute}'],
            [['deleted'], 'integer'],
            [['role'], 'string'],
            [['username', 'password'], 'string', 'max' => 255],
            [['username'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'ຊື່ຜູ້ໃຊ້',
            'password' => 'ລະຫັດຜ່ານ',
            'deleted' => 'ລຶບ',
            'role' => 'ສິດ',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getEssays()
    {
        return $this->hasMany(Essay::className(), ['user_id' => 'id']);
    }

    public function getMeetings()
    {
        return $this->hasMany(Meeting::className(), ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPublishes()
    {
        return $this->hasMany(Publish::className(), ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getHistories()
    {
        return $this->hasMany(History::className(), ['user_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return false;
    }

}
