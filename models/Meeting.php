<?php

namespace app\models;

/**
 * This is the model class for table "meeting".
 *
 * @property int $id
 * @property string $title
 * @property string $created_date
 * @property string $meeting_date
 * @property string $remark
 * @property int $user_id
 * @property int $deleted
 *
 * @property User $user
 * @property Participants[] $participants
 */
class Meeting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meeting';
    }

    /**
     * {@inheritdoc}
     * @return MeetingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MeetingQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'created_date', 'meeting_date', 'user_id', 'deleted'], 'required'],
            [['created_date', 'meeting_date'], 'safe'],
            [['remark'], 'string'],
            [['user_id', 'deleted'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'ຫົວຂໍ້',
            'created_date' => 'ວັນທີບັນທຶກ',
            'meeting_date' => 'ວັນທີປະຊຸມ',
            'remark' => 'ໝາຍເຫດ',
            'user_id' => 'ຜູ້ບັນທຶກ',
            'deleted' => 'ລຶບ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipants()
    {
        return $this->hasMany(Participants::className(), ['meeting_id' => 'id']);
    }
}
