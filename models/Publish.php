<?php

namespace app\models;

/**
 * This is the model class for table "publish".
 *
 * @property int $id
 * @property int $month
 * @property int $year
 * @property int $deleted
 * @property int $user_id
 * @property string $last_update
 *
 * @property User $user
 * @property PublishDetail[] $publishDetails
 */
class Publish extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'publish';
    }

    /**
     * {@inheritdoc}
     * @return PublishQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PublishQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['month', 'year', 'user_id'], 'required'],
            [['month', 'year', 'deleted', 'user_id'], 'integer'],
            [['last_update'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'month' => 'ເດືອນ',
            'year' => 'ປີ',
            'deleted' => 'ລຶບ',
            'user_id' => 'ຜູ້ບັນທຶກ',
            'last_update' => 'ເວລາ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublishDetails()
    {
        return $this->hasMany(PublishDetail::className(), ['publish_id' => 'id']);
    }
}
