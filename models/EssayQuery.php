<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Essay]].
 *
 * @see Essay
 */
class EssayQuery extends \yii\db\ActiveQuery
{
    public function activ()
    {
        return $this->andWhere('deleted=0');
    }

    /**
     * {@inheritdoc}
     * @return Essay[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Essay|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
