<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "magazine".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property string $cover_image
 * @property string $index_image
 * @property string|null $content_file
 * @property int $for_month
 * @property int $for_year
 * @property int $deleted
 * @property int $hidden
 * @property string $created_date
 * @property int $user_id
 * @property string|null $author 
 * @property string|null $from_date 
 * @property string|null $to_date 
 * @property string|null $subtitle 
 *
 * @property User $user
 */
class Magazine extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'magazine';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content', 'cover_image', 'index_image', 'for_month', 'for_year', 'deleted', 'hidden', 'user_id'], 'required'],
            [['content', 'subtitle'], 'string'],
            [['for_month', 'for_year', 'deleted', 'hidden', 'user_id'], 'integer'],
            [['created_date', 'from_date', 'to_date'], 'safe'],
            [['title', 'cover_image', 'index_image', 'content_file', 'author'], 'string', 'max' => 500],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'ຫົວຂໍ້',
            'subtitle' => 'ບົດນຳ',
            'content' => 'ເນື້ອໃນ',
            'cover_image' => 'ຮູບໜ້າປົກ',
            'index_image' => 'ຮູບສາລະບານ',
            'content_file' => 'ຟາຍບົດວາລະສານ',
            'for_month' => 'ປະຈຳເດືອນ',
            'for_year' => 'ປະຈຳປີ',
            'deleted' => 'ລຶບ',
            'hidden' => 'ເຊື່ອງ',
            'created_date' => 'ວັນທີ່',
            'user_id' => 'ຜູ້ປ້ອນ',
            'author' => 'ຜູ້ຂຽນ',
            'from_date' => 'ຈາກວັນທີ',
            'to_date' => 'ຫາວັນທີ',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
