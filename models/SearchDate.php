<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 12/3/18
 * Time: 1:38 PM
 */

namespace app\models;


use yii\base\Model;

class SearchDate extends Model
{
    public $fromdate;
    public $todate;
}