<?php

namespace app\models;

/**
 * This is the model class for table "publish_detail".
 *
 * @property int $id
 * @property int $essay_id
 * @property int $publish_id
 * @property int $position
 *
 * @property Essay $essay
 * @property Publish $publish
 */
class PublishDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'publish_detail';
    }

    /**
     * {@inheritdoc}
     * @return PublishDetailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PublishDetailQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['essay_id', 'publish_id'], 'required'],
            [['essay_id', 'publish_id', 'position'], 'integer'],
            [['essay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::className(), 'targetAttribute' => ['essay_id' => 'id']],
            [['publish_id'], 'exist', 'skipOnError' => true, 'targetClass' => Publish::className(), 'targetAttribute' => ['publish_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'essay_id' => 'ບົດຄວາມ',
            'publish_id' => 'Publish',
            'position' => 'ລຳດັບ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEssay()
    {
        return $this->hasOne(Essay::className(), ['id' => 'essay_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublish()
    {
        return $this->hasOne(Publish::className(), ['id' => 'publish_id']);
    }
}
