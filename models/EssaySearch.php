<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * EssaySearch represents the model behind the search form of `app\models\Essay`.
 */
class EssaySearch extends Essay
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['id', 'deleted', 'user_id', 'level_id', 'employee_id', 'category_id', 'status_id', 'section_id', 'section_sub_id'], 'integer'],
           [['barcode', 'name', 'title', 'filename', 'source', 'tel', 'created_date', 'received_date', 'gender', 'last_update', 'place'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Essay::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_date' => $this->created_date,
            'received_date' => $this->received_date,
            'deleted' => $this->deleted,
            'user_id' => $this->user_id,
            'level_id' => $this->level_id,
            'employee_id' => $this->employee_id,
            'status_id' => $this->status_id,
            'section_id' => $this->section_id,
            'section_sub_id' => $this->section_sub_id,
        ]);

        $query->andFilterWhere(['like', 'barcode', $this->barcode])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'filename', $this->filename])
            ->andFilterWhere(['like', 'source', $this->source])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'tel', $this->tel]);

        return $dataProvider;
    }

    public function searchCanReceiveEssays($params)
    {
        $query = Essay::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_date' => $this->created_date,
            'received_date' => $this->received_date,
            'deleted' => $this->deleted,
            'user_id' => $this->user_id,
            'level_id' => $this->level_id,
            'employee_id' => $this->employee_id,
            'status_id' => $this->status_id,
            'section_id' => $this->section_id,
            'section_sub_id' => $this->section_sub_id,
        ]);

        $query->andFilterWhere(['like', 'barcode', $this->barcode])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'filename', $this->filename])
            ->andFilterWhere(['like', 'source', $this->source])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'tel', $this->tel]);
        $query->andWhere('status_id in (2,3,4)');

        return $dataProvider;
    }
}