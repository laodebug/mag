<?php

namespace app\models;

/**
 * This is the model class for table "status".
 *
 * @property int $id
 * @property string $name
 * @property int $deleted
 * @property string $color
 *
 * @property Essay[] $essays
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * {@inheritdoc}
     * @return StatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StatusQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'message' => 'ກະລຸນາປ້ອນ {attribute}'],
            [['deleted'], 'integer'],
            [['name', 'color'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ຊື່',
            'deleted' => 'Deleted',
            'color' => 'ສີ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEssays()
    {
        return $this->hasMany(Essay::className(), ['status_id' => 'id']);
    }
}
