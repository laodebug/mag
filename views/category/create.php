<?php

use app\components\MyForm;


/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = 'ເພີ່ມ ປະເພດບົດຄວາມ';
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo MyForm::r($this, $model);
