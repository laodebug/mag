<?php

use app\components\MyForm;

/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = 'ແກ້ໄຂ ປະເພດບົດຄວາມ: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
echo MyForm::r($this, $model);