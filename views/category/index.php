<?php

use app\components\MyGridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ປະເພດບົດຄວາມ';
$this->params['breadcrumbs'][] = $this->title;
$cols = [
    ['attribute' => 'name']
];
echo MyGridView::r($dataProvider, $searchModel, $cols, 0);