<?php

use app\components\MyForm;


/* @var $this yii\web\View */
/* @var $model app\models\History */

$this->title = 'ເພີ່ມ ເນື້ອໃນ';
$this->params['breadcrumbs'][] = ['label' => 'ປະຫວັດຄວາມເປັນມາ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo MyForm::r($this, $model);
