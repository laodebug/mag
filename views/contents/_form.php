<?php

use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contents-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'content')->widget(CKEditor::className(), Yii::$app->params['ckoptions']); ?>
    <div class="form-group">
        <?= Html::submitButton('ບັນທຶກ', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
