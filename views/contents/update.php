<?php

use app\components\MyForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contents */

$this->title = 'ແກ້ໄຂ ເນື້ອໃນ: '. $model->title;
$this->params['breadcrumbs'][] = ['label' => 'ເນື້ອໃນ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->code]];
$this->params['breadcrumbs'][] = 'ແກ້ໄຂ';
echo MyForm::r($this, $model);