<?php

use app\components\MyForm;


/* @var $this yii\web\View */
/* @var $model app\models\Duration */

$this->title = 'ເພີ່ມ ໄລຍະເວລາ';
$this->params['breadcrumbs'][] = ['label' => 'ໄລຍະເວລາ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo MyForm::r($this, $model);