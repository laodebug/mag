<?php

use app\components\MyForm;

/* @var $this yii\web\View */
/* @var $model app\models\Duration */

$this->title = 'ແກ້ໄຂ ໄລຍະເວລາ: ' . $model->remark;
$this->params['breadcrumbs'][] = ['label' => 'Durations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->remark, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'ໄລຍະເວລາ';
echo MyForm::r($this, $model);