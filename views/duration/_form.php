<?php

use app\components\MyTextInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Duration */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="duration-form">

    <?php $form = ActiveForm::begin(); ?>
  <div class="col-sm-6">
      <?= MyTextInput::r($form, $model, 'min', 1) ?>
  </div>
  <div class="col-sm-6">
      <?= MyTextInput::r($form, $model, 'max') ?>
  </div>
  <div class="col-sm-6">
      <?= MyTextInput::r($form, $model, 'remark') ?>
  </div>
  <div class="col-sm-5">
      <?= $form->field($model, 'color')->textInput(['class' => 'form-control colorpicker']) ?>
  </div>
  <div class="col-sm-1">
    <div class="form-group field-shipmentstatus-color">
      <label class="control-label" for="shipmentstatus-color"></label>
      <div id="color"></div>
    </div>
  </div>
  <div class="col-sm-12">
      <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
  </div>

    <?php ActiveForm::end(); ?>

</div>
