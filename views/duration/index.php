<?php

use app\components\MyGridView;
use app\models\Duration;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DurationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ໄລຍະເວລາ ກວດບົດ (ຄິດໄລ່ເປັນມື້)';
$this->params['breadcrumbs'][] = $this->title;
$labels = new Duration();
$labels = $labels->attributeLabels();
$cols = [
    ['attribute' => 'min'],
    ['attribute' => 'max'],
    ['attribute' => 'remark'],
    [
        'attribute' => 'color',
        'format' => 'raw',
        'value' => function ($data) {
            return '<div class="color" style="background-color: ' . $data->color . '"></div>';
        }
    ]
];
echo MyGridView::r($dataProvider, $searchModel, $cols, 0, 1, 1, 1, $labels);