<?php

/* @var $this yii\web\View */

use app\components\MyForm;

/* @var $model app\models\Level */

$this->title = 'Update Level: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Levels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
echo MyForm::r($this, $model);