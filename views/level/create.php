<?php

use app\components\MyForm;


/* @var $this yii\web\View */
/* @var $model app\models\Level */

$this->title = 'Create Level';
$this->params['breadcrumbs'][] = ['label' => 'Levels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo MyForm::r($this, $model);