<?php

use app\components\MyTextInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Level */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="level-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= MyTextInput::r($form, $model, 'name', 1) ?>
  <div class="form-group">
      <?= Html::submitButton('ບັນທຶກ', ['class' => 'btn btn-success']) ?>
  </div>

    <?php ActiveForm::end(); ?>

</div>
