<?php

use app\components\MyDatePicker;
use app\components\MyDropdown;
use app\components\MyTextInput;
use app\models\Category;
use app\models\Section;
use app\models\SectionSub;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Essay */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
.field-essay-source,
.field-essay-place {
    display: none;
}
</style>
<div class="essay-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= MyTextInput::r($form, $model, 'title', 1) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'category_id')
                ->dropDownList(ArrayHelper::map(Category::find()->all(), 'id', 'name'), [
                    'class' => 'form-control input-lg'
                ])
                ->label('ປະເພດບົດ')
            ?>
        </div>
        <div class="col-sm-12">
            <?php
            //          if ($model->isNewRecord)
            echo $form->field($model, 'file')->fileInput(['class' => 'form-control input-lg'])
            ?>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-2">
            <?= $form->field($model, 'gender')->dropDownList(['F' => 'ຍິງ', 'M' => 'ຊາຍ'], [
                'class' => 'form-control input-lg'
            ]) ?>
        </div>
        <div class="col-sm-4">
            <?= MyTextInput::r($form, $model, 'name') ?>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <!-- <div class="col-sm-12 text-center">
                    <label class="control-label" for="">ມາຈາກພາກສ່ວນ</label>
                </div> -->
                <div class="col-sm-6">
                    <?php
                    echo $form->field($model, 'source')
                        ->dropDownList([
                            'ນັກສຶກສາ' => 'ນັກສຶກສາ',
                            'ອາຈານ' => 'ອາຈານ',
                            'ພະນັກງານ' => 'ພະນັກງານ',
                            'ບຸກຄົນທາງນອກ' => 'ບຸກຄົນທາງນອກ',
                        ], [
                            'class' => 'form-control input-lg'
                        ])->label(false);
                    echo MyDropdown::r2($form, $model, 'section_id', Section::find()->all());
                    ?>
                </div>
                <div class="col-sm-6">
                    <!-- <div id="student"> -->
                    <?php
                    echo $form->field($model, 'place')
                        ->textInput(['class' => 'form-control input-lg']);
                    ?>
                    <?php
                    echo MyDropdown::r2($form, $model, 'section_sub_id', SectionSub::find()->all());
                    ?>
                    <!-- </div> -->
                    <!-- <div id="teacher">
                        <select class="form-control input-lg place" name="place">
                            <option value="ປັດຊະຍາ">ປັດຊະຍາ</option>
                            <option value="ກໍ່ສ້າງພັກ">ກໍ່ສ້າງພັກ</option>
                            <option value="ບໍລິຫານສາດ">ບໍລິຫານສາດ</option>
                            <option value="ເສດຖະສາດ">ເສດຖະສາດ</option>
                            <option value="ລັດທິ">ລັດທິ</option>
                        </select>
                    </div>
                    <div id="employee">
                        <select class="form-control input-lg place">
                            <option value="ສູນຂໍ້ມູນຂ່າວສານ">ສູນຂໍ້ມູນຂ່າວສານ</option>
                            <option value="ກົມວິຊາການ">ກົມວິຊາການ</option>
                            <option value="ຫ້ອງການ">ຫ້ອງການ</option>
                            <option value="ກົມຈັດຕັ້ງ" selected="">ກົມຈັດຕັ້ງ</option>
                            <option value="ກົມ ຄວສ">ກົມ ຄວສ</option>
                            <option value="ກົມກວດກາ">ກົມກວດກາ</option>
                            <option value="ສູນແນວຄິດ">ສູນແນວຄິດ</option>
                        </select>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= MyTextInput::r($form, $model, 'tel') ?>
        </div>
        <div class="col-sm-6">
            <?= MyDatePicker::r($form, $model, 'received_date') ?>
        </div>
    </div>
    <hr>
    <div class="col-sm-12 text-center">
        <?= Html::submitButton('<i class="fa fa-save"></i> ບັນທຶກ', ['class' => 'btn btn-primary btn-lg']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerJs(
    "$('#essay-section_id').change(function() {
        var id = $(this).val();
        var section = $('#essay-section_id option[value=\"'+id+'\"]').html();
        $('#essay-source').val(section);
        $.get('index.php?r=section/gets&id='+id, function(res) {
            $('#essay-section_sub_id').empty();
            res = JSON.parse(res);
            if(res && res.length > 0) {
                $('.field-essay-place').hide();
                $('.field-essay-section_sub_id').show();
                res.forEach((r) => {
                    $('#essay-section_sub_id').append('<option value=' + r.id + '>'+r.name+'</option>');
                });
            } else {
                $('.field-essay-section_sub_id').hide();
                $('#essay-place').val('');
                $('.field-essay-place').show();
            }
        });
    });"
);
// $this->registerJs(
//     "$('#essay-source').change(function() {
//         var val = $(this).val();
//         console.log(val);
//         if(val === 'ນັກສຶກສາ' || val === 'ບຸກຄົນທາງນອກ') {
//           $('#student').show();
//           $('#teacher, #employee').hide();
//           $('#essay-place').val('').empty();
//         } else if(val === 'ອາຈານ') {
//           $('#teacher').show();
//           $('#teacher .place').change();
//           $('#student, #employee').hide();
//         } else if(val === 'ພະນັກງານ') {
//           $('#employee').show();
//           $('#employee .place').change();
//           $('#teacher, #student').hide();
//         }     
//     });
//     $('#essay-source').change();
//     $('.place').change(function (){
//       var v = $(this).val();
//       console.log('essay-place', v);
//       $('#essay-place').val(v);
//     });
//     "
// );