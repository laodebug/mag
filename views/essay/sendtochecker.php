<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 3/24/19
 * Time: 8:39 PM
 */

use app\components\EssayGrid;
use yii\widgets\ActiveForm;

$this->title = 'ສົ່ງ ບົດຄວາມ ໃຫ້ ບກ ກວດ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
  <div class="box-body">
    <div class="well">
        <?php $form = ActiveForm::begin(['method' => 'get']); ?>
      <div class="input-group input-group-lg">
        <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
        <input type="text" class="form-control" name="barcode" autofocus="true" value=""
               placeholder="ບາໂຄດ">
        <span class="input-group-btn">
              <button type="submit" class="btn btn-primary btn-flat">
                <i class="fa fa-search"></i>
              </button>
            </span>
      </div>
        <?php ActiveForm::end(); ?>
    </div>
      <?php
      echo EssayGrid::widget([
          'showchecker' => false,
          'dataProvider' => $dataProvider,
          'filterModel' => false
      ]);
      ?>
  </div>
</div>