<?php

use app\components\MyForm;

/* @var $this yii\web\View */
/* @var $model app\models\Essay */

$this->title = 'ແກ້ໄຂ ຂໍ້ມູນບົດຄວາມ: ' . $model->title;
//$this->params['breadcrumbs'][] = ['label' => 'ຂໍ້ມູນບົດຄວາມ', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = 'ແກ້ໄຂ';
echo MyForm::r($this, $model);