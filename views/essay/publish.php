<?php

use app\models\Essay;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EssaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ບົດທີ່ສາມາດລົງໄດ້';
$this->params['breadcrumbs'][] = $this->title;
$essay = new Essay();
$labels = $essay->attributeLabels();
$months = [
    ["id" => 1, "name" => '1 - ມັງກອນ'],
    ["id" => 2, "name" => '2 - ກຸມພາ'],
    ["id" => 3, "name" => '3 - ມີນາ'],
    ["id" => 4, "name" => '4 - ເມສາ'],
    ["id" => 5, "name" => '5 - ພຶດສະພາ'],
    ["id" => 6, "name" => '6 - ມິຖຸນາ'],
    ["id" => 7, "name" => '7 - ກໍລະກົດ'],
    ["id" => 8, "name" => '8 - ສິງຫາ'],
    ["id" => 9, "name" => '9 - ກັນຍາ'],
    ["id" => 10, "name" => '10 - ຕຸລາ'],
    ["id" => 11, "name" => '11 - ພະຈິກ'],
    ["id" => 12, "name" => '12 - ທັນວາ'],
];
?>
<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">
                <?php
                try {
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'emptyText' => 'ບໍ່ມີຂໍ້ມູນ',
                        'headerRowOptions' => [
                            'style' => 'background-color: #cccccc'
                        ],
                        'summary' => "ສະແດງ {begin} ຫາ {end} ຈາກທັງໝົດ {totalCount}",
                        'columns' => [
                            [
                                'class' => SerialColumn::className(),
                                'options' => [
                                    'style' => 'width: 50px'
                                ]
                            ],
                            // [
                            //     'class' => 'yii\grid\CheckboxColumn',
                            //     'name' => 'selected'
                            // ],
                            // [
                            //     'label' => 'ລຳດັບ',
                            //     'format' => 'raw',
                            //     'options' => ['width' => '100px'],
                            //     'value' => function ($data) {
                            //         if (isset($data) && $data->id)
                            //             return Html::textInput('positions[' . $data->id . ']', null, ['class' => 'form-control input-lg']);
                            //     }
                            // ],
                            [
                                'attribute' => 'title',
                                'filterInputOptions' => [
                                    'class' => 'form-control input-lg',
                                    'placeholder' => 'ຊອກຫາ'
                                ]
                            ],
                            [
                                'attribute' => 'name',
                                'filterInputOptions' => [
                                    'class' => 'form-control input-lg',
                                    'placeholder' => 'ຊອກຫາ'
                                ]
                            ],
                            [
                                'attribute' => 'source',
                                'filterInputOptions' => [
                                    'class' => 'form-control input-lg',
                                    'placeholder' => 'ຊອກຫາ'
                                ]
                            ],
                            // [
                            //     'attribute' => 'place',
                            //     'filterInputOptions' => [
                            //         'class' => 'form-control input-lg',
                            //         'placeholder' => 'ຊອກຫາ'
                            //     ]
                            // ],
                            [
                                'label' => '',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return Html::a('<i class="fa fa-plus"></i>', ['essay/addpublish', 'id' => $data->id], [
                                        'class' => 'btn btn-success',
                                        'data-toggle' => "tooltip",
                                        'data-placement' => "top",
                                        'title' => "ເພີ່ມ"
                                    ]);
                                }
                            ]
                        ]
                    ]);
                    // Pjax::end();
                } catch (Exception $ex) {
                    echo $ex->getMessage();
                }
                ?>
            </div>
            <div class="col-sm-4">
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-sm-12">
                        <?php if (isset($_SESSION['publish']) && count($_SESSION['publish']) > 0) : ?>
                            <div class="essay-form well">
                                <div class="row">
                                    <div class="col-sm-7">
                                        <?= $form->field($publish, 'month')->label('ເດືອນ')
                                            ->dropDownList(\yii\helpers\ArrayHelper::map($months, 'id', 'name'), [
                                                'class' => 'form-control input-lg'
                                            ]) ?>
                                    </div>
                                    <div class="col-sm-5">
                                        <?= \app\components\MyTextInput::number($form, $publish, 'year') ?>
                                    </div>
                                </div>
                            </div>
                            <?php foreach ($_SESSION['publish'] as $key => $value) : ?>
                                <div>
                                    <?= Html::a('<i class="fa fa-remove"></i>', ['essay/removepublish', 'id' => $value['id']], ['class' => 'btn btn-danger btn-sm']) ?>
                                    <?= $value['title'] ?> -- <i><b style="color: grey"><?= $value['name'] ?></b></i>
                                </div>
                                <hr>
                            <?php endforeach;  ?>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary btn-lg">
                                    ບັນທຶກ
                                </button>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>