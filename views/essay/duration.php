<?php

use app\components\MyColor;
use app\models\Employee;
use app\models\Essay;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EssaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ບົດຄວາມ ສະຖານະ ' . $duration->remark;
$this->params['breadcrumbs'][] = $this->title;
$essay = new Essay();
$labels = $essay->attributeLabels();
?>
<div class="box">
  <div class="box-body">
      <?php
      try {
          Pjax::begin();
          echo GridView::widget([
              'dataProvider' => $dataProvider,
              'filterModel' => $searchModel,
              'emptyText' => 'ບໍ່ມີຂໍ້ມູນ',
              'headerRowOptions' => [
                  'style' => 'background-color: #cccccc'
              ],
              'summary' => "ສະແດງ {begin} ຫາ {end} ຈາກທັງໝົດ {totalCount}",
              'columns' => [
                  [
                      'class' => SerialColumn::className(),
                      'options' => [
                          'style' => 'width: 50px'
                      ]
                  ],
                  [
                      'attribute' => 'barcode',
                      'format' => 'raw',
                      'options' => [
                          'style' => 'width: 100px'
                      ],
                      'filterInputOptions' => [
                          'class' => 'form-control input-lg',
                          'placeholder' => 'ຊອກຫາ'
                      ],
                      'value' => function ($data) {
                          return Html::a($data->barcode, ['essay/view', 'id' => $data->id]);
                      }
                  ],
                  [
                      'attribute' => 'title',
                      'filterInputOptions' => [
                          'class' => 'form-control input-lg',
                          'placeholder' => 'ຊອກຫາ'
                      ]
                  ],
                  [
                      'attribute' => 'name',
                      'filterInputOptions' => [
                          'class' => 'form-control input-lg',
                          'placeholder' => 'ຊອກຫາ'
                      ]
                  ],
                  [
                      'attribute' => 'source',
                      'filterInputOptions' => [
                          'class' => 'form-control input-lg',
                          'placeholder' => 'ຊອກຫາ'
                      ]
                  ],
                  [
                      'attribute' => 'place',
                      'filterInputOptions' => [
                          'class' => 'form-control input-lg',
                          'placeholder' => 'ຊອກຫາ'
                      ]
                  ],
                  [
                      'attribute' => 'employee_id',
                      'filter' => ArrayHelper::map(Employee::find()->where('deleted=0')->all(), 'id', 'name'),
                      'filterInputOptions' => [
                          'class' => 'form-control input-lg',
                          'prompt' => 'ທັງໝົດ'
                      ],
                      'value' => function ($data) {
                          return $data->employee ? $data->employee->name : '';
                      }
                  ],
                  [
                      'attribute' => 'last_update',
                      'label' => 'ເວລາ (ມື້)',
                      'format' => 'raw',
                      'value' => function ($data) use ($durations) {
                          return in_array($data->status_id, [2, 3, 4]) ? MyColor::day($durations, $data->last_update) : '';
                      }
                  ],
                  [
                      'attribute' => 'status_id',
                      'label' => 'ສະຖານະ',
                      'format' => 'raw',
                      'filter' => ArrayHelper::map($statuses, 'id', 'name'),
                      'value' => function ($data) use ($statuses) {
                          return MyColor::s($statuses, $data->status_id);
                      }
                  ],
                  [
                      'label' => '',
                      'format' => 'raw',
                      'value' => function ($data) {
                          return Html::a('<i class="fa fa-eye"></i>', ['essay/view', 'id' => $data->id], ['class' => 'btn btn-info btn-lg']);
                      }
                  ]
              ]
          ]);
          Pjax::end();
      } catch (Exception $ex) {
          echo $ex->getMessage();
      }
      ?>
  </div>
</div>
