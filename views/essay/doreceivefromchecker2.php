<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 10/12/18
 * Time: 11:21 PM
 */

use app\components\EssayDetailView;
use app\models\Employee;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Essay */
/* @var $history app\models\History */

/* @var $form yii\widgets\ActiveForm */

$this->title = 'ຮັບບົດຄວາມຈາກ ບກ ຂັ້ນ ຜູ້ຊີ້ນຳ';
$employees = Employee::find()->where(['level_id' => 2,'deleted' => 0])->all();
?>
  <div class="box box-solid">

    <div class="box-body">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
      <div class="row">
        <div class="col-sm-5">
            <?= EssayDetailView::widget(['model' => $model]) ?>
        </div>
        <div class="col-sm-7">
          <div class="col-sm-12 well">
            <div class="row">
              <div class="col-sm-6">
                  <?php
                  if (isset($model->employee)) {
                      echo "<label class=''>ບັນນາໂດຍ: &nbsp;</label>";
                      echo "<h4>" . $model->employee->name . "</h4>";
                  }
                  ?>
              </div>
              <div class="col-sm-6">
                  <?= $form->field($history, 'file')->fileInput(['class' => 'form-control input-lg'])->label('ຟາຍບົດຄວາມ') ?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                  <?= $form->field($history, 'status_id')->label('ທິດທາງ')
                      ->dropDownList([
                          '3' => 'ສົ່ງຄືນເຈົ້າຂອງປັບປຸງ',
                          '2' => 'ສົ່ງຄືນໃຫ້ ບກ ຂັ້ນຕົ້ນ',
                          '5' => 'ບົດລໍຖ້າລົງ'
                      ], ['class' => 'form-control input-lg']) ?>
              </div>
              <div class="col-sm-6">
                <div class="row" id="forward">
                  <div class="col-sm-12">
                      <?= $form->field($history, 'employee_id')->label('ບກ ຂັ້ນຕົ້ນ')
                          ->dropDownList(ArrayHelper::map($employees, 'id', 'name'), ['class' => 'form-control input-lg']) ?>
                  </div>
                </div>
                <div class="row" id="return">
                  <div class="col-sm-12">
                      <?= $form->field($history, 'return_receiver')
                          ->textInput(['class' => 'form-control input-lg'])
                          ->label('ຊື່ຜູ້ຮັບບົດຄືນ') ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                  <?= $form->field($history, 'comment')
                      ->textarea(['placeholder' => 'ຄຳຄິດເຫັນ',
                          'rows' => 5]) ?>
              </div>
              <div class="col-sm-12">
                <div class="text-center">
                  <button class="btn btn-primary btn-lg"><i class="fa fa-save"></i> ບັນທຶກ</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-4 text-center col-sm-offset-4">
          <div id="webcam">
            <video id="video" style="width:100%;height:100%" autoplay></video>
          </div>
        </div>
        <div class="col-sm-12 text-center" id="result">
        </div>
      </div>
        <?php ActiveForm::end(); ?>
    </div>
  </div>
<?php
$this->registerJs("
var vdo = document.querySelector('video');
var scale = 1.5;
var index = 0;

$('#history-status_id').change(function() {
  var v = $(this).val();
  $('#publish, #forward, #return').hide();
  if(v === '5') {
    $('#publish').show();
  } else if(v === '2') {
    $('#forward').show();
  } else if (v === '3') {
    $('#return').show();
  }
});
$('#history-status_id').change();

if(navigator.mediaDevices)
navigator.mediaDevices.getUserMedia({video: true}).then(function(res) {
  vdo.srcObject = res;
  vdo.onclick = function(r) {
    index++;
    var canvas = document.createElement('canvas');
        canvas.width = vdo.videoWidth * scale;
        canvas.height = vdo.videoHeight * scale;
        canvas.getContext('2d')
              .drawImage(vdo, 0, 0, canvas.width, canvas.height);        
        
        var img = $('<img style=\'width: 100%\'>');
        img.data('index', index);
        img.attr('src', canvas.toDataURL());
        
        var btn = $('<button class=\'btn btn-danger btn-block\' type=\'button\'><i class=\'fa fa-trash\'></i></button>');
        btn.css('margin-bottom', '10px');
        btn.data('index', index);
        btn.click(function(e) {
          console.log(e);
          var index = $(this).data('index');
          $('#row' + index).remove();
        });
        
        var input = $('<input>');
        input.attr('type','hidden');
        input.attr('name','History[photos]['+index+']');
        input.val(canvas.toDataURL());
        input.type='hidden';
        input.value = img.src;
        
        var item = $('<div class=\'col-sm-4\' id=\"row'+index+'\">');
        item.append(img)
        .append(btn)
        .append(input);
        $('#result').append(item);             
  };
});

$('#capture').click(function() {
  Webcam.snap(function(data_uri) {    
    $('#results').append('<img style=\"width: 100%\" src=\"'+data_uri+'\"/>');
  });
 });
");
