<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 3/24/19
 * Time: 5:17 PM
 */

use app\models\Employee;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$this->title = 'ປ່ຽນ ບກ';
$employees = null;
$employees = Employee::find()->where('deleted=0')->all();
?>
<div class="box">
  <div class="box-body">
    <div class="essay-form">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
      <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'employee_id')->dropDownList(ArrayHelper::map($employees, 'id', 'name')) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <button type="submit" class="btn btn-primary btn-lg">
            <i class="fa fa-save"></i> ບັນທຶກ
          </button>
        </div>
      </div>
        <?php ActiveForm::end() ?>
    </div>

  </div>
</div>
