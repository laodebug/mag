<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 10/12/18
 * Time: 11:21 PM
 */

use app\components\EssayDetailView;
use app\models\Employee;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Essay */
/* @var $history app\models\History */

/* @var $form yii\widgets\ActiveForm */
$this->title = 'ສົ່ງ ບົດຄວາມ ໃຫ້ ບກ ຂັ້ນຕົ້ນກວດ';
$this->params['breadcrumbs'][] = $this->title;
$employees = Employee::find()->andWhere('deleted=0')->all();
if (isset($model)) {
    if ($model->status_id == 1)
        $employees = Employee::find()->where(['level_id' => 2, 'deleted' => 0])->all();
    else if ($model->status_id == 2)
        $employees = Employee::find()->where(['level_id' => 3, 'deleted' => 0])->all();
}
?>
<div class="box box-solid">
  <div class="box-body">
    <div class="row">
      <div class="col-sm-5">
          <?= EssayDetailView::widget([
              'model' => $model
          ]) ?>
      </div>
      <div class="col-sm-7">
          <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
          ?>
        <div class="col-sm-12 well">
          <div class="row">
            <div class="col-sm-12">
                <?php echo $form->field($history, 'employee_id')->label('ບກ ຂັ້ນຕົ້ນ')
                    ->dropDownList(ArrayHelper::map($employees, 'id', 'name'), [
                        'prompt' => 'ກະລຸນາເລືອກ',
                        'class' => 'form-control input-lg'
                    ]);
                ?>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="text-center">
                <button class="btn btn-primary btn-lg"><i class="fa fa-save"></i> ບັນທຶກ</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      <?php ActiveForm::end(); ?>
  </div>
</div>

