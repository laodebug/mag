<?php

use app\components\MyForm;


/* @var $this yii\web\View */
/* @var $model app\models\Essay */

$this->title = 'ເພີ່ມ ບົດຄວາມໃໝ່';
$this->params['breadcrumbs'][] = ['label' => 'ບົດຄວາມ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo MyForm::r($this, $model);