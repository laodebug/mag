<?php

use app\models\PublishDetail;
use Picqer\Barcode\BarcodeGenerator;
use Picqer\Barcode\BarcodeGeneratorHTML;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Essay */

$this->title = 'ບົດຄວາມ: ' . $model->title;
//$this->params['breadcrumbs'][] = ['label' => 'Essays', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
$bar = new BarcodeGeneratorHTML();
$role = Yii::$app->user->identity->role;
?>
<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-3">
                <?= $bar->getBarcode($model->barcode, BarcodeGenerator::TYPE_CODE_39, 1, 50) ?>
                <p><strong><?= $model->barcode ?></strong></p>
            </div>
            <div class="col-sm-3">
                <?php
                if ($model->status_id == 5)  : ?>
                    <div class="small-box text-center" style="background-color: #06db65">
                        <div class="inner">
                            <h4>ບົດລໍຖ້າລົງ</h4>
                        </div>
                    </div>
                <?php
                endif;
                if ($model->status_id == 6) :
                    $publishdetail = PublishDetail::find()->alias('d')
                        ->join('join', 'publish p', 'p.id = d.publish_id and p.deleted=0')
                        ->where([
                            'essay_id' => $model->id
                        ])
                        ->one();
                    ?>
                    <div class="small-box text-center" style="background-color: #06db65">
                        <div class="inner">
                            <h4>ບົດລົງແລ້ວ</h4>
                            <p>ເດືອນ: <?= $publishdetail->publish->month . '/' . $publishdetail->publish->year; ?></p>
                        </div>
                    </div>
                <?php
                endif;
                ?>
            </div>
            <div class="col-sm-6 text-right">
                <?php
                echo Html::a('<i class="fa fa-print"></i>', ['print', 'id' => $model->id], [
                    'class' => 'btn btn-lg btn-success',
                    'data-toggle' => "tooltip",
                    'data-placement' => "top",
                    'title' => "ພິມບາໂຄດ"
                ]);
                if (!in_array($role, ['ທົ່ວໄປ'])) {
                    echo in_array($model->status_id, [1]) ? Html::a('<i class="fa fa-user-plus"></i>', ['essay/sendtochecker', 'barcode' => $model->barcode], [
                        'class' => 'btn btn-primary btn-lg',
                        'data-toggle' => "tooltip",
                        'data-placement' => "top",
                        'title' => "ສົ່ງໃຫ້ ບກ ກວດ"
                    ]) : '';
                    echo in_array($model->status_id, [2, 3, 4]) ? Html::a('<i class="fa fa-handshake-o"></i>', ['essay/doreceive', 'barcode' => $model->barcode], [
                        'class' => 'btn btn-info btn-lg',
                        'data-toggle' => "tooltip",
                        'data-placement' => "top",
                        'title' => "ຮັບບົດຄວາມຈາກ ບກ"]) : '';

                    echo in_array($model->status_id, [1, 2, 3, 4]) ? Html::a('<i class="fa fa-calendar-check-o"></i>', ['essay/dopublish', 'barcode' => $model->barcode], [
                        'class' => 'btn btn-success btn-lg',
                        'data-toggle' => "tooltip",
                        'data-placement' => "top",
                        'title' => "ບົດລໍຖ້າລົງ"]) : '';
                    echo Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $model->id], [
                        'class' => 'btn btn-warning btn-lg',
                        'data-toggle' => "tooltip",
                        'data-placement' => "top",
                        'title' => "ແກ້ໄຂຂໍ້ມູນບົດຄວາມ"
                    ]);
                    echo Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger btn-lg',
                        'data-toggle' => "tooltip",
                        'data-placement' => "top",
                        'title' => "ລຶບບົດຄວາມ",
                        'data' => [
                            'confirm' => 'ທ່ານຕ້ອງການລຶບແທ້ບໍ່?',
                            'method' => 'post',
                        ],
                    ]);
                }
                ?>
            </div>
        </div>
        <hr>
        <div class="col-sm-6 well">
            <?php
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'name',
                        [
                            'attribute' => 'gender',
                            'value' => function ($data) {
                                return $data['gender'] == 'M' ? 'ຊາຍ' : 'ຍິງ';
                            }
                        ],
                        [
                            'attribute' => 'source',
                            'value' => function ($data) {
                                return isset($data->section) ? $data->section->name : $data->source;
                            }
                        ],
                        [
                            'attribute' => 'sectionSub',
                            'label' => 'ກົມກອງ',
                            'value' => function ($data) {
                                return isset($data->sectionSub) ? $data->sectionSub->name : $data->place;
                            }
                        ],
                        'tel'
                    ]
                ]);
            } catch (Exception $exception) {
                echo $exception->getMessage();
            }
            ?>
        </div>
        <div class="col-sm-6 well">
            <?php
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'attribute' => 'filename',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::a($data['filename'], 'uploads/' . $data['filename'], ['target' => '_blank']);
                            }
                        ],
                        'created_date',
                        'received_date',
                        [
                            'attribute' => 'employee_id',
                            'value' => function ($data) {
                                if ($data->employee)
                                    return $data->employee->name;
                            }
                        ]
                    ]
                ]);
            } catch (Exception $exception) {
                echo $exception->getMessage();
            }
            ?>
        </div>
        <div class="col-sm-12">
            <?= $this->render('timeline', [
                'model' => $model
            ]) ?>
        </div>
    </div>
</div>
