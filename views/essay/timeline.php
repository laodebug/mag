<?php

/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 10/12/18
 * Time: 10:04 PM
 */

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $model app\models\Essay */
/* @var $histories app\models\History[] */
$histories = $model->getHistories()->orderBy('created_date')->all();
$role = Yii::$app->user->identity->role;
?>
<div class="box box-solid box-primary">
  <div class="box-body">
      <?= GridView::widget([
          'dataProvider' => new ActiveDataProvider(['query' => $model->getHistories()->andWhere(['deleted' => 0])]),
          'headerRowOptions' => [
              'style' => 'background-color: #cccccc'
          ],
          'summary' => false,
          'columns' => [
              'created_date',
              [
                  'label' => 'ເຫດການ',
                  'format' => 'html',
                  'value' => function ($data) {
                      $str = '<p>';
                      if (strtoupper($data->sr) === 'U') {
                          $str = $data->user->username . ' ແກ້ໄຂຂໍ້ມູນບົດຄວາມ ';
                      } else {
                          $str .= $data->user->username;
                          switch ($data->status_id) {
                              case 1:
                                  $str .= ' ຮັບ ບົດຄວາມໃໝ່ຈາກເຈົ້າຂອງບົດ ' . (isset($data->essay) ? $data->essay->name : '');
                                  break;
                              case 2:
                                  if (strtoupper($data->sr) == "S")
                                      $str .= ' ສົ່ງ ບົດຄວາມໃຫ້ ບກ ຂັ້ນຕົ້ນ ' . (isset($data->employee) ? $data->employee->name : '');
                                  else
                                      $str .= ' ຮັບ ບົດຄວາມຈາກ ບກ ຂັ້ນຕົ້ນ ' . (isset($data->employee) ? $data->employee->name : '');
                                  break;
                              case 3:
                                  if (strtoupper($data->sr) == "R")
                                      $str .= ' ຮັບ ບົດຄວາມຈາກຜູ້ກ່ຽວປັບປຸງ ';
                                  else
                                      $str .= ' ສົ່ງ ບົດຄວາມໃຫ້ ຜູ້ກ່ຽວປັບປຸງ ' . $data->return_receiver;
                                  break;
                              case 4:
                                  if (strtoupper($data->sr) == "R")
                                      $str .= ' ຮັບ ບົດຄວາມຈາກ ບກ ຂັ້ນຜູ້ຊີ້ນຳ ' . (isset($data->employee) ? $data->employee->name : '');
                                  else
                                      $str .= ' ສົ່ງ ບົດຄວາມໃຫ້ ບກ ຂັ້ນຜູ້ຊີ້ນຳ ' . (isset($data->employee) ? $data->employee->name : '');
                                  break;
                              case 5:
                                  $str .= ' ປ່ຽນເປັນ ບົດລໍຖ້າລົງ ວາລະສານ';
                                  break;
                              case 6:
                                  $str .= ' ບົດລົງ ວາລະສານແລ້ວ';
                                  break;
                          }
                      }
                      $str .= '</p>';
                      if (isset($data->comment) && strlen($data->comment) > 0)
                          $str .= '<p><i class="fa fa-commenting-o"></i> ' . $data->comment . '</p>';

                      $str .= '<br>';
                      foreach ($data->photos as $photo) {
                          $str .= Html::a('<img src="uploads/' . $photo->name . '.png" class="aa" />', 'uploads/' . $photo->name . '.png', [
                              'target' => '_blank'
                          ]);
                      }

                      return $str;
                  }
              ],
              [
                  'label' => 'ຟາຍ',
                  'format' => 'raw',
                  'value' => function ($data) {
                      return $data->file_name ? Html::a(' <i class="fa fa-download" ></i> ', 'uploads/' . $data->file_name, [
                          'class' => 'btn btn-primary',
                          'target' => '_blank',
                          'data-toggle' => "tooltip",
                          'data-placement' => "top",
                          'title' => "ດາວໂຫຼດຟາຍ"
                      ]) : '';
                  }
              ],
              [
                  'label' => '',
                  'format' => 'raw',
                  'value' => function ($data) use ($role) {
                      $str = '';
                      if ($role !== 'ທົ່ວໄປ' && in_array($data->status_id, [2, 4]))
                          $str .= Html::a(' <i class="fa fa-pencil" ></i > ', ['essay/changechecker', 'id' => $data->id], [
                              'class' => 'btn btn-warning',
                              'data-toggle' => "tooltip",
                              'data-placement' => "top",
                              'title' => "ປ່ຽນ ບກ"
                          ]);

                      $str .= Html::a(' <i class="fa fa-trash" ></i > ', ['history/delete', 'id' => $data->id], [
                          'class' => 'btn btn-danger',
                          'data-method' => "post",
                          'data-toggle' => "tooltip",
                          'data-placement' => "top",
                          'title' => "ລຶບ"
                      ]);
                      return $str;
                  }
              ]
          ]
      ]);
      ?>
  </div>
</div>
<style>
  .aa {
    width: 10%;
  }
</style>