<?php

use Picqer\Barcode\BarcodeGenerator;
use Picqer\Barcode\BarcodeGeneratorPNG;

/* @var $this yii\web\View */
/* @var $model app\models\Essay */

$barcode = new BarcodeGeneratorPNG();
?>
<div style="text-align: center;margin: 10px 0">
  <div style="text-align: center;font-size: 0.5em;"><?= $model->title ?></div>
  <img style="width: 98%;height: 0.5in"
       src="data:image/png;base64, <?= base64_encode($barcode->getBarcode($model->barcode, BarcodeGenerator::TYPE_CODE_39)) ?>"/>
  <div style="text-align: center;font-size: 0.5em;">ສມປຊ - <?= $model->barcode ?></div>
</div>

<script>
  window.print();
  // window.history.go(-1);
</script>
<style>
  * {
    font-family: 'Phetsarath OT', 'Saysettha OT';
  }
</style>