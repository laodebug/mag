<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 10/12/18
 * Time: 11:21 PM
 */

use app\components\EssayDetailView;
use app\models\Employee;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Essay */
/* @var $history app\models\History */

/* @var $form yii\widgets\ActiveForm */

$this->title = 'ຮັບບົດຄວາມ ທີ່ສົ່ງຄືນປັບປຸງ';
$employees1 = Employee::find()->where(['level_id' => 2, 'deleted' => 0])->all();
$employees2 = Employee::find()->where(['level_id' => 3, 'deleted' => 0])->all();
?>
    <div class="box box-solid">
        <div class="box-body">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="row">
                <div class="col-sm-5">
                    <?= EssayDetailView::widget(['model' => $model]) ?>
                </div>
                <div class="col-sm-7">
                    <div class="col-sm-12 well">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php
                                if (isset($model->employee)) {
                                    echo "<label class=''>ບັນນາໂດຍ: &nbsp;</label>";
                                    echo "<h4>" . $model->employee->name . "</h4>";
                                }
                                ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($history, 'file')->fileInput(['class' => 'form-control input-lg'])->label('ຟາຍບົດຄວາມ') ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?= $form->field($history, 'status_id')->label('ທິດທາງ')
                                    ->dropDownList([
                                        '2' => 'ສົ່ງໃຫ້ ບກ ຂັ້ນຕົ້ນ',
                                        '4' => 'ສົ່ງໃຫ້ ບກ ຂັ້ນຜູ້ຊີ້ນຳ',
                                        '5' => 'ບົດລໍຖ້າລົງ'
                                    ], ['class' => 'form-control input-lg']) ?>
                            </div>
                            <div class="col-sm-6">
                                <div class="row" id="forward1">
                                    <div class="col-sm-12">
                                        <?= $form->field($history, 'employee_id')->label('ບກ ຂັ້ນຕົ້ນ')
                                            ->dropDownList(ArrayHelper::map($employees1, 'id', 'name'), ['class' => 'form-control input-lg']) ?>
                                    </div>
                                </div>
                                <div class="row" id="forward2">
                                    <div class="col-sm-12">
                                        <?= $form->field($history, 'employee_id')->label('ບກ ຂັ້ນຜູ້ຊີ້ນຳ')
                                            ->dropDownList(ArrayHelper::map($employees2, 'id', 'name'), ['class' => 'form-control input-lg editors']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->field($history, 'comment')
                                    ->textarea(['placeholder' => 'ຄຳຄິດເຫັນ',
                                        'rows' => 5]) ?>
                            </div>
                            <div class="col-sm-12">
                                <div class="text-center">
                                    <button class="btn btn-primary btn-lg"><i class="fa fa-save"></i> ບັນທຶກ</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <div id="webcam">
                        <video id="video" style="width:100%;height:100%" autoplay></video>
                    </div>
                </div>
                <div class="col-sm-7 text-center">
                    <div class="row" id="result"></div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php
$this->registerJs("
var video = document.querySelector('video');
var scale = 1.5;
var index = 0;

$('#history-status_id').change(function() {
  var v = $(this).val();
  $('#forward1, #forward2').hide();
  $('#forward1 select, #forward2 select').prop(\"disabled\", true)
  if(v === '2') {
    $('#forward1').show();
    $('#forward1 select').prop(\"disabled\", false);
  } else if(v === '4') {
    $('#forward2').show();
    $('#forward2 select').prop(\"disabled\", false);
  }
});
$('#history-status_id').change();

if(navigator.mediaDevices)
navigator.mediaDevices.getUserMedia({video: true}).then(function(res) {
  video.srcObject = res;
  video.onclick = function(r) {
    index++;
    var canvas = document.createElement('canvas');
        canvas.width = video.videoWidth * scale;
        canvas.height = video.videoHeight * scale;
        canvas.getContext('2d')
              .drawImage(video, 0, 0, canvas.width, canvas.height);        
        
        var img = $('<img style=\'width: 100%\'>');
        img.data('index', index);
        img.attr('src', canvas.toDataURL());
        
        var btn = $('<button class=\'btn btn-danger btn-block\' type=\'button\'><i class=\'fa fa-trash\'></i></button>');
        btn.css('margin-bottom', '10px');
        btn.data('index', index);
        btn.click(function(e) {
          console.log(e);
          var index = $(this).data('index');
          $('#row' + index).remove();
        });
        
        var input = $('<input>');
        input.attr('type','hidden');
        input.attr('name','History[photos]['+index+']');
        input.val(canvas.toDataURL());
        input.type='hidden';
        input.value = img.src;
        
        var item = $('<div class=\'col-sm-4\' id=\"row'+index+'\">');
        item.append(img)
        .append(btn)
        .append(input);
        $('#result').append(item);              
  };
});
");
