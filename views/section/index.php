<?php

use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Section */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ກົມກອງ';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
    <div class="box-body">
        <p class="pull-right">
            <?= Html::a('<i class="fa fa-plus"></i>', ['section/create'], [
                'class' => 'btn btn-success btn-lg',
                'data-toggle' => "tooltip",
                'data-placement' => "top",
                'title' => "ເພີ່ມ"
            ]) ?>
        </p>
        <?php
        try {
            echo GridView::widget([
                'headerRowOptions' => [
                    'style' => 'background-color: #cccccc'
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'emptyText' => 'ບໍ່ມີຂໍ້ມູນ',
                'summary' => "ສະແດງ {begin} ຫາ {end} ຈາກທັງໝົດ {totalCount}",
                'columns' => [
                    [
                        'class' => SerialColumn::className(),
                        'options' => [
                            'style' => 'width: 50px'
                        ]
                    ],
                    [
                        'attribute' => 'name',
                        'label' => 'ພາກສ່ວນ',
                        'filterInputOptions' => [
                            'class' => 'form-control input-lg',
                            'placeholder' => 'ຊອກຫາ'
                        ]
                    ],
                    [
                        'label' => '',
                        'format' => 'raw',
                        'options' => [
                            'style' => 'width: 150px;'
                        ],
                        'value' => function ($data) {
                            return Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $data->id], [
                                'class' => 'btn btn-primary btn-lg',
                                'data-toggle' => "tooltip",
                                'data-placement' => "top",
                                'title' => "ແກ້ໄຂຂໍ້ມູນ"
                            ])
                                . ' '
                                . Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $data->id], [
                                    'data-method' => 'post', 'class' => 'btn btn-danger btn-lg',
                                    'data-toggle' => "tooltip",
                                    'data-placement' => "top",
                                    'title' => "ລຶບ"
                                ]);
                        }
                    ],
                ]
            ]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        ?>
    </div>
</div>