<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Section */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-sm-4">
        <?php $form = ActiveForm::begin(); ?>
        <?= \app\components\MyTextInput::r($form, $model, 'name') ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'ບັນທຶກ'), ['class' => 'btn btn-success btn-lg']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>