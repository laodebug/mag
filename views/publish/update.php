<?php

use app\components\MyForm;

/* @var $this yii\web\View */
/* @var $model app\models\Publish */

$this->title = Yii::t('app', 'ແກ້ໄຂການລົງວາລະສານ: ເດືອນ: ' . $model->month . ' ປີ' . $model->year, [
    'nameAttribute' => '' . $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Publishes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
echo MyForm::r($this, $model);