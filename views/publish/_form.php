<?php

use app\components\MyTextInput;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Publish */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-sm-4">
        <?php $form = ActiveForm::begin(); ?>
        <?= MyTextInput::number($form, $model, 'month') ?>
        <?= MyTextInput::number($form, $model, 'year') ?>
        <?= $form->field($model, 'user_id')
            ->dropDownList(ArrayHelper::map(User::find()->where('deleted=0')->all(), 'id', 'username'), [
                'class' => 'form-control input-lg'
            ]) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'ບັນທຶກ'), ['class' => 'btn btn-success btn-lg']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
