<?php

use app\components\MyColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PublishSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "ບົດລົງວາລະສານ";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
  <div class="box-body">
      <?php try {
          echo GridView::widget([
              'dataProvider' => $dataProvider,
              'filterModel' => $searchModel,
              'emptyText' => 'ບໍ່ມີຂໍ້ມູນ',
              'headerRowOptions' => [
                  'style' => 'background-color: #cccccc'
              ],
              'summary' => "ສະແດງ {begin} ຫາ {end} ຈາກທັງໝົດ {totalCount}",
              'columns' => [
                  ['class' => 'yii\grid\SerialColumn'],
                  'year',
                  'month',
                  [
                      'attribute' => 'user_id',
                      'value' => function ($data) {
                          return $data->user->username;
                      }
                  ],
                  [
                      'label' => 'ຈຳນວນບົດຄວາມ',
                      'value' => function ($data) {
                          return count($data->publishDetails);
                      }
                  ],
                  MyColumn::action(),
              ]
          ]);
      } catch (Exception $ex) {
          print_r($ex);
      }
      ?>
  </div>
</div>
