<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Publish */

$this->title = Yii::t('app', 'Create Publish');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Publishes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publish-create">

  <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
