<?php

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Publish */

$this->title = "ບົດຄວາມລົງວາລະສານ ປະຈຳເດືອນ: " . $model->month . '/' . $model->year;
$this->params['breadcrumbs'][] = ['label' => 'ບົດຄວາມລົງວາລະສານ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?php $form = \yii\bootstrap\ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-sm-12">
                <?php try {
                    $query = $model->getPublishDetails()
                        ->orderBy('position')
                        ->alias('d')
                        ->join('join', 'essay e', 'e.id = d.essay_id and e.deleted=0');
                    echo GridView::widget([
                        'dataProvider' => new ActiveDataProvider(['query' => $query]),
                        'emptyText' => 'ບໍ່ມີຂໍ້ມູນ',
                        'summary' => "ສະແດງ {begin} ຫາ {end} ຈາກທັງໝົດ {totalCount}",
                        'columns' => [
//                      [
//                          'class' => SerialColumn::className(),
//                          'options' => [
//                              'style' => 'width: 50px'
//                          ]
//                      ],
                            [
                                'attribute' => 'position',
                                'format' => 'raw',
                                'options' => [
                                    'style' => 'width: 100px'
                                ],
                                'value' => function ($data) {
                                    return Html::input('text', 'positions[' . $data->essay_id . ']', $data->position,
                                        [
                                            'class' => 'form-control input-lg'
                                        ]);
                                }
                            ],
                            [
                                'attribute' => 'essay_id',
                                'value' => function ($data) {
                                    return $data->essay->title;
                                }
                            ],
                            [
                                'label' => 'ຊື່ເຈົ້າຂອງ',
                                'value' => function ($data) {
                                    return $data->essay->name;
                                }
                            ],
                            [
                                'label' => 'ພາກສ່ວນ',
                                'value' => function ($data) {
                                    return $data->essay->source;
                                }
                            ],
                            [
                                'label' => 'ກົມກອງ',
                                'value' => function ($data) {
                                    return $data->essay->place;
                                }
                            ],
                            [
                                'label' => '',
                                'format' => 'raw',
                                'options' => [
                                    'style' => 'width: 150px'
                                ],
                                'value' => function ($data) use ($model) {
                                    return
                                        Html::a('<i class="fa fa-eye"></i>', ['essay/view', 'id' => $data->essay_id], [
                                            'class' => 'btn btn-info btn-sm',
                                            'target' => '_blank',
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "top",
                                            'title' => "ດາວໂຫຼດຟາຍ"
                                        ])
                                        .
                                        Html::a('<i class="fa fa-download"></i>', 'uploads/' . $data->essay->filename, [
                                            'class' => 'btn btn-success btn-sm',
                                            'target' => '_blank',
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "top",
                                            'title' => "ດາວໂຫຼດຟາຍ"
                                        ])
                                        .
                                        Html::a('<i class="fa fa-trash"></i>', ['deletedetail', 'publish_id' => $model->id, 'essay_id' => $data->essay_id], [
                                            'data-method' => 'post',
                                            'class' => 'btn btn-danger btn-sm',
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "top",
                                            'title' => "ລຶບຂໍ້ມູນ"
                                        ]);
                                }
                            ],
                        ]
                    ]);
                } catch (Exception $exception) {
                    echo $exception->getMessage();
                } ?>
            </div>
            <div class="col-sm-12">
                <button class="btn btn-primary btn-lg" type="submit">
                    <i class="fa fa-save"></i> ບັນທຶກ
                </button>
            </div>
        </div>
        <?php \yii\bootstrap\ActiveForm::end(); ?>
    </div>
</div>
