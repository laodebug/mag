<?php

use app\components\MyForm;

/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = 'ແກ້ໄຂ ບົດນຳ: ';
$this->params['breadcrumbs'][] = ['label' => 'Chapters', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
echo MyForm::r($this, $model);