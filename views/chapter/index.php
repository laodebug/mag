<?php

use app\components\MyGridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ບົດນຳ';
$this->params['breadcrumbs'][] = $this->title;
$cols = [
    ['attribute' => 'title'],
    [
        'attribute' => 'category_id',
        'value' => function ($data) {
            return $data->category->name;
        }
    ],
    [
        'attribute' => 'hidden',
        'label' => 'ສະແດງ',
        'value' => function($data) {
            return $data->hidden ? 'No' : 'Yes';
        }
    ],
];
echo MyGridView::r($dataProvider, $searchModel, $cols, 0);
