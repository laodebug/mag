<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Category;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= \app\components\MyTextInput::r($form, $model, 'title') ?>
    <?= $form->field($model, 'category_id')
        ->dropDownList(ArrayHelper::map(Category::find()->all(), 'id', 'name'), [
            'class' => 'form-control input-lg'
        ])
        ->label('ປະເພດບົດ')
    ?>
    <?= \app\components\MyTextInput::r($form, $model, 'author') ?>
    <?= $form->field($model, 'content')->widget(CKEditor::className(), Yii::$app->params['ckoptions']); ?>
    <div class="form-group">
        <?= Html::submitButton('ບັນທຶກ', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>