<?php

use app\components\MyForm;


/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = 'ເພີ່ມ ບົດນຳ';
$this->params['breadcrumbs'][] = ['label' => 'Chapters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo MyForm::r($this, $model);
