<?php

use app\components\MyDropdown;
use app\components\MyTextInput;
use app\models\Section;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SectionSub */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="col-sm-4">
        <?php $form = ActiveForm::begin(); ?>
        <?= MyTextInput::r($form, $model, 'name') ?>
        <?= MyDropdown::r($form, $model, 'section_id', Section::find()->all()) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'ບັນທຶກ'), ['class' => 'btn btn-success btn-lg']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
