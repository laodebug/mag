<?php

use app\components\MyForm;


/* @var $this yii\web\View */
/* @var $model app\models\Status */

$this->title = 'ເພີ່ມ ກົມກອງ';
$this->params['breadcrumbs'][] = ['label' => 'ສະຖານະ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo MyForm::r($this, $model);