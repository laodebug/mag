<?php

use app\components\MyDatePicker;
use dosamigos\ckeditor\CKEditor;
use nickdenry\ckeditorRoxyFileman\RoxyFileManager;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use navatech\roxymce\assets\BootstrapTreeviewAsset;
use navatech\roxymce\assets\FancyBoxAsset;
use navatech\roxymce\assets\FontAwesomeAsset;
use navatech\roxymce\assets\LazyLoadAsset;
use yii\helpers\Url;

FontAwesomeAsset::register($this);
LazyLoadAsset::register($this);
FancyBoxAsset::register($this);
BootstrapTreeviewAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Magazine */
/* @var $form yii\widgets\ActiveForm */

$months = [];
for ($i = 1; $i < 13; $i++) {
    $months[$i] = $i;
}
?>

<div class="magazine-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <div class="row">
        <div class="col-sm-2">
            <?= $form->field($model, 'for_month')->dropDownList($months, []) ?>
        </div>
        <div class="col-sm-2">
            <?= $form->field($model, 'for_year')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-sm-8">
            <?php // echo $form->field($model, 'author')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= MyDatePicker::r($form, $model, 'from_date') ?>
        </div>
        <div class="col-sm-6">
            <?php //echo MyDatePicker::r($form, $model, 'to_date') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8">
                    <?= $form->field($model, 'cover_image') ?>
                </div>
                <div class="col-sm-4" style="padding-top: 24px;">
                    <a class="btn btn-primary" data-toggle="modal" href="" data-target="#modal-cover" data-remote="false">ເລືອກຮູບໜ້າປົກ</a>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-8">
                    <?= $form->field($model, 'index_image') ?>
                </div>
                <div class="col-sm-4" style="padding-top: 24px;">
                    <a class="btn btn-primary" data-toggle="modal" href="" data-target="#modal-index" data-remote="false">ເລືອກຮູບສາລະບານ</a>
                </div>
            </div>
        </div>
    </div>
    <?php
    echo $form->field($model, 'subtitle')->widget(CKEditor::class, Yii::$app->params['ckoptions']);
    ?>
    <?php
    echo $form->field($model, 'content')->widget(CKEditor::class, Yii::$app->params['ckoptions']);
    ?>    
    <?= $form->field($model, 'content_file'); ?>
    <?= $form->field($model, 'hidden')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<div class="modal modal-roxy fade" id="modal-cover">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <iframe src="<?= Url::to([
                                            '/roxymce/default',
                                            'type'   => 'image',
                                            'input'  => 'magazine-cover_image',
                                            'dialog' => 'modal',
                                        ]) ?>" height="470px" width="100%"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-roxy" id="modal-index">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <iframe src="<?= Url::to([
                                            '/roxymce/default',
                                            'type'   => 'image',
                                            'input'  => 'magazine-index_image',
                                            'dialog' => 'modal',
                                        ]) ?>" height="470px" width="100%"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>