<?php

use app\components\MyForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Magazine */

$this->title = 'ແກ້ໄຂ ບົດວາລະສານ: ';
$this->params['breadcrumbs'][] = ['label' => 'Chapters', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
echo MyForm::r($this, $model);