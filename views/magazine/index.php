<?php

use app\components\MyGridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ວາລະສານ';
$this->params['breadcrumbs'][] = $this->title;
$cols = [
    [
        'attribute' => 'for_month',
        'value' => function ($data) {
            return $data->for_year . "/" . $data->for_month;
        }
    ],
    ['attribute' => 'title'],
    ['attribute' => 'author'],
    [
        'attribute' => 'hidden',
        'label' => 'ສະແດງ',
        'value' => function ($data) {
            return $data->hidden ? 'No' : 'Yes';
        }
    ],
];
echo MyGridView::r($dataProvider, $searchModel, $cols, 0);
