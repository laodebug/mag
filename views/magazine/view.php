<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Magazine */

$this->params['breadcrumbs'][] = ['label' => 'Magazines', 'url' => ['index']];
\yii\web\YiiAsset::register($this);
?>
<div class="magazine-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <h1><?= Html::encode($model->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'cover_image:html',
            'index_image:html',
            'content_file:html',
            'content:html',
            'for_month',
            'for_year',
            [
                'attribute' => 'hidden',
                'value' => function($data) {
                    return $data->hidden ? 'Yes' : 'No';
                }
            ],
            [
                'attribute' => 'user_id',
                'value' => function($data) {
                    return $data->user->username;
                }
            ],
        ],
    ]) ?>

</div>
