<?php

use app\components\MyTextInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= MyTextInput::r($form, $model, 'username', 1) ?>
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'class' => 'form-control input-lg']) ?>
    <?= $form->field($model, 'role')->dropDownList(Yii::$app->params['roles']) ?>
  <div class="form-group">
      <?= Html::submitButton('ບັນທຶກ', ['class' => 'btn btn-success']) ?>
  </div>

    <?php ActiveForm::end(); ?>

</div>
