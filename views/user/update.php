<?php

use app\components\MyForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'ແກ້ໄຂ ຂໍ້ມູນຜູ້ໃຊ້: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'ຜູ້ໃຊ້', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'ແກ້ໄຂ ' . $model->username;
echo MyForm::r($this, $model);