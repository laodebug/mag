<?php

use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ຜູ້ໃຊ້';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <p class="pull-right">
            <?= Html::a('<i class="fa fa-plus"></i>', ['user/create'], [
                'class' => 'btn btn-success',
                'data-toggle' => "tooltip",
                'data-placement' => "top",
                'title' => "ເພີ່ມ"
            ]) ?>
        </p>
        <?php
        try {
            echo GridView::widget([
                'headerRowOptions' => [
                    'style' => 'background-color: #cccccc'
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'emptyText' => 'ບໍ່ມີຂໍ້ມູນ',
                'summary' => "ສະແດງ {begin} ຫາ {end} ຈາກທັງໝົດ {totalCount}",
                'columns' => [
                    [
                        'class' => SerialColumn::className(),
                        'options' => [
                            'style' => 'width: 50px'
                        ]
                    ],
                    [
                        'attribute' => 'username',
                        'filterInputOptions' => [
                            'class' => 'form-control input-lg',
                            'placeholder' => 'ຊອກຫາ'
                        ]
                    ],
                    [
                        'attribute' => 'password',
                        'filter' => false,
                        'value' => function ($data) {
                            // return print_r(Yii::$app->user->identity->role, true);
                            if (Yii::$app->user->identity->role == 'ຜູ້ດູແລລະບົບ') {
                                return $data->password;
                            } else {
                                return '';
                            }
                        }
                    ],
                    [
                        'attribute' => 'role',
                        'filter' => ['ທົ່ວໄປ' => 'ທົ່ວໄປ'],
                        //                      'filterInputOptions' => [
                        //                          'class' => 'form-control input-lg',
                        //                          'placeholder' => 'ຊອກຫາ'
                        //                      ]
                    ],
                    [
                        'attribute' => 'deleted',
                        'label' => 'ສະຖານະ',
                        'format' => 'raw',
                        'filter' => ['ປົກກະຕິ', 'ລຶບແລ້ວ'],
                        'filterInputOptions' => [
                            'class' => 'form-control input-lg',
                        ],
                        'value' => function ($data) {
                            return $data->deleted == 0 ? '<label class="label label-success">ປົກກະຕິ</label>' : '<label class="label label-danger">ລຶບແລ້ວ</label>';
                        }
                    ],
                    [
                        'label' => '',
                        'format' => 'raw',
                        'options' => [
                            'style' => 'width: 150px;'
                        ],
                        'value' => function ($data) {
                            return Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $data->id], [
                                'class' => 'btn btn-primary',
                                'data-toggle' => "tooltip",
                                'data-placement' => "top",
                                'title' => "ແກ້ໄຂຂໍ້ມູນ"
                            ])
                                . ' '
                                . ($data->role == 'ຜູ້ດູແລລະບົບ' ? '' : Html::a('<i class="fa fa-check"></i>', ['active', 'id' => $data->id], [
                                    'class' => 'btn btn-success',
                                    'data-toggle' => "tooltip",
                                    'data-placement' => "top",
                                    'title' => "ເປີດຄືນ"
                                ]))
                                . ($data->role == 'ຜູ້ດູແລລະບົບ' ? '' : Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $data->id], [
                                    'data-method' => 'post', 'class' => 'btn btn-danger',
                                    'data-toggle' => "tooltip",
                                    'data-placement' => "top",
                                    'title' => "ລຶບ"
                                ]));
                        }
                    ],
                ]
            ]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        ?>
    </div>
</div>