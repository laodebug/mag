<?php

use app\components\MyForm;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'ເພີ່ມ ຂໍ້ມູນຜູ້ໃຊ້';
$this->params['breadcrumbs'][] = ['label' => 'ຜູ້ໃຊ້', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo MyForm::r($this, $model);