<?php

/* @var $this yii\web\View */

use app\models\Level;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $searchModel app\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ຜູ້ກວດກາ';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
  <div class="box-body">
    <p class="pull-right">
        <?= Html::a('<i class="fa fa-plus"></i>', ['employee/create'], ['class' => 'btn btn-success btn-lg']) ?>
    </p>
      <?php try {
          echo GridView::widget([
              'headerRowOptions' => [
                  'style' => 'background-color: #cccccc'
              ],
              'dataProvider' => $dataProvider,
              'filterModel' => $searchModel,
              'emptyText' => 'ບໍ່ມີຂໍ້ມູນ',
              'summary' => "ສະແດງ {begin} ຫາ {end} ຈາກທັງໝົດ {totalCount}",
              'columns' => [
                  [
                      'class' => SerialColumn::className(),
                      'options' => [
                          'style' => 'width: 50px'
                      ]
                  ],
                  [
                      'attribute' => 'name',
                      'filterInputOptions' => [
                          'class' => 'form-control input-lg',
                          'placeholder' => 'ຊອກຫາ'
                      ]
                  ],
                  [
                      'attribute' => 'tel',
                      'filterInputOptions' => [
                          'class' => 'form-control input-lg',
                          'placeholder' => 'ຊອກຫາ'
                      ]
                  ],
                  [
                      'attribute' => 'level_id',
                      'filter' => ArrayHelper::map(Level::find()->where('deleted=0')->all(), 'id', 'name'),
                      'filterInputOptions' => [
                          'class' => 'form-control input-lg',
                          'prompt' => 'ທັງໝົດ'
                      ],
                      'value' => function ($data) {
                          if ($data->level)
                              return $data->level->name;
                      }
                  ],
                  [
                      'label' => '',
                      'format' => 'raw',
                      'value' => function ($data) {
                          return Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $data->id], [
                                  'class' => 'btn btn-primary btn-lg',
                                  'data-toggle' => "tooltip",
                                  'data-placement' => "top",
                                  'title' => "ແກ້ໄຂຂໍ້ມູນ"
                              ])
                              . ' '
                              . Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $data->id], [
                                  'class' => 'btn btn-danger btn-lg',
                                  'data-toggle' => "tooltip",
                                  'data-placement' => "top",
                                  'data' => [
                                      'confirm' => 'ທ່ານຕ້ອງການລຶບແທ້ບໍ່?',
                                      'method' => 'post',
                                  ],
                                  'title' => "ລຶບ"
                              ]);
                      }
                  ],
              ]
          ]);
      } catch (Exception $exception) {
          echo $exception->getMessage();
      } ?>
  </div>
</div>
