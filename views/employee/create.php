<?php

use app\components\MyForm;


/* @var $this yii\web\View */
/* @var $model app\models\Employee */

$this->title = 'ເພີ່ມຂໍ້ມູນ ບກ';
$this->params['breadcrumbs'][] = ['label' => 'Employees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo MyForm::r($this, $model);