<?php

use app\components\MyTextInput;
use app\models\Level;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employee-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= MyTextInput::r($form, $model, 'name', 1) ?>
    <?= MyTextInput::r($form, $model, 'tel') ?>
    <?= $form->field($model, 'level_id')
        ->dropDownList(ArrayHelper::map(Level::find()->where('id>1 and deleted=0')->all(), 'id', 'name'), [
            'prompt' => 'ກະລຸນາເລືອກ',
            'class' => 'form-control select2'
        ])->label('ຂັ້ນ') ?>

  <div class="form-group">
      <?= Html::submitButton('ບັນທຶກ', ['class' => 'btn btn-primary']) ?>
  </div>

    <?php ActiveForm::end(); ?>

</div>
