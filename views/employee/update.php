<?php

use app\components\MyForm;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */

$this->title = 'ແກ້ໄຂຂໍ້ມູນ ບກ: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'ຂໍ້ມູນ ບກ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'ແກ້ໄຂຂໍ້ມູນ';
echo MyForm::r($this, $model);