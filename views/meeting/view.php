<?php

/* @var $this yii\web\View */
/* @var $model app\models\Meeting */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Meetings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<p>ວັນທີປະຊຸມ: <?= date_format(new DateTime($model->meeting_date), 'd/m/Y') ?></p>
<hr>
<div ng-app="mag" ng-controller="participants">
  <div class="well">
    <div class="row">
      <div class="col-sm-12">
        <div class="input-group input-group-lg">
          <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
          <input ng-model="code" type="text" class="form-control" autofocus="true"
                 placeholder="ລະຫັດນັກສຶກສາ" ng-keyup="join($event)">
          <span class="input-group-btn">
              <button type="submit" class="btn btn-primary btn-flat">
                <i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="box">
        <div class="box-body">
          <div class="row">
            <div class="col-sm-3 text-center" ng-if="student">
              <div class="box box-success">
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" ng-src="students/{{student.photo}}">
                  <h3 class="profile-username text-center">{{studenet.code}}</h3>
                  <p class="text-muted text-center">{{studenet.firstname}} {{student.lastname}}</p>
                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>ເວລາເຂົ້າ</b> <a class="pull-right">{{student.checkin}}</a>
                    </li>
                    <li class="list-group-item">
                      <b class="badge-success">ເວລາອອກ</b> <a class="pull-right">{{student.checkout}}</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="row" ng-if="students">
            <div class="col-sm-3 text-center" ng-repeat="s in students">
              <div class="box box-primary">
                <div class="box-body box-profile">
                  <div class="row">
                    <div class="col-sm-6">
                      <img ng-if="s.photo" class="profile-user-img img-responsive img-circle"
                           ng-src="students/{{s.photo}}">
                    </div>
                    <div class="col-sm-6">
                      <h4 class="profile-username text-center">{{s.code}}</h4>
                      <p class="text-muted text-center">{{s.firstname}} {{s.lastname}}</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6 bg-green-active">
                      {{s.checkin}}
                    </div>
                    <div class="col-sm-6 bg-red-active">
                      {{s.checkout}}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <table class="table" ng-if="false">
            <thead>
            <tr>
              <th>#</th>
              <th>ລະຫັດ</th>
              <th>ຊື່</th>
              <th>ນາມສະກຸນ</th>
              <th>ເວລາເຂົ້າ</th>
              <th>ເວລາອອກ</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="s in students">
              <td>{{$index + 1}}</td>
              <td>{{s.code}}</td>
              <td>{{s.firstname}}</td>
              <td>{{s.lastname}}</td>
              <td>{{s.checkin}}</td>
              <td>{{s.checkout}}</td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  var app = angular.module('mag', []);
  var baseurl = "index.php?r=participants/";
  app.controller('participants', function ($scope, $http) {
    $scope.code = null;
    $scope.student = null;
    $scope.students = [];

    $scope.get = function () {
      $http.get(baseurl + "get&id=<?= $model->id ?>")
        .then(function (response) {
          $scope.students = response.data;
        });
    };
    $scope.get();
    $scope.join = function ($e) {
      if ($e.keyCode === 13)
        if ($scope.code) {
          const code = $scope.code;
          $scope.code = null;
          $http.get(baseurl + "join&id=<?= $model->id ?>&code=" + code)
            .then(function (response) {
              $scope.student = response.data['student'];
              $scope.students = response.data['students'];
              setTimeout(function () {
                $scope.student = null;
              }, 5000)
            });
        }
    };
  });
</script>
