<?php

use app\components\MyGridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MeetingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ກອງປະຊຸມ';
$this->params['breadcrumbs'][] = $this->title;
$cols = [
    ['attribute' => 'meeting_date'],
    ['attribute' => 'title'],
    ['attribute' => 'remark']
];
echo MyGridView::r($dataProvider, $searchModel, $cols);