<?php

use app\components\MyForm;

/* @var $this yii\web\View */
/* @var $model app\models\Meeting */

$this->title = 'ແກ້ໄຂຂໍ້ມູນ ກອງປະຊຸມ: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Meetings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
echo MyForm::r($this, $model);