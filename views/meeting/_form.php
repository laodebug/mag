<?php

use app\components\MyDatePicker;
use app\components\MyTextInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Meeting */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="meeting-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= MyDatePicker::r($form, $model, 'meeting_date') ?>
    <?= MyTextInput::r($form, $model, 'title') ?>
    <?= $form->field($model, 'remark')->textarea(['rows' => 6]) ?>
  <div class="form-group">
      <?= Html::submitButton('ບັນທຶກ', ['class' => 'btn btn-success input-lg']) ?>
  </div>

    <?php ActiveForm::end(); ?>

</div>
