<?php

use app\components\MyForm;

/* @var $this yii\web\View */
/* @var $model app\models\Meeting */

$this->title = 'ເພີ່ມ ກອງປະຊຸມ';
$this->params['breadcrumbs'][] = ['label' => 'ກອງປະຊຸມ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo MyForm::r($this, $model);