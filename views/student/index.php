<?php

use app\components\MyGridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ນັກສຶກສາ';
$this->params['breadcrumbs'][] = $this->title;
$cols = [
    ['attribute' => 'code'],
    ['attribute' => 'firstname'],
    ['attribute' => 'lastname'],
    ['attribute' => 'tel']
];
echo MyGridView::r($dataProvider, $searchModel, $cols);