<?php

/* @var $this yii\web\View */

use app\components\MyForm;

/* @var $model app\models\Student */

$this->title = 'ແກ້ໄຂຂໍ້ມູນ ນັກສຶກສາ: ' . $model->code;
$this->params['breadcrumbs'][] = ['label' => 'ນັກສຶກສາ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'ແກ້ໄຂ';
echo MyForm::r($this, $model);