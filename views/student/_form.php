<?php

use app\components\MyTextInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Student */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
  <div class="row">
    <div class="col-sm-4">
        <?php if (isset($model->photo)) echo Html::img('students/' . $model->photo); ?>
        <?php if (!isset($model->photo))
            echo $form->field($model, 'uploader')->fileInput();
        ?>
    </div>
    <div class="col-sm-8">
        <?= MyTextInput::r($form, $model, 'code') ?>
        <?= MyTextInput::r($form, $model, 'firstname') ?>
        <?= MyTextInput::r($form, $model, 'lastname') ?>
        <?= MyTextInput::r($form, $model, 'dob') ?>
        <?= MyTextInput::r($form, $model, 'tel') ?>
      <div class="form-group">
          <?= Html::submitButton('ບັນທຶກ', ['class' => 'btn btn-success btn-lg']) ?>
      </div>

    </div>
  </div>
    <?php ActiveForm::end(); ?>

</div>
