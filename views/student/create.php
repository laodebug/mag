<?php

use app\components\MyForm;

/* @var $this yii\web\View */
/* @var $model app\models\Student */

$this->title = 'ເພີ່ມຂໍ້ມູນ ນັກສຶກສາ';
$this->params['breadcrumbs'][] = ['label' => 'Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo MyForm::r($this, $model);