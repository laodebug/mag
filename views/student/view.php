<?php

/* @var $this yii\web\View */
/* @var $model app\models\Student */

$this->title = "ຂໍ້ມູນນັກສຶກສາ " . $model->code;
$this->params['breadcrumbs'][] = ['label' => 'ນັກສຶກສາ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
  <div class="col-sm-6 col-sm-offset-3">
    <div class="box box-success">
      <div class="box-body box-profile">
          <?= \yii\helpers\Html::img("students/" . $model->photo, ['class' => 'profile-user-img img-responsive img-circle']) ?>
        <h3 class="profile-username text-center"><?= $model->code ?></h3>
        <p class="text-muted text-center"><?= $model->firstname ?> <?= $model->lastname ?></p>
        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>ວັນເດືອນປີເກີດ</b> <a class="pull-right"><?= $model->dob ?></a>
          </li>
          <li class="list-group-item">
            <b>ເບິໂທ</b> <a class="pull-right"><?= $model->tel ?></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
