<?php

use app\components\MyForm;

/* @var $this yii\web\View */
/* @var $model app\models\Status */

$this->title = 'ແກ້ໄຂ ສະຖານະ: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'ສະຖານະ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'ແກ້ໄຂ';
echo MyForm::r($this, $model);