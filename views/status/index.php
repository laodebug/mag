<?php

use app\components\MyGridView;
use app\models\Status;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ສະຖານະ ບົດຄວາມ';
$this->params['breadcrumbs'][] = $this->title;
$cols = [
    ['attribute' => 'name'],
    [
        'attribute' => 'color',
        'format' => 'raw',
        'value' => function ($data) {
            return '<div class="color" style="background-color: ' . $data->color . '"></div>';
        }
    ]
];
$labels = new Status();
$labels = $labels->attributeLabels();
echo MyGridView::r($dataProvider, $searchModel, $cols, 0, 1, 1, 1, $labels);