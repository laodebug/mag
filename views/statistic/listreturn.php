<?php

/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 10/13/18
 * Time: 12:55 AM
 */

use yii\bootstrap\Html;

$this->title = $title;
?>

<div class="box">
    <div class="box-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ລດ</th>
                    <th>ຊື່ບົດ</th>
                    <th class="text-right">ຈຳນວນຄັ້ງ</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($models as $i => $m) : ?>
                <tr>
                    <td><?= $i + 1 ?></td>
                    <td><?= Html::a($m['title'], ['essay/view', 'id' => $m['id']]) ?></td>
                    <td class="text-right">
                        <?= Html::a(number_format($m['count']), ['essay/view', 'id' => $m['id']]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</div>