<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 10/13/18
 * Time: 12:55 AM
 */

$this->title = $title;
?>

<div class="box">
  <div class="box-body">
    <table class="table table-bordered">
      <thead>
      <tr>
        <th style="width: 50px;">ລດ</th>
        <th style="width: 200px;">ວັນທີ</th>
        <th>ຊື່ບົດ</th>
        <th>ເຈົ້າຂອງບົດ</th>
        <th>ຄຳຄິດເຫັນ</th>
        <th style="width: 100px;"></th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ($models as $i => $m) : ?>
        <tr>
          <td><?= $i + 1 ?></td>
          <td><?= $m->created_date ?></td>
          <td><?= isset($m->essay) ? $m->essay->title : '' ?></td>
          <td><?= isset($m->essay) ? $m->essay->name : '' ?></td>
          <td><?= $m->comment ?></td>
          <td class="text-center">
              <?= \yii\helpers\Html::a('<i class="fa fa-eye"></i>', ['essay/view', 'id' => $m->essay_id], [
                  'class' => 'btn btn-info btn-lg'
              ]) ?>
          </td>
        </tr>
      <?php endforeach; ?>
      </tbody>
    </table>

  </div>
</div>