<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 10/13/18
 * Time: 12:55 AM
 */

use yii\widgets\ActiveForm;

$this->title = 'ສະຖິຕິ';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
  <div class="box-body">
    <div class="row">
      <div class="col-sm-3">
          <?php $form = ActiveForm::begin(['method' => 'get', 'action' => ['statistic/index1']]); ?>
          <?= $form->field($model, 'fromdate')->textInput(['class' => 'form-control input-lg datepicker']) ?>
      </div>
      <div class="col-sm-3">
          <?= $form->field($model, 'todate')->textInput(['class' => 'form-control input-lg datepicker']) ?>
      </div>
      <div class="col-sm-3">
        <div><label>ຊອກຫາ</label></div>
        <button class="btn btn-primary input-lg" type="submit">
          <i class="fa fa-search"></i>
        </button>
      </div>
        <?php ActiveForm::end(); ?>
    </div>
    <hr>

  </div>
</div>
