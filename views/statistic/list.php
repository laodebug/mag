<?php
/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 10/13/18
 * Time: 12:55 AM
 */

$this->title = $title;
?>

<div class="box">
  <div class="box-body">
    <table class="table table-bordered">
      <thead>
      <tr>
        <th>ລດ</th>
        <th>ຊື່ບົດ</th>
        <th>ເຈົ້າຂອງບົດ</th>
        <th>ຊື່ ບກ</th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ($models as $i => $m) : ?>
        <tr>
          <td><?= $i + 1 ?></td>
          <td><?= $m->title ?></td>
          <td><?= $m->name ?></td>
          <td><?= isset($m->employee) ? $m->employee->name : '' ?></td>
        </tr>
      <?php endforeach; ?>
      </tbody>
    </table>

  </div>
</div>