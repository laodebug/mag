<?php

/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 10/13/18
 * Time: 12:55 AM
 */

use yii\bootstrap\Html;

$this->title = $title;
$g = ['M' => 'ທ.', 'F' => 'ນ.'];
?>
<h4>ຍິງ: <?= isset($genders[0]) ? number_format($genders[0]['count']) : 0 ?>,
    ຊາຍ: <?= isset($genders[1]) ? number_format($genders[1]['count']) : 0 ?></h4>
<div class="box">
    <div class="box-body">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>ລດ</th>
                <th>ຊື່ເຈົ້າຂອງບົດຄວາມ</th>
                <th>ບົດຄວາມ</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($models as $i => $m) : ?>
                <tr>
                    <td><?= $i + 1 ?></td>
                    <td><?= $g[$m->gender] . ' ' . $m->name ?></td>
                    <td><?= Html::a($m['title'], ['essay/view', 'id' => $m['id']]) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</div>
