<?php

/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 10/13/18
 * Time: 12:55 AM
 */

use yii\bootstrap\Html;

$this->title = $title . ' ' . $section->name;
?>
<h4>ຍິງ: <?= isset($genders[0]) ? number_format($genders[0]['count']) : 0 ?>, ຊາຍ: <?= isset($genders[1]) ? number_format($genders[1]['count']) : 0?></h4>
<div class="box">
    <div class="box-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ລດ</th>
                    <th>ຊື່ ກົມກອງ</th>
                    <th class="text-right">ຈຳນວນບົດ</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sum = 0;
                foreach ($models as $i => $m) :
                    $sum += $m['count'];
                ?>
                <tr>
                    <td><?= $i + 1 ?></td>
                    <td><?= Html::a($m['name'], ['sectionsub', 'id' => $m['id']]) ?></td>
                    <td class="text-right">
                        <?= Html::a(number_format($m['count']), ['sectionsub', 'id' => $m['id']]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="2" class="text-right">ລວມ</th>
                    <th class="text-right"><?= number_format($sum) ?></th>
                </tr>
            </tfoot>
        </table>

    </div>
</div>
