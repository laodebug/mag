<?php

/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 10/13/18
 * Time: 12:55 AM
 */

use miloschuman\highcharts\Highcharts;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'ສະຖິຕິ';
$this->params['breadcrumbs'][] = $this->title;
$new = 0;
$pendings1 = 0;
$pendings2 = 0;
$return = 0;
$publish = 0;
$published = 0;
$male = 0;
$female = 0;
$cats = [];
if (isset($models))
    foreach ($models as $m) {
        switch ($m->status_id) {
            case 1:
                $new++;
                break;
            case 2:
                $pendings1++;
                break;
            case 3:
                $return++;
                break;
            case 4:
                $pendings2++;
                break;
            case 5:
                $publish++;
                break;
            case 6:
                $published++;
                break;
        }
        if ($m->gender == 'M') $male++;
        if ($m->gender == 'F') $female++;
        if (!isset($cats[$m->category_id]))
            $cats[$m->category_id] = ['value' => 0, 'name' => $m->category->name];
        $cats[$m->category_id]['value']++;
    }

$fromdate = date('d/m/Y', strtotime($model->fromdate));
$todate = date('d/m/Y', strtotime($model->todate));
?>

<div class="box">
    <div class="box-body">
        <div class="well">
            <?php $form = ActiveForm::begin(['method' => 'get', 'action' => ['statistic/index']]); ?>
            <div class="row">
                <div class="col-sm-3">
                    <?= $form->field($model, 'fromdate')
                    ->textInput(['class' => 'form-control input-lg datepicker'])
                    ->label('ຈາກວັນທີ') ?>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($model, 'todate')->textInput(['class' => 'form-control input-lg datepicker'])->label('ຫາວັນທີ') ?>
                </div>
                <div class="col-sm-3">
                    <div><label>ຊອກຫາ</label></div>
                    <button class="btn btn-primary input-lg" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="box box-solid box-primary">
                    <div class="box-body">
                        <div>
                            <?php
                                $data = [];
                                $sum = 0;
                                foreach ($essaybycheckers as $essaybychecker) {
                                    if (!isset($essaybychecker->employee)) continue;
                                    $sum += $essaybychecker->comment;
                                }
                                foreach ($essaybycheckers as $e => $essaybychecker) {
                                    if (!isset($essaybychecker->employee)) continue;
                                    $data[] = [
                                        'name' => $essaybychecker->employee->name . ' ' . number_format($essaybychecker->comment * 100 / $sum, 2) . '%',
                                        'y' => $essaybychecker->comment * 100 / $sum
                                    ];
                                }
                                echo Highcharts::widget([
                                    'options' => [
                                        'credits' => ['enabled' => false],
                                        'chart' => [
                                            'type' => 'pie'
                                        ],
                                        'title' => ['text' => 'ສະຖິຕິບົດຄວາມຕາມ ບກ ບັນນາ <br><small>(ວັນທີ ' . $fromdate . ' ຫາ ' . $todate . ')</small>'],
                                        'series' => [
                                            [
                                                'name' => 'ຈຳນວນບັນນາ',
                                                'data' => $data
                                            ]
                                        ]
                                    ]
                                ]);
                                ?>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                </tr>
                                <tr>
                                    <th>ຊື່ ບກ</th>
                                    <th class="text-right">ຈຳນວນບັນນາ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($essaybycheckers as $i => $essaybychecker) : if (!isset($essaybychecker->employee)) continue; ?>
                                <tr>
                                    <th><?= isset($essaybychecker->employee) ? $essaybychecker->employee->name : '' ?>
                                    </th>
                                    <th class="text-right">
                                        <?= \yii\helpers\Html::a($essaybychecker->comment, [
                                                        'statistic/user',
                                                        'id' => $essaybychecker->employee_id,
                                                        'fromdate' => $model->fromdate,
                                                        'todate' => $model->todate
                                                    ]) ?>
                                    </th>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="box box-solid box-primary">
                    <div class="box-body">
                        <div>
                            <?php
                                $data = [];
                                $sum = $new + $pendings1 + $pendings2 + $return + $publish + $published;
                                if ($sum > 0) {
                                    echo Highcharts::widget([
                                        'options' => [
                                            'credits' => ['enabled' => false],
                                            'chart' => [
                                                'type' => 'pie'
                                            ],
                                            'title' => ['text' => 'ສະຖິຕິບົດຄວາມຕາມປະເພດ <br> <small>(ວັນທີ ' . $fromdate . ' ຫາ ' . $todate . ')</small>'],
                                            'series' => [
                                                [
                                                    'name' => 'ຈຳນວນບົດຄວາມ',
                                                    'data' => [
                                                        ['name' => 'ບົດຄວາມໃໝ່ ' . number_format($new * 100 / $sum, 2) . ' %', 'y' => $new],
                                                        ['name' => 'ຄ້າງນໍາ ບກ ຂັ້ນຕົ້ນ ' . number_format($pendings1 * 100 / $sum, 2) . ' %', 'y' => $pendings1],
                                                        ['name' => 'ຄ້າງນໍາ ບກ ຂັ້ນຜູ້ຊີ້ນຳ' . number_format($pendings2 * 100 / $sum, 2) . ' %', 'y' => $pendings2],
                                                        ['name' => 'ສົ່ງຄືນເຈົ້າຂອງປັບປຸງ ' . number_format($return * 100 / $sum, 2) . ' %', 'y' => $return],
                                                        ['name' => 'ບົດລໍຖ້າລົງ ' . number_format($publish * 100 / $sum, 2), 'y' => $publish],
                                                        ['name' => 'ບົດລົງວາລະສານ ' . number_format($published * 100 / $sum, 2) . ' %', 'y' => $published],
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]);
                                }
                                ?>
                        </div>
                        <hr>
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th>ບົດຄວາມທັງໝົດ</th>
                                    <td class="text-right">
                                        <?= \yii\helpers\Html::a(count($models), ['statistic/all', 'fromdate' => $model->fromdate, 'todate' => $model->todate]) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>ບົດຄວາມໃໝ່</th>
                                    <td class="text-right">
                                        <?= \yii\helpers\Html::a($new, ['statistic/new', 'fromdate' => $model->fromdate, 'todate' => $model->todate]) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>ຄ້າງນໍາ ບກ ຂັ້ນຕົ້ນ</th>
                                    <td class="text-right">
                                        <?= \yii\helpers\Html::a($pendings1, ['statistic/pendings', 'fromdate' => $model->fromdate, 'todate' => $model->todate, 'level' => 1]) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>ຄ້າງນໍາ ບກ ຂັ້ນຜູ້ຊີ້ນຳ</th>
                                    <td class="text-right">
                                        <?= \yii\helpers\Html::a($pendings2, ['statistic/pendings', 'fromdate' => $model->fromdate, 'todate' => $model->todate, 'level' => 2]) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>ສົ່ງຄືນເຈົ້າຂອງປັບປຸງ</th>
                                    <td class="text-right">
                                        <?= \yii\helpers\Html::a($return, ['statistic/return', 'fromdate' => $model->fromdate, 'todate' => $model->todate]) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>ບົດລໍຖ້າລົງ</th>
                                    <td class="text-right">
                                        <?= \yii\helpers\Html::a($publish, ['statistic/publish', 'fromdate' => $model->fromdate, 'todate' => $model->todate]) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>ບົດລົງວາລະສານ</th>
                                    <td class="text-right">
                                        <?= \yii\helpers\Html::a($published, ['statistic/published', 'fromdate' => $model->fromdate, 'todate' => $model->todate]) ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="box box-solid box-primary">
                    <div class="box-body">
                        <div>
                            <?php
                                $data = [];
                                $sum = 0;
                                foreach ($cats as $i => $cat) {
                                    $sum += $cat['value'];
                                }
                                if ($sum > 0) {
                                    foreach ($cats as $i => $cat) {
                                        $data[] = ['name' => $cat['name'] . ' ' . number_format($cat['value'] * 100 / $sum, 2) . ' %', 'y' => $cat['value']];
                                    }
                                    echo Highcharts::widget([
                                        'options' => [
                                            'credits' => ['enabled' => false],
                                            'chart' => [
                                                'type' => 'pie'
                                            ],
                                            'title' => ['text' => 'ສະຖິຕິບົດຄວາມຕາມປະເພດ  <br> <small>(ວັນທີ ' . $fromdate . ' ຫາ ' . $todate . ')</small>'],
                                            'series' => [
                                                [
                                                    'name' => 'ຈຳນວນບົດຄວາມ',
                                                    'data' => $data
                                                ]
                                            ]
                                        ]
                                    ]);
                                }
                                ?>
                        </div>
                        <hr>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>ປະເພດບົດຄວາມ</th>
                                    <th class="text-right">ຈຳນວນ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>ບົດຄວາມທັງໝົດ</th>
                                    <th class="text-right"><?= number_format($sum) ?></th>
                                </tr>
                                <?php
                                    foreach ($cats as $i => $cat) : ?>
                                <tr>
                                    <td><?= $cat['name'] ?></td>
                                    <td class="text-right">
                                        <?= Html::a(number_format($cat['value']), ['statistic/category', 'id' => $i, 'fromdate' => $model->fromdate, 'todate' => $model->todate]) ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="box box-solid box-primary">
                    <div class="box-body">
                        <div>
                            <?php
                                if ($male + $female > 0) {
                                    echo Highcharts::widget([
                                        'options' => [
                                            'credits' => ['enabled' => false],
                                            'chart' => [
                                                'type' => 'pie'
                                            ],
                                            'title' => ['text' => 'ສະຖິຕິບົດຄວາມຕາມເພດ <br> <small>(ວັນທີ ' . $fromdate . ' ຫາ ' . $todate . ')</small>'],
                                            'series' => [
                                                [
                                                    'name' => "ຈຳນວນບົດຄວາມ",
                                                    'data' => [[
                                                        'name' => "ຍິງ " . number_format($female * 100 / ($male + $female), 2) . ' %',
                                                        'y' => $female
                                                    ], [
                                                        'name' => "ຊາຍ " . number_format($male * 100 / ($male + $female), 2) . ' %',
                                                        'y' => $male
                                                    ]]
                                                ]
                                            ]
                                        ]
                                    ]);
                                }
                                ?>
                        </div>
                        <hr>
                        <?php if ($male + $female > 0) : ?>
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th>ທັງໝົດ</th>
                                    <td class="text-right">
                                        <?= \yii\helpers\Html::a(number_format($male + $female), ['statistic/female', 'fromdate' => $model->fromdate, 'todate' => $model->todate]) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>ຍິງ</th>
                                    <td class="text-right">
                                        <?= \yii\helpers\Html::a(number_format($female), ['statistic/female', 'fromdate' => $model->fromdate, 'todate' => $model->todate]) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>ຊາຍ</th>
                                    <td class="text-right">
                                        <?= \yii\helpers\Html::a(number_format($male), ['statistic/male', 'fromdate' => $model->fromdate, 'todate' => $model->todate]) ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="box box-solid box-primary">
                    <div class="box-body">
                        <h4>ຈຳນວນບົດປັບປຸງ</h4>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ຈຳນວນບົດຄວາມ</th>
                                    <th class="text-right">
                                        <?= Html::a(count($editessays), [
                                                'statistic/adjust',
                                                'fromdate' => $model->fromdate,
                                                'todate' => $model->todate
                                            ], []) ?>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="box box-solid box-primary">
                    <div class="box-body">
                        <h4>ຈຳນວນບົດຕາມພາກສ່ວນ</h4>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ພາກສ່ວນ</th>
                                    <th class="text-right">
                                        ຈຳນວນບົດ
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($sectionessays as $i => $s) : ?>
                                <tr>
                                    <td><?= $s['name'] ?></td>
                                    <td class="text-right">
                                        <?= Html::a($s['count'], [
                                                        'statistic/section', 'id' => $s['id']
                                                    ], []) ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>