<?php

use yii\helpers\Html;

?>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div style="font-size: 1.5em;border-bottom: 1px solid #ff0000;">
                <span style="border-bottom: 5px solid #ff0000;">
                    ບົດນຳ
                </span>
            </div>
            <div>
                <?php

                foreach ($chapters as $key => $chapter) : ?>
                    <div style="border-bottom: 1px solid #eeeeee;padding-top: 1em">
                        <div style="font-size: 1.3em">
                            <?= Html::a($chapter->title, ['site/chapter-view', 'id' => $chapter->id], []) ?>
                        </div>
                        <div style="padding-top: 0.5em;color: #999999;padding-bottom: 0.5em">
                            <i class="glyphicon glyphicon-pencil" style="font-size: 0.7em;"></i>
                            <?= $chapter->author ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div style="margin-top: 1em" class="text-right">
                <?= Html::a('ເບິ່ງທັງໝົດ >>', ['site/chapters'], []) ?>
            </div>
        </div>
        <div class="col-sm-9">
            <div>
                <h1 class="text-center"><?= $model->title ?></h1>
                <div class="text-center">
                    <i class="glyphicon glyphicon-pencil"></i>
                    <?= $model->author ?>
                </div>
            </div>
            <div style="padding: 2em 4em 8em 4em">
                <hr style="border-bottom: 5px solid #ff0000">
                <?= $model->content ?>

                <hr style="border-bottom: 5px solid #ff0000">
            </div>
        </div>
    </div>
</div>