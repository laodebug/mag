<?php

use yii\helpers\Html;
?>
<div style="border-bottom: 1px solid #eeeeee;padding-top: 1em">
    <div class="row">
        <div class="col-sm-2">
                <?= Html::a(Html::img($model->cover_image, ['style' => [
                    'width' => '100px'
                ]]), ['site/magazine-view', 'id' => $model->id], []) ?>
        </div>
        <div class="col-sm-10">
            <div style="font-size: 1.3em">
                <?= Html::a($model->title, ['site/magazine-view', 'id' => $model->id], []) ?>
            </div>
            <div style="padding-top: 0.5em;color: #999999;padding-bottom: 0.5em">
                <i class="glyphicon glyphicon-pencil" style="font-size: 0.7em;"></i>
                <?= $model->author ?>
            </div>
        </div>
    </div>
</div>