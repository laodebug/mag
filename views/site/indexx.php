<?php

use yii\helpers\Html;
?>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div style="font-size: 1.5em;border-bottom: 1px solid #ff0000;">
                <span style="border-bottom: 5px solid #ff0000;">
                    ບົດນຳ
                </span>
            </div>
            <div>
                <?php foreach ($chapters as $key => $chapter) : ?>
                    <div style="border-bottom: 1px solid #eeeeee;padding-top: 1em">
                        <div style="font-size: 1.3em">
                            <?= Html::a($chapter->title, ['site/chapter-view', 'id' => $chapter->id], []) ?>
                        </div>
                        <div style="padding-top: 0.5em;color: #999999;padding-bottom: 0.5em">
                            <i class="glyphicon glyphicon-pencil" style="font-size: 0.7em;"></i>
                            <?= $chapter->author ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div style="margin-top: 1em" class="text-right">
                <?= Html::a('ເບິ່ງທັງໝົດ >>', ['site/chapters'], []) ?>
            </div>
        </div>
        <div class="col-sm-7">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php foreach ($magazines as $key => $mag) : ?>
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="<?= $key == 0 ? 'active' : '' ?>"></li>                    
                    <?php endforeach; ?>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php foreach ($magazines as $key => $mag) : ?>
                        <div class="text-center item <?= $key == 0 ? 'active' : '' ?>">
                            <?= Html::img($mag->cover_image, ['style' => [
                                'height' => '400px',
                                'margin' => '0 auto'
                            ]]) ?>

                            <div class="carousel-caption">
                                <?php // echo $mag->title 
                                ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <div style="font-size: 1.5em;margin-top: 1em; margin-bottom: 1em;border-bottom: 1px solid #ff0000;">
                <span style="border-bottom: 5px solid #ff0000;">
                    ວາລະສານປະຈຸບັນ
                </span>
            </div>
            <div>
                <?php foreach ($magazines as $key => $mag) : ?>
                    <div class="row" style="border-bottom: 1px solid #eeeeee;margin: 1em">
                        <div class="col-sm-2">
                            <?= Html::a(Html::img($mag->cover_image, ['style' => [
                                'width' => '100px',
                            ]]), ['site/magazine-view', 'id' => $mag->id], []) ?>
                        </div>
                        <div class="col-sm-10" style="font-size: 1.2em;">
                            <strong>
                                <?= Html::a($mag->title, ['site/magazine-view', 'id' => $mag->id], []) ?>
                                <?= $mag->subtitle ?>
                            </strong>
                            <div style="color: #999999;">
                                <small>
                                    <i class="glyphicon glyphicon-pencil"></i>
                                    <?= $mag->author ?>
                                </small>
                            </div>
                            <div class="text-right" style="padding-top: 2em">
                                <i>
                                    <small>
                                        <i class="glyphicon glyphicon-calendar"></i>
                                        <?php
                                        $date = new DateTime($mag->created_date);
                                        echo $date->format('d/m/Y');
                                        ?>
                                    </small>
                                </i>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-sm-2">
            <div style="font-size: 1.5em;border-bottom: 1px solid #ff0000;margin-bottom: 1em">
                <span style="border-bottom: 5px solid #ff0000;">
                    ວາລະສານ
                </span>
            </div>
            <div>
                <?php
                if (isset($magazines)) :
                    foreach ($magazines as $key => $magazine) : ?>
                        <div style="border-bottom: 1px solid #eeeeee; padding-bottom: 2em">
                            <div style="padding: 0 2em;">
                                <?= Html::a(Html::img($magazine->cover_image, [
                                    'style' => ['width' => '100px']
                                ]), ['site/magazine-view', 'id' => $mag->id], []) ?>
                            </div>
                            <div class="text-center" style="font-size: 1.2em;">
                                <?= Html::a($magazine->title, ['site/magazine-view', 'id' => $magazine->id], []) ?>
                            </div>
                        </div>
                <?php
                    endforeach;
                endif;
                ?>
            </div>

            <div style="margin-top: 1em" class="text-right">
                <?= Html::a('ເບິ່ງທັງໝົດ >>', ['site/magazines'], []) ?>
            </div>
        </div>
    </div>

    <hr style="margin-top: 2em;">
    <div style="font-size: 1.5em;border-bottom: 1px solid #ff0000;margin-bottom: 1em">
        <span style="border-bottom: 5px solid #ff0000;">
            ວາລະສານແຕ່ລະເດືອນ
        </span>
    </div>
    <div class="row">
        <?php
        foreach ($magazines as $key => $mag) : ?>
            <div class="col-sm-1">
                <?= Html::a(Html::img($mag->cover_image, [
                    'style' => [
                        'width' => '100%'
                    ]
                ]), ['site/magazine-view', 'id' => $mag->id]) ?>
                <div class="text-center" style="font-size: 1.2em;">
                    <?= Html::a("ເດືອນ ". $mag->for_month ." ປີ " . $mag->for_year, ['site/magazine-view', 'id' => $mag->id]) ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
