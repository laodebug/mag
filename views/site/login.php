<?php

/* @var $this yii\web\View */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

$this->title = 'ສມປຊ - ລະບົບຕິດຕາມ ວາລະສານ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box">
    <div class="login-box-body">
        <div class="text-center">
            <img src="logo.jpg" alt="">
        </div>
        <h3 class="text-center">ລະບົບຕິດຕາມ ວາລະສານ</h3>
        <hr>
        <h4 class="text-center">ເຂົ້າສູ່ລະບົບ</h4>
        <?php $form = ActiveForm::begin([
            'fieldConfig' => [
                'template' => "<div class=\"form-group has-feedback\">{input}<p>{error}</p></div>",
            ],
        ]); ?>

        <?= $form->field($model, 'username', [
            'template' => '{input}<span class="fa fa-user form-control-feedback"></span>'
        ])->textInput([
            'class' => 'form-control text-center',
            'autofocus' => true,
            'placeholder' => 'ຊື່ຜູ້ໃຊ້'
        ]) ?>

        <?= $form->field($model, 'password', [
            'template' => '{input}<span class="fa fa-lock form-control-feedback"></span>'
        ])->passwordInput([
            'class' => 'form-control text-center',
            'placeholder' => 'ລະຫັດຜ່ານ'
        ]) ?>

        <div class="row">
            <div class="col-sm-12">
                <?php
                try {
                    echo Alert::widget();
                } catch (Exception $e) {
                    Yii::$app->session->setFlash('warning', $e->getMessage());
                }
                ?>
            </div>
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label>
                        <?= $form->field($model, 'rememberMe')->checkbox()->label('ຈື່ຂ້ອຍໄວ້') ?>
                    </label>
                </div>
            </div>
            <div class="col-xs-4">
                <?= Html::submitButton('ຕົກລົງ', [
                    'class' => 'btn btn-primary btn-block btn-flat',
                    'name' => 'login-button'
                ]) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <?php
    $this->registerJs('submit', "
    $('form').submit(function() {
        alert('ok');
        var checked = $('#loginform-rememberme').val();
        console.log(checked);
        if (checked) {
            var username = $('#loginform-username');
            console.log(username);
            localStorage.setItem('username', username);
        }
    });
    ");