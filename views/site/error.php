<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

$this->title = 'ພົບບັນຫາ';
?>
<div class="site-error">
  <div class="alert alert-danger">
    ບໍ່ພົບຂໍ້ມູນ
    <p>
        <?= $name ?>
    </p>
  </div>
</div>
