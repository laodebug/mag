<?php

use yii\helpers\Html;
use yii\widgets\ListView;

?>
<div class="container">
    <div class="row">
        <div class="col-sm-10">
            <div style="font-size: 1.5em;border-bottom: 1px solid #ff0000;">
                <span style="border-bottom: 5px solid #ff0000;">
                    ວາລະສານແຕ່ລະເດືອນ
                </span>
            </div>
            <div style="margin-top: 1em">
                <?php
                echo ListView::widget([
                    'dataProvider' => $provider,
                    'itemView' => '_magazine',
                    'summary' => false,
                    'emptyText' => 'ບໍ່ມີຂໍ້ມູນ',
                ]);
                ?>
            </div>
        </div>
        <div class="col-sm-2">
            <div style="font-size: 1.5em;border-bottom: 1px solid #ff0000;margin-bottom: 1em">
                <span style="border-bottom: 5px solid #ff0000;">
                    ບົດນຳ
                </span>
            </div>
            <div>
                <?php
                if (isset($chapters)) :
                    foreach ($chapters as $key => $chapter) : ?>
                        <div style="border-bottom: 1px solid #eeeeee; padding-bottom: 1em;">
                            <div class="text-center" style="font-size: 1.2em;">
                                <?= Html::a($chapter->title, ['site/chapter-view', 'id' => $chapter->id], [
                                    'class' => 'text-left'
                                ]) ?>
                            </div>
                        </div>
                <?php
                    endforeach;
                endif;
                ?>
            </div>

            <div style="margin-top: 1em" class="text-right">
                <?= Html::a('ເບິ່ງທັງໝົດ >>', ['site/chapters'], []) ?>
            </div>
        </div>
    </div>
</div>