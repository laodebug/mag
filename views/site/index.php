<?php

/* @var $this yii\web\View */

use app\models\Status;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = '';
$statuses = Status::find()->where('deleted=0')->orderBy('id')->all();
$statuscounts = [];
foreach ($models as $i => $model) {
    if (!isset($statuscounts[$model->status_id])) $statuscounts[$model->status_id] = 0;
    $statuscounts[$model->status_id]++;
}

?>
  <div class="row">
    <div class="col-sm-12 text-center">
      <img src="logo.jpg" alt="">
    </div>
  </div>
  <h2 class="title text-center">
    ລະບົບຕິດຕາມ ວາລະສານ ສະຖາບັນການເມືອງ-ການປົກຄອງ
  </h2>

  <div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        <?php $form = ActiveForm::begin(['method' => 'get']); ?>
      <div class="input-group input-group-lg">
        <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
        <input id="barcode" type="text" class="form-control" name="barcode" autofocus="true" value=""
               placeholder="ບາໂຄດ">
        <span class="input-group-btn">
      <button type="submit" class="btn btn-primary btn-flat"
              data-toggle="tooltip"
              data-placement="top"
              title="ຊອກຫາ">
        <i class="fa fa-search"></i>
      </button>
        </span>
      </div>
        <?php ActiveForm::end() ?>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-primary">
        <div class="inner">
          <h3><?= number_format(count($models)) ?></h3>
          <p>ບົດຄວາມທັງໝົດ</p>
        </div>
        <div class="icon">
          <i class="ion ion-android-list"></i>
        </div>
          <?= Html::a('ລາຍລະອຽດ <i class="fa fa-arrow-circle-right"></i>', ['essay/index'], [
              'class' => 'small-box-footer'
          ]) ?>
      </div>
    </div>
      <?php
      foreach ($statuses as $i => $status): if (!isset($statuscounts[$status->id])) continue; ?>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box" style="background-color: <?= $status->color ?>">
            <div class="inner">
              <h3><?= number_format($statuscounts[$status->id]) ?></h3>
              <p><?= $status['name'] ?></p>
            </div>
            <div class="icon">
              <i class="ion ion-android-list"></i>
            </div>
              <?= Html::a('ລາຍລະອຽດ <i class="fa fa-arrow-circle-right"></i>', ['essay/index', 'statusid' => $status->id], [
                  'class' => 'small-box-footer'
              ]) ?>
          </div>
        </div>
      <?php endforeach; ?>
  </div>
<?php
$this->registerJs("
setInterval(function() {
  $('#barcode').focus();
}, 500);
");
