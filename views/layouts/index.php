<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\models\Category;
use app\widgets\Alert;
use yii\helpers\Html;

$chapterTypes = Category::find()->orderBy('id')->all();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script src="dist/js/angular.min.js"></script>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <style>
        body,
        h1,
        h2,
        h4,
        .laofont {
            font-family: 'Phetsarath OT', 'Saysettha OT' !important;
        }

        input[type='number'] {
            text-align: right;
        }

        #color,
        .color {
            min-height: 30px;
            padding: 5px;
            text-align: center;
            color: #FFFFFF;
        }

        a.active,
        a.active:hover {
            background-color: #00c0ef !important;
            color: #ffffff !important;
        }

        a.active:hover {
            opacity: 0.8;
        }

        .banner {
            text-align: center;
            background-color: #00c0ef;
            color: #FFFFFF;
            font-size: 3em;
            padding-top: 1em;
            padding-bottom: 1em;
        }

        .grid-view td {
            white-space: normal;
        }
    </style>
</head>

<body class="hold-transition">
    <?php $this->beginBody(); ?>
    <div class="wrapper">
        <div class="banner">
            ວາລະສານ ທິດສະດີການເມືອງ-ການປົກຄອງ
        </div>
        <header class="navbar navbar-static" id="top">
            <div class="container">
                <div class="navbar-collapse collapse navbar-right">
                    <ul id="main-nav" class="nav navbar-nav navbar-main-menu">
                        <li><?= Html::a('ໜ້າຫຼັກ', ['site/indexx'], []) ?></li>
                        <li><?= Html::a('ຄວາມເປັນມາ', ['site/content', 'code' => 'history'], []) ?></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown">ປະເພດບົດຄວາມ <span class="caret"></span>
                            </a>
                            <ul id="w0" class="dropdown-menu">
                                <?php if (isset($chapterTypes)) : ?>
                                    <?php foreach ($chapterTypes as $i => $type) : ?>
                                        <li><?php echo Html::a($type->name, ['site/chaptertype', 'id' => $type->id], []) ?></li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </ul>
                        </li>
                        <li><?= Html::a('ຄະນະບັນນາທິການ', ['site/content', 'code' => 'org'], []) ?></li>
                        <li><?= Html::a('ລະບົບຕິດຕາມວາລະສານ', ['site/index'], []) ?></li>
                    </ul>
                    <div class="nav navbar-nav navbar-right">
                        <form id="search-form" class="navbar-form">
                            <div class="form-group nospace">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" placeholder="ຊອກຫາ…" autocomplete="off" value="" style="width: 226px;">
                                </div>
                            </div>
                            <div id="search-resultbox" style="width: 350px;"></div>
                        </form>
                    </div>
                </div>
            </div>
        </header>
        <div>
            <?php
            try {
                echo Alert::widget();
            } catch (Exception $e) {
                Yii::$app->session->setFlash('warning', $e->getMessage());
            }
            ?>
            <?= $content ?>
        </div>
        <footer style="margin-top: 2em;padding: 1em 2em; font-size: 1.2em">
            <div style="background-color: red;height: 5px;margin-bottom: 1em"></div>
            <strong>ສະຫງວນລິຂະສິດ &copy; <?= date('Y') ?> </strong>
        </footer>
    </div>

    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bower_components/moment/min/moment.min.js"></script>
    <script src="bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="dist/js/bootstrap-datetimepicker.min.js"></script>
    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="bower_components/fastclick/lib/fastclick.js"></script>
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>