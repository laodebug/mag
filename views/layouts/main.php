<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\models\Duration;
use app\models\Essay;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script src="dist/js/angular.min.js"></script>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <style>
        body,
        h1,
        h2,
        h4,
        .laofont {
            font-family: 'Phetsarath OT', 'Saysettha OT' !important;
        }

        input[type='number'] {
            text-align: right;
        }

        #color,
        .color {
            min-height: 30px;
            padding: 5px;
            text-align: center;
            color: #FFFFFF;
        }

        a.active,
        a.active:hover {
            background-color: #00c0ef !important;
            color: #ffffff !important;
        }

        a.active:hover {
            opacity: 0.8;
        }
    </style>
</head>

<body class="hold-transition skin-blue-light sidebar-mini fixed">
    <?php $this->beginBody();
    $durations = Duration::find()->orderBy('min')->all();
    $models = [];
    foreach ($durations as $i => $duration) {
        $models[$i] = Essay::find()
            ->where('datediff(current_date, last_update) between :fromdate and :todate', [
                ':fromdate' => $duration->min,
                ':todate' => $duration->max,
            ])
            ->andWhere('status_id in (2,3,4) and deleted=0')
            ->all();
    }
    $routes = Yii::$app->urlManager->parseRequest(Yii::$app->request);
    $route = $routes[0];
    $menus = [];
    $role = '';
    if (!Yii::$app->user->isGuest) {
        $role = Yii::$app->user->identity->role;
        if ($role === "ຜູ້ດູແລລະບົບ") {
            $menus = [
                ['name' => 'ໜ້າຫຼັກ', 'url' => 'site/index', 'icon' => 'home'],
                ['name' => 'ບົດຄວາມ ທັງໝົດ', 'url' => 'essay/index', 'icon' => 'list-ul'],
                ['name' => 'ເພີ່ມ ບົດຄວາມໃໝ່', 'url' => 'essay/create', 'icon' => 'plus'],
                ['name' => 'ສົ່ງ ບົດຄວາມໃໝ່ໃຫ້ ບກ', 'url' => 'essay/sendtochecker', 'icon' => 'user-plus'],
                ['name' => 'ຮັບ ບົດຄວາມ', 'url' => 'essay/receive', 'icon' => 'handshake-o'],
                ['name' => 'ບົດລໍຖ້າລົງ', 'url' => 'essay/publish', 'icon' => 'share-alt'],
                ['name' => 'ບົດທີ່ໄດ້ລົງແລ້ວ', 'url' => 'publish/index', 'icon' => 'check'],
                ['name' => 'ສະຖິຕິ', 'url' => 'statistic/index', 'icon' => 'bar-chart'],
            ];
        }
        if (in_array($role, ["ກອງເລຂາ"])) {
            $menus = [
                ['name' => 'ໜ້າຫຼັກ', 'url' => 'site/index', 'icon' => 'home'],
                ['name' => 'ບົດຄວາມ ທັງໝົດ', 'url' => 'essay/index', 'icon' => 'list-ul'],
                ['name' => 'ເພີ່ມ ບົດຄວາມໃໝ່', 'url' => 'essay/create', 'icon' => 'plus'],
                ['name' => 'ສົ່ງ ບົດຄວາມໃໝ່ໃຫ້ ບກ', 'url' => 'essay/sendtochecker', 'icon' => 'user-plus'],
                ['name' => 'ຮັບ ບົດຄວາມ', 'url' => 'essay/receive', 'icon' => 'handshake-o'],
                ['name' => 'ສະຖິຕິ', 'url' => 'statistic/index', 'icon' => 'bar-chart'],
            ];
        }
        if (in_array($role, ["ທົ່ວໄປ"])) {
            $menus = [
                ['name' => 'ໜ້າຫຼັກ', 'url' => 'site/index', 'icon' => 'home'],
                ['name' => 'ບົດຄວາມ ທັງໝົດ', 'url' => 'essay/index', 'icon' => 'list-ul'],
                ['name' => 'ສະຖິຕິ', 'url' => 'statistic/index', 'icon' => 'bar-chart'],
            ];
        }

        if ($role === "ຫົວໜ້າ") {
            $menus = [
                ['name' => 'ໜ້າຫຼັກ', 'url' => 'site/index', 'icon' => 'home'],
                ['name' => 'ບົດຄວາມ ທັງໝົດ', 'url' => 'essay/index', 'icon' => 'list-ul'],
                ['name' => 'ບົດລໍຖ້າລົງ', 'url' => 'essay/publish', 'icon' => 'share-alt'],
                ['name' => 'ບົດທີ່ໄດ້ລົງແລ້ວ', 'url' => 'publish/index', 'icon' => 'check'],
                ['name' => 'ສະຖິຕິ', 'url' => 'statistic/index', 'icon' => 'bar-chart'],
            ];
        }
    }
    ?>
    <div class="wrapper">
        <header class="main-header">
            <a href="index.php" class="logo">
                <img src="logo.jpg" alt="" style="height: 100%;">
                <span class="laofont"><b>ສມປຊ</b></span>
            </a>
            <nav class="navbar navbar-static-top navbar-fixed-top">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">ສມປຊ</span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <?php foreach ($durations as $i => $duration) : ?>
                            <li class="dropdown notifications-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #000000; background-color: <?= $duration['color'] ?>">
                                    <?= $duration['remark'] ?>
                                    <span class="label label-warning"><?= count($models[$i]) ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header"></li>
                                    <li>
                                        <ul class="menu">
                                            <?php foreach ($models[$i] as $m => $model) : ?>
                                                <li>
                                                    <?= Html::a('<i class="fa fa-file-text text-aqua"></i>' . $model['title'], ['essay/view', 'id' => $model->id]) ?>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                    <li class="footer">
                                        <?= Html::a('ທັງໝົດ', ['essay/duration', 'id' => $duration->id]) ?>
                                    </li>
                                </ul>
                            </li>
                        <?php endforeach; ?>
                        <li>
                            <a href="index.php?r=site/logout">
                                <i class="fa fa-power-off"></i> ອອກ
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
            </nav>
        </header>

        <aside class="main-sidebar">
            <section class="sidebar">
                <ul class="sidebar-menu" data-widget="tree">
                    <?php foreach ($menus as $i => $menu) : ?>
                        <li><?= Html::a('<i class="fa fa-' . $menu['icon'] . '"></i> <span>' . $menu['name'] . '</span>', [$menu['url']], ['class' => $route == $menu['url'] ? 'active' : '']) ?>
                        </li>
                    <?php endforeach; ?>
                    <?php if ($role === 'ຜູ້ດູແລລະບົບ') : ?>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-gear"></i> <span>ການຕັ້ງຄ່າ</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><?= Html::a('<i class="fa fa-users text-red"></i> <span>ຜູ້ກວດກາ</span>', ['employee/index']) ?></li>
                                <li><?= Html::a('<i class="fa fa-users text-red"></i> <span>ພາກສ່ວນ</span>', ['section/index']) ?></li>
                                <li><?= Html::a('<i class="fa fa-users text-red"></i> <span>ກົມກອງ</span>', ['section-sub/index']) ?></li>
                                <li><?= Html::a('<i class="fa fa-list-ol"></i> <span>ຂັ້ນ ຜູ້ກວດກາ</span>', ['level/index']) ?></li>
                                <li><?= Html::a('<i class="fa fa-list-ol"></i> <span>ປະເພດບົດຄວາມ</span>', ['category/index']) ?></li>
                                <li><?= Html::a('<i class="fa fa-list-ol"></i> <span>ໄລຍະເວລາ</span>', ['duration/index']) ?></li>
                                <li><?= Html::a('<i class="fa fa-list-ol"></i> <span>ສະຖານະ</span>', ['status/index']) ?></li>
                                <li><?= Html::a('<i class="fa fa-user text-yellow"></i> <span>ຜູ້ໃຊ້</span>', ['user/index']) ?></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-gear"></i> <span>ເວັບໄຊ</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><?= Html::a('<i class="fa fa-users text-red"></i> <span>ບົດນຳ</span>', ['chapter/index']) ?></li>
                                <li><?= Html::a('<i class="fa fa-users text-red"></i> <span>ວາລະສານ</span>', ['magazine/index']) ?></li>
                                <li><?= Html::a('<i class="fa fa-users text-red"></i> <span>ຄວາມເປັນມາ</span>', ['contents/update', 'id' => 'history']) ?></li>
                                <li><?= Html::a('<i class="fa fa-users text-red"></i> <span>ຄະນະບັນນາທິການ</span>', ['contents/update', 'id' => 'org']) ?></li>
                            </ul>
                        </li>
                    <?php endif; ?>
            </section>
        </aside>

        <div class="content-wrapper">
            <section class="content-header">
                <h1><?= $this->title ?></h1>
                <?php
                try {
                    echo Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        'homeLink' => [
                            'label' => 'ໜ້າຫຼັກ',
                            'url' => ['site/index']
                        ]
                    ]);
                } catch (Exception $e) {
                    Yii::$app->session->setFlash('warning', $e->getMessage());
                }
                ?>
            </section>

            <section class="content">
                <?php
                try {
                    echo Alert::widget();
                } catch (Exception $e) {
                    Yii::$app->session->setFlash('warning', $e->getMessage());
                }
                ?>

                <?= $content ?>
            </section>
        </div>
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 4.0
            </div>
            <strong>ສະຫງວນລິຂະສິດ &copy; <?= date('Y') ?> </strong>
        </footer>
    </div>

    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!--<script src="bower_components/chart.js/Chart.js"></script>-->
    <!--<script src="bower_components/raphael/raphael.min.js"></script>-->
    <!--<script src="bower_components/morris.js/morris.min.js"></script>-->
    <!--<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>-->
    <!--<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>-->
    <!--<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>-->
    <!--<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>-->
    <script src="bower_components/moment/min/moment.min.js"></script>
    <script src="bower_components/select2/dist/js/select2.full.min.js"></script>
    <!--<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>-->
    <!--<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>-->
    <script src="dist/js/bootstrap-datetimepicker.min.js"></script>
    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <!--<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>-->
    <!--<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>-->
    <!--<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>-->
    <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="bower_components/fastclick/lib/fastclick.js"></script>
    <script src="dist/js/adminlte.min.js"></script>

    <script>
        $.widget.bridge('uibutton', $.ui.button);
        $('.datepicker').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss'
        });
        $('.monthpicker').datepicker({
            format: "yyyy-mm",
            viewMode: "months",
            minViewMode: "months"
        });
        $('input[type="number"]').focus(function() {
            if ($(this).val() == 0) {
                $(this).val(null);
            }
        });
        $('.select2').select2();
        $('.colorpicker').colorpicker()
            .change(function() {
                $('#color').css('background-color', $(this).val());
            }).change();
        $("#color").click(function() {
            $(".colorpicker").focus();
        });
        $('[data-toggle="tooltip"]').tooltip();
    </script>
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>