<?php

namespace app\controllers;

use app\models\Category;
use app\models\Chapter;
use app\models\Contents;
use app\models\Essay;
use app\models\History;
use app\models\LoginForm;
use app\models\Magazine;
use app\models\MagazineSearch;
use app\models\User;
use Exception;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

// use yii\web\Cookie;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if ($action->id == 'error') $this->layout = 'index';
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'layout' => 'index',
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($barcode = null, $search = null)
    {
        if (isset($barcode) && strlen($barcode) == 9) {
            return $this->redirect(['essay/view', 'id' => $barcode]);
        } else if (isset($search) && strlen($search) > 0) {
            return $this->redirect(['site/search', 'search' => $search]);
        }
        if (Yii::$app->user->isGuest) return $this->redirect(['site/login']);
        $models = Essay::find()->where('deleted=0')->all();
        return $this->render('index', [
            'models' => $models
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        // $cookie = Yii::$app->request->cookies;
        // if ($cookie->has('username'))
        //     $model->username = $cookie->get('username');

        if ($model->load(Yii::$app->request->post())) {
            $user = User::find()
                ->where([
                    'username' => $model->username,
                    'password' => $model->password,
                    'deleted' => 0
                ])->one();
            if (isset($user)) {
                if ($model->rememberMe) {
                    Yii::$app->user->login(@$user, 3600 * 24 * 30);
                    // $cookie->add(
                    //     new Cookie([
                    //         'name' => 'username',
                    //         'value' => $user->username,
                    //         'expire' => time() + 3600 * 24 * 30 //1 month
                    //     ])
                    // );
                }
                return $this->redirect(['site/index']);
            }
            Yii::$app->session->setFlash('danger', 'ຊື່ຜູ້ໃຊ້ ຫຼື ລະຫັດຜ່ານ ບໍ່ຖືກຕ້ອງ');
            return $this->redirect(['site/login']);
        }
        $this->view->title = "ເຂົ້າສູ່ລະບົບ";
        $model->password = '';
        $this->layout = 'login';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionUpdate()
    {
        try {
            //            $this->exe("cd ../");
            $this->exe("yii migrate");
            //            $this->exe("git pull");
        } catch (Exception $ex) {
            print_r($ex);
        }
    }

    private function exe($command)
    {
        $return = [];
        $out = [];
        echo "exe " . $command;
        exec($command, $out, $return);
        print_r($out);
        echo "\r\n";
        print_r($return);
    }

    public function actionFix()
    {
        $models = Essay::find()->where('deleted=0')->all();
        foreach ($models as $model) {
            $hitory = History::find()
                ->where([
                    'essay_id' => $model->id
                ])
                ->andWhere("lower(sr) <> 'u' ")
                ->orderBy('created_date desc')
                ->one();
            if (isset($hitory)) {
                try {
                    $model->status_id = $hitory->status_id;
                    $model->employee_id = $hitory->employee_id;
                    if (!$model->save()) {
                        throw new \yii\db\Exception($model->errors);
                    }
                    echo json_encode($model->attributes);
                    echo "<hr/>";
                } catch (Exception $exception) {
                    print_r($exception);
                }
            }
        }
    }

    public function actionIndexx()
    {
        $this->layout = 'index';
        $chapters = Chapter::find()
            ->where([
                'deleted' => 0,
                'hidden' => 0
            ])
            ->orderBy('id desc')
            ->all();
        $mags = Magazine::find()
            ->where([
                'deleted' => 0,
                'hidden' => 0
            ])
            ->andWhere('date(current_timestamp) >= from_date')
            ->orderBy('id desc')
            ->all();

        $searchModel = new MagazineSearch();
        $searchModel->deleted = 0;
        $magDataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $magDataProvider->query->orderBy('id desc');

        return $this->render('indexx', [
            'chapters' => $chapters,
            'magazines' => $mags,
            'magDataProvider' => $magDataProvider
        ]);
    }

    public function actionChapterView($id)
    {
        $this->layout = 'index';
        $model = Chapter::find()->where(['id' => $id, 'deleted' => 0, 'hidden' => 0])->one();
        if (!isset($model)) {
            return $this->redirect(['site/error']);
        }
        $chapters = Chapter::find()
            ->where([
                'deleted' => 0,
                'hidden' => 0
            ])
            ->orderBy('id desc')
            ->all();
        return $this->render('chapter-view', [
            'model' => $model,
            'chapters' => $chapters,
        ]);
    }

    public function actionMagazineView($id)
    {
        $this->layout = 'index';
        $model = Magazine::find()->where(['id' => $id])->one();
        if (!isset($model)) {
            return $this->redirect(['site/error']);
        }
        $chapters = Chapter::find()
            ->where([
                'deleted' => 0,
                'hidden' => 0
            ])
            ->orderBy('id desc')
            ->all();
        return $this->render('magazine-view', [
            'model' => $model,
            'chapters' => $chapters,
        ]);
    }

    public function actionChapters()
    {
        $this->layout = 'index';
        $query = Chapter::find()->where(['deleted' => 0, 'hidden' => 0]);
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $mags = Magazine::find()
            ->where([
                'deleted' => 0,
                'hidden' => 0
            ])            
            ->andWhere('date(current_timestamp) >= from_date')
            ->orderBy('id desc')
            ->all();
        return $this->render('chapters', [
            'provider' => $provider,
            'magazines' => $mags,
            'models' => [],
        ]);
    }

    public function actionMagazines()
    {
        $this->layout = 'index';
        $query = Magazine::find()
            ->where([
                'deleted' => 0,
                'hidden' => 0
            ])
            ->andWhere('date(current_timestamp) >= from_date');
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $chapters = Chapter::find()
            ->where([
                'deleted' => 0,
                'hidden' => 0
            ])
            ->orderBy('id desc')
            ->all();
        return $this->render('magazines', [
            'provider' => $provider,
            'chapters' => $chapters,
            'models' => [],
        ]);
    }

    public function actionContent($code)
    {
        $this->layout = 'index';
        $content = Contents::find()->where(['code' => $code])->one();
        return $this->render('content', [
            'content' => $content,
        ]);
    }

    public function actionSearch($search = null)
    {
        $this->layout = 'index';
        $providerMags = null;
        $providerChapters = null;

        $chapters = Chapter::find()
            ->where([
                'deleted' => 0,
                'hidden' => 0,
            ])
            ->limit(3)
            ->orderBy('id desc')
            ->all();

        $mags = Magazine::find()
            ->where([
                'deleted' => 0,
                'hidden' => 0,
            ])
            ->limit(3)
            ->orderBy('id desc')
            ->all();

        if (isset($search)) {
            $queryMags = Magazine::find()
                ->where([
                    'deleted' => 0,
                    'hidden' => 0
                ])                
                ->andWhere('date(current_timestamp) >= from_date')
                ->andWhere('title like :search or content like :search', [
                    'search' => "%$search%"
                ]);

            $providerMags = new ActiveDataProvider([
                'query' => $queryMags,
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);

            $queryChapters = Chapter::find()
                ->where([
                    'deleted' => 0,
                    'hidden' => 0
                ])
                ->andWhere('title like :search or content like :search', [
                    'search' => "%$search%"
                ]);
            $providerChapters = new ActiveDataProvider([
                'query' => $queryChapters,
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
            return $this->render('search', [
                'search' => $search,
                'providerChapters' => $providerChapters,
                'providerMags' => $providerMags,
                'chapters' => $chapters,
                'magazines' => $mags,
            ]);
        }

        return $this->render('searchnotfound', [
            'search' => $search,
            'chapters' => $chapters,
            'magazines' => $mags,
        ]);
    }

    public function actionChaptertype($id)
    {
        $category = Category::find()->where(['id' => $id])->one();
        if (!isset($category)) {
            new NotFoundHttpException();
        }

        $queryChapters = Chapter::find()
            ->where([
                'category_id' => $category->id,
                'deleted' => 0,
                'hidden' => 0,
            ]);
        $providerChapters = new ActiveDataProvider([
            'query' => $queryChapters,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->layout = 'index';
        return $this->render('chaptertype', [
            'providerChapters' => $providerChapters,
            'category' => $category
        ]);
    }

    public function actionPull()
    {
        try {            
            $returns = [];
            $out = [];
            $this->callCommand("git -c core.quotepath=false fetch origin", '/var/www/html/mag');
            $this->callCommand("git -c core.quotepath=false merge origin/master --no-stat -v", '/var/www/html/mag');
        } catch (\Exception $ex) {
            print_r($ex);
        }
    }
    
    private function callCommand($command, $dir){
        try {
            $returns = array();
            $out = array();
            chdir($dir);

            echo "EXECUTING COMMAND: $command\r\n";
            
            exec($command, $out, $returns);

            foreach ($out as $r)
                echo "$r\r\n";
                
            echo "\r\n\r\n";
        } catch (\Exception $ex) {
            print_r($ex);
        }
    }

    public function actionTest()
    {
        echo 'OK';
    }
}
