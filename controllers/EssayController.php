<?php

namespace app\controllers;

use app\models\Duration;
use app\models\Essay;
use app\models\EssaySearch;
use app\models\History;
use app\models\Photo;
use app\models\Publish;
use app\models\PublishDetail;
use app\models\Status;
use Yii;
use yii\base\Exception;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use function date;
use function json_encode;
use function rand;

/**
 * EssayController implements the CRUD actions for Essay model.
 */
class EssayController extends Controller
{

    /**
     * @param $action
     * @return bool|Response
     * @throws BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('danger', 'ທ່ານບໍ່ໄດ້ຮັບອະນຸຍາດໃຫ້ເຂົ້າເຖິງໜ້ານີ້');
            return $this->redirect(['site/index']);
        }
        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Essay models.
     * @return mixed
     */
    public function actionIndex($statusid = null)
    {
        $searchModel = new EssaySearch();
        $searchModel->deleted = 0;
        if (isset($statusid))
            $searchModel->status_id = $statusid;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->addSelect('*')
            ->addSelect(['last_update' => 'datediff(current_date, last_update)']);
        $durations = Duration::find()->all();
        $statuses = Status::find()->where('deleted=0')->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'durations' => $durations,
            'statuses' => $statuses,
            'statusid' => $statusid
        ]);
    }

    public function actionDuration($id)
    {
        $duration = Duration::find()->where(['id' => $id])->one();
        $searchModel = new EssaySearch();
        $searchModel->deleted = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->addSelect('*')
            ->addSelect(['last_update' => 'datediff(current_date, last_update)'])
            ->andWhere('status_id in (2,3,4) and datediff(current_date, last_update) between :fromdate and :todate', [
                ':fromdate' => $duration->min,
                ':todate' => $duration->max,
            ]);
        $durations = Duration::find()->all();
        $statuses = Status::find()->where('deleted=0')->all();
        return $this->render('duration', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'duration' => $duration,
            'durations' => $durations,
            'statuses' => $statuses
        ]);
    }

    /**
     * Displays a single Essay model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Essay model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Essay the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Essay::find()->where('deleted=0 and id = :id or barcode = :id', [':id' => $id])->one();
        if (isset($model)) {
            if ($model->deleted == 0)
                return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new Essay model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->identity->role == 'ທົ່ວໄປ') {
            Yii::$app->session->setFlash('danger', 'ທ່ານບໍ່ໄດ້ຮັບອະນຸຍາດໃຫ້ເຂົ້າເຖິງໜ້ານີ້');
            return $this->redirect(['site/index']);
        }
        $model = new Essay();
        $model->received_date = date('Y-m-d');
        if ($model->load(Yii::$app->request->post())) {
            $db = Yii::$app->db->beginTransaction();
            try {
                $model->file = UploadedFile::getInstance($model, 'file');
                if (isset($model->file))
                    $model->filename = $model->file->baseName . '.' . $model->file->extension;
                $model->barcode = date('ymdHms') . rand(0, 9);
                $model->user_id = Yii::$app->user->id;
                $model->created_date = date('Y-m-d H:i:s');
                $model->last_update = date('Y-m-d H:i:s');
                $model->status_id = 1;
                if (!$model->save())
                    throw new Exception(json_encode($model->errors));

                $model->barcode = date('Y') . "-" . str_pad($model->id, 9, '0', STR_PAD_LEFT);
                $model->save(true, ['barcode']);
                if (isset($model->file))
                    if (!$model->file->saveAs('uploads/' . $model->file->baseName . '.' . $model->file->extension, false))
                        throw new Exception('ບໍ່ສາມາດອັບໂຫຼດຟາຍ');

                $history = new History();
                $history->created_date = date('Y-m-d H:i:s');
                $history->essay_id = $model->id;
                $history->user_id = Yii::$app->user->id;
                $history->status_id = 1;
                $history->sr = 'R';
                if (!$history->save())
                    throw new Exception(json_encode($history->errors));

                $history->file = UploadedFile::getInstance($model, 'file');
                if (isset($history->file))
                    $history->file_name = $history->file->baseName . '-' . date('YmdHis') . '.' . $history->file->extension;

                if (!$history->save()) throw new Exception(json_encode($history->errors));

                if (isset($history->file))
                    if (!$history->file->saveAs('uploads/' . $history->file_name))
                        throw new Exception('ບໍ່ສາມາດອັບໂຫຼດຟາຍ');

                $db->commit();
                Yii::$app->session->setFlash('success', 'ສຳເລັດ');
                return $this->redirect(['view', 'id' => $model->id]);
            } catch (Exception $exception) {
                $db->rollBack();
                Yii::$app->session->setFlash('danger', $exception->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Essay model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->identity->role == 'ທົ່ວໄປ') {
            Yii::$app->session->setFlash('danger', 'ທ່ານບໍ່ໄດ້ຮັບອະນຸຍາດໃຫ້ເຂົ້າເຖິງໜ້ານີ້');
            return $this->redirect(['site/index']);
        }

        $model = $this->findModel($id);
        $temp = $model;
        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if (!$model->save())
                    throw new \Exception(json_encode($model->errors));
                $history = new History();
                $history->essay_id = $id;
                $history->sr = 'U';
                $history->user_id = Yii::$app->user->identity->id;
                $history->created_date = date('Y-m-d H:i:s');
                $history->status_id = $model->status_id; // leave it latest status
                $history->comment = implode(array_values($temp->attributes), '|');
                $history->comment .= ' => ';
                $history->comment .= implode(array_values($model->attributes), '|');
                if (!$history->save())
                    throw new \Exception(json_encode($history->errors));

                $history->file = UploadedFile::getInstance($model, 'file');
                if (isset($history->file)) {
                    $history->file_name = $history->file->baseName . '-' . date('YmdHis') . '.' . $history->file->extension;
                    $model->filename = $history->file_name;
                }

                if (!$history->save(true, ['file_name'])) throw new Exception(json_encode($history->errors));

                if (isset($history->file))
                    if (!$history->file->saveAs('uploads/' . $history->file_name))
                        throw new Exception('ບໍ່ສາມາດອັບໂຫຼດຟາຍ');

                if (!$model->save(true, ['filename'])) throw new Exception(json_encode($model->errors));

                $transaction->commit();
                Yii::$app->session->setFlash('success', 'ສຳເລັດ');
                return $this->redirect(['view', 'id' => $model->id]);
            } catch (\Exception $e) {
                Yii::$app->session->setFlash('danger', $e->getMessage());
                $transaction->rollBack();
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Essay model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->updateAttributes(['deleted' => 1]);
        return $this->redirect(['index']);
    }

    public function actionSendtochecker($barcode = null)
    {
        if (Yii::$app->user->identity->role == 'ທົ່ວໄປ') {
            Yii::$app->session->setFlash('danger', 'ທ່ານບໍ່ໄດ້ຮັບອະນຸຍາດໃຫ້ເຂົ້າເຖິງໜ້ານີ້');
            return $this->redirect(['site/index']);
        }

        if (isset($barcode))
            return $this->redirect(['essay/dosendtochecker', 'barcode' => $barcode]);

        $searchModel = new EssaySearch();
        $searchModel->deleted = 0;
        $searchModel->status_id = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('sendtochecker', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionDosendtochecker($barcode)
    {
        if (Yii::$app->user->identity->role == 'ທົ່ວໄປ') {
            Yii::$app->session->setFlash('danger', 'ທ່ານບໍ່ໄດ້ຮັບອະນຸຍາດໃຫ້ເຂົ້າເຖິງໜ້ານີ້');
            return $this->redirect(['site/index']);
        }

        $model = Essay::find()->where(['barcode' => $barcode])->one();
        if (!in_array($model->status_id, [1])) {
            Yii::$app->session->setFlash('danger', 'ບົດຄວາມໃໝ່ເທົ່ານັ້ນ ຈຶ່ງຈະສາມາດສົ່ງໃຫ້ ບກ ກວດ');
            return $this->redirect(['essay/view', 'id' => $model->id]);
        }
        $history = new History();
        $post = Yii::$app->request->post();
        if (isset($post['History'])) {
            $history->load($post);
            if (!isset($history->employee_id) || $history->employee_id == '') {
                Yii::$app->session->setFlash('danger', 'ກະລຸນາເລຶອກ ບກ');
                return $this->redirect(['essay/sendtochecker', 'barcode' => $barcode]);
            }
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $history->sr = 'S';
                $history->created_date = date('Y-m-d H:i:s');
                $history->essay_id = $model->id;
                $history->user_id = Yii::$app->user->getIdentity()->getId();
                $history->status_id = 2;
                if (!$history->save())
                    throw  new \Exception(json_encode($history->errors));

                $model->employee_id = $history->employee_id;
                $model->last_update = date('Y-m-d H:i:s');
                $model->status_id = 2;
                if (!$model->save()) throw  new \Exception(json_encode($model->errors));
                $transaction->commit();
            } catch (\Exception $ex) {
                try {
                    $transaction->rollBack();
                } catch (\Exception $e) {
                    Yii::$app->session->setFlash('danger', 'Cannot Rollback. ' . $e->getMessage());
                }
                Yii::$app->session->setFlash('danger', $ex->getMessage());
            }

            return $this->redirect(['essay/view', 'id' => $model->barcode]);
        }
        return $this->render('dosendtochecker', [
            'model' => $model,
            'history' => $history
        ]);
    }

    public function actionReceive($barcode = null)
    {
        if (Yii::$app->user->identity->role == 'ທົ່ວໄປ') {
            Yii::$app->session->setFlash('danger', 'ທ່ານບໍ່ໄດ້ຮັບອະນຸຍາດໃຫ້ເຂົ້າເຖິງໜ້ານີ້');
            return $this->redirect(['site/index']);
        }

        if (isset($barcode))
            return $this->redirect(['essay/doreceive', 'barcode' => $barcode]);

        $searchModel = new EssaySearch();
        $searchModel->deleted = 0;
        $dataProvider = $searchModel->searchCanReceiveEssays(Yii::$app->request->queryParams);

        return $this->render('receive', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionDoreceive($barcode)
    {
        if (Yii::$app->user->identity->role == 'ທົ່ວໄປ') {
            Yii::$app->session->setFlash('danger', 'ທ່ານບໍ່ໄດ້ຮັບອະນຸຍາດໃຫ້ເຂົ້າເຖິງໜ້ານີ້');
            return $this->redirect(['site/index']);
        }

        $model = Essay::find()->where(['barcode' => $barcode])->andWhere('deleted=0')->one();
        if (!in_array($model->status_id, [2, 3, 4])) {
            Yii::$app->session->setFlash('danger', 'ບົດຄວາມບໍ່ໄດ້ຢູ່ນຳ ບກ');
            return $this->redirect(['essay/view', 'id' => $barcode]);
        }

        $history = new History();
        $post = Yii::$app->request->post();
        if (isset($post['History'])) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $history->load($post);
                $history->file = UploadedFile::getInstance($history, 'file');
                if (isset($history->file))
                    $history->file_name = $history->file->baseName . '-' . date('YmdHis') . '.' . $history->file->extension;

                $history->created_date = date('Y-m-d H:i:s');
                $history->essay_id = $model->id;
                $history->user_id = Yii::$app->user->getId();
                $history->sr = "R";
                $history->employee_id = $model->employee_id;
                $history->status_id = $model->status_id;
                $history->comment = null;
                if (!$history->save())
                    throw new Exception(json_encode($history->errors));

                if (isset($post['History']['photos'])) {
                    foreach ($post['History']['photos'] as $i => $photo) {
                        $newphoto = new Photo();
                        $newphoto->history_id = $history->id;
                        $newphoto->name = $history->essay_id . uniqid();
                        if (!$newphoto->save()) throw new \Exception($newphoto->errors);
                        $this->base64_to_jpeg($photo, $newphoto->name);
                    }
                }

                $historysend = new History();
                $historysend->load($post);
                $historysend->sr = "S";
                $historysend->essay_id = $model->id;
                $historysend->user_id = Yii::$app->user->getId();
                $historysend->created_date = date('Y-m-d H:i:s', strtotime('+1 second'));

                if (in_array($historysend->status_id, [2, 4]))
                    $model->employee_id = $historysend->employee_id;
                else
                    $historysend->employee_id = null;

                if (isset($historysend->file))
                    $model->filename = $history->file_name;

                if (!$historysend->save()) throw new \Exception(json_encode($historysend->errors));
                $model->status_id = $historysend->status_id;
                $model->last_update = date('Y-m-d H:i:s');
                if (!$model->save()) throw new \Exception(json_encode($model->errors));

                $transaction->commit();
                return $this->redirect(['essay/view', 'id' => $barcode]);
            } catch (\Exception $exception) {
                Yii::$app->session->setFlash('danger', json_encode($exception->getMessage()));
                try {
                    $transaction->rollBack();
                } catch (\Exception $ex) {
                    Yii::$app->session->setFlash('danger', json_encode($ex->getMessage()));
                }
            }
        }
        $view = '';
        switch ($model->status_id) {
            case 2:
                $view = 'doreceivefromchecker1';
                break;
            case 3:
                $view = 'doreceiveafteredit';
                break;
            case 4:
                $view = 'doreceivefromchecker2';
                break;
        }
        return $this->render($view, [
            'model' => $model,
            'history' => $history
        ]);
    }

    public function base64_to_jpeg($base64_string, $name)
    {
        $folderPath = "uploads/";
        $image_parts = explode(";base64,", $base64_string);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file = $folderPath . $name . '.' . $image_type;
        file_put_contents($file, $image_base64);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPrint($id)
    {
        $this->layout = 'print';
        return $this->renderPartial('print', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionDopublish($barcode)
    {
        if (Yii::$app->user->identity->role == 'ທົ່ວໄປ') {
            Yii::$app->session->setFlash('danger', 'ທ່ານບໍ່ໄດ້ຮັບອະນຸຍາດໃຫ້ເຂົ້າເຖິງໜ້ານີ້');
            return $this->redirect(['site/index']);
        }

        $model = Essay::find()
            ->where('deleted=0 and barcode=:barcode', [
                ':barcode' => $barcode
            ])->one();
        if (!isset($model)) {
            Yii::$app->session->setFlash('warning', 'ບໍ່ພົບຂໍ້ມູນ');
            return $this->goBack();
        } else {
            $history = new History();
            $history->status_id = 5;
            $history->deleted = 0;
            $history->user_id = Yii::$app->user->identity->id;
            $history->created_date = date('Y-m-d H:i:s');
            $history->essay_id = $model->id;
            $history->sr = 'P';
            $db = Yii::$app->db->beginTransaction();
            try {
                if (!$history->save()) {
                    throw new Exception(json_encode($history->errors));
                }

                $model->status_id = 5;
                if (!$model->save()) {
                    throw new Exception(json_encode($model->errors));
                }
                $db->commit();
                Yii::$app->session->setFlash('success', 'ສຳເລັດ');
                return $this->redirect(['essay/publish']);
            } catch (\Exception $ex) {
                Yii::$app->session->setFlash('error', json_encode($model->errors));
                return $this->goBack(['view', 'id' => $model->id]);
            }
        }
    }

    public function actionPublish()
    {
        if (Yii::$app->user->identity->role == 'ທົ່ວໄປ') {
            Yii::$app->session->setFlash('danger', 'ທ່ານບໍ່ໄດ້ຮັບອະນຸຍາດໃຫ້ເຂົ້າເຖິງໜ້ານີ້');
            return $this->redirect(['site/index']);
        }

        $searchModel = new EssaySearch();
        $searchModel->deleted = 0;
        $searchModel->status_id = 5;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $publish = new Publish();
        $publish->month = date('m') + 0;
        $publish->year = date('Y');

        $post = Yii::$app->request->post();
        if (isset($post['Publish'])) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $publish->load($post['Publish']);
                $publish->month = $post['Publish']['month'];
                $publish->year = $post['Publish']['year'];
                $publish = Publish::find()->where(['month' => $publish->month, 'year' => $publish->year, 'deleted' => 0])->one();
                if (!isset($publish)) {
                    $publish = new Publish();
                    $publish->month = $post['Publish']['month'];
                    $publish->year = $post['Publish']['year'];
                }

                if (isset($_SESSION['publish']) && count($_SESSION['publish']) > 0) {
                    $publish->deleted = 0;
                    $publish->user_id = Yii::$app->user->identity->getId();
                    if (!$publish->save()) throw new \Exception($publish->errors);

                    foreach ($_SESSION['publish'] as $s => $item) {
                        $publishdetail = PublishDetail::find()->where([
                            'publish_id' => $publish->id,
                            'essay_id' => $item['id'],
                        ])->one();
                        if (!isset($publishdetail)) {
                            $publishdetail = new PublishDetail();
                            $publishdetail->publish_id = $publish->id;
                            $publishdetail->essay_id = $item['id'];
                            $publishdetail->position = $s + 1;
                            if (!$publishdetail->save()) {
                                throw new \Exception($publishdetail->errors);
                            }
                            $essay = Essay::find()
                                ->where(['id' => $publishdetail->essay_id, 'deleted' => 0])
                                ->one();
                            if (isset($essay)) {
                                $essay->status_id = 6;
                                $essay->last_update = date('Y-m-d H:i:s');
                                if (!$essay->save()) throw new \Exception($essay->errors);
                            }
                        }
                    }
                }
                $transaction->commit();
                $_SESSION['publish'] = [];
                Yii::$app->session->setFlash('success', 'ສຳເລັດ');
                return $this->redirect(['publish/view', 'id' => $publish->id]);
            } catch (\Exception $exception) {
                Yii::$app->session->setFlash('danger', $exception->getMessage());
            }
        }

        return $this->render('publish', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'publish' => $publish
        ]);
    }

    public function actionAddpublish($id)
    {
        if (Yii::$app->user->identity->role == 'ທົ່ວໄປ') {
            Yii::$app->session->setFlash('danger', 'ທ່ານບໍ່ໄດ້ຮັບອະນຸຍາດໃຫ້ເຂົ້າເຖິງໜ້ານີ້');
            return $this->redirect(['site/index']);
        }
        $model = Essay::find()->where(['id' => $id])->one();
        if (!isset($_SESSION['publish'])) {
            $_SESSION['publish'] = [];
        }
        if (in_array($model, $_SESSION['publish'])) {
            Yii::$app->session->setFlash('danger', 'ມີຢູ່ແລ້ວ');
        } else {
            Yii::$app->session->setFlash('success', 'ເພີ່ມສຳເລັດ');
            $_SESSION['publish'][] = $model;
        }
        return $this->redirect(['publish']);
    }

    public function actionRemovepublish($id)
    {
        if (Yii::$app->user->identity->role == 'ທົ່ວໄປ') {
            Yii::$app->session->setFlash('danger', 'ທ່ານບໍ່ໄດ້ຮັບອະນຸຍາດໃຫ້ເຂົ້າເຖິງໜ້ານີ້');
            return $this->redirect(['site/index']);
        }
        $model = Essay::find()->where(['id' => $id])->one();
        if (!isset($_SESSION['publish'])) {
            $_SESSION['publish'] = [];
        }

        $exist = array_search($model, $_SESSION['publish']);
        if ($exist !== false) {
            unset($_SESSION['publish'][$exist]);
            Yii::$app->session->setFlash('success', 'ລຶບສຳເລັດ');
        }
        return $this->redirect(['publish']);
    }

    public function actionChangechecker($id)
    {
        if (Yii::$app->user->identity->role == 'ທົ່ວໄປ') {
            Yii::$app->session->setFlash('danger', 'ທ່ານບໍ່ໄດ້ຮັບອະນຸຍາດໃຫ້ເຂົ້າເຖິງໜ້ານີ້');
            return $this->redirect(['site/index']);
        }

        $model = History::find()->where(['id' => $id, 'deleted' => 0])->one();
        $post = Yii::$app->request->post();
        if (isset($post['History'])) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $model->employee_id = $post["History"]['employee_id'];
                if (!$model->save()) throw new \Exception(json_encode($model->errors));

                $essay = Essay::find()->where(['id' => $model->essay_id])->one();
                $essay->employee_id = $model->employee_id;
                $essay->last_update = date('Y-m-d H:i:s');
                if (!$essay->save()) throw new \Exception(json_encode($essay->errors));
                $transaction->commit();

                return $this->redirect(['essay/view', 'id' => $model->essay_id]);
            } catch (\Exception $exception) {
                Yii::$app->session->setFlash('danger', json_encode($exception->getMessage()));
                try {
                    $transaction->rollBack();
                } catch (\Exception $ex) {
                    Yii::$app->session->setFlash('danger', json_encode($ex->getMessage()));
                }
            }
        }
        return $this->render('changechecker', [
            'model' => $model
        ]);
    }
}
