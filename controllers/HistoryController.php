<?php

namespace app\controllers;

use app\models\Essay;
use app\models\History;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * HistoryController implements the CRUD actions for History model.
 */
class HistoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Deletes an existing History model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->updateAttributes(['deleted' => 1]);
        if ($model->essay_id) {
            $essay = Essay::find()->where(['id' => $model->essay_id])->andWhere('deleted=0 and ')->one();
            if (isset($essay)) {
                $latest_history = History::find()->where('essay_id=:essay_id and deleted=0 and id != :id', [
                    ':essay_id' => $essay->id,
                    ':id' => $id
                ])
                    ->orderBy('created_date desc')
                    ->one();
                if (isset($latest_history)) {
                    $essay->status_id = $latest_history->status_id;
                    $essay->save();
                }
            }
        }

        return $this->redirect(['essay/view', 'id' => $model->essay_id]);
    }

    /**
     * Finds the History model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return History the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = History::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
