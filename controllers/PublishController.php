<?php

namespace app\controllers;

use app\models\Essay;
use app\models\Publish;
use app\models\PublishDetail;
use app\models\PublishSearch;
use Exception;
use Throwable;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PublishController implements the CRUD actions for Publish model.
 */
class PublishController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest || !in_array(Yii::$app->user->identity->role, ['ຜູ້ດູແລລະບົບ', 'ຫົວໜ້າ'])) {
            Yii::$app->session->setFlash('danger', 'ທ່ານບໍ່ໄດ້ຮັບອະນຸຍາດໃຫ້ເຂົ້າເຖິງໜ້ານີ້');
            return $this->redirect(['site/index']);
        }
        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'deletedetail' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Publish models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PublishSearch();
        $searchModel->deleted = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Publish model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if (isset($post['positions'])) {
            foreach ($post['positions'] as $essay_id => $position) {
                PublishDetail::updateAll([
                    'position' => $position
                ], [
                    'publish_id' => $id,
                    'essay_id' => $essay_id
                ]);
            }
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Publish model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Publish the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Publish::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Creates a new Publish model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Publish();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Publish model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Publish model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param $essay_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Throwable
     */
    public function actionDeletedetail($publish_id, $essay_id)
    {
        $publish = $this->findModel($publish_id);
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $detail = PublishDetail::find()->alias('d')
                ->join('join', 'publish p', 'p.id = d.publish_id and p.deleted=0')
                ->where(['publish_id' => $publish->id, 'essay_id' => $essay_id])->one();
            if (isset($detail)) {
                $essay = Essay::find()->where(['id' => $essay_id, 'deleted' => 0])->one();
                if (isset($essay)) {
                    $essay->status_id = 5;
                    if (!$essay->save()) throw new Exception(json_encode($essay->errors));
                }
                if (!$detail->delete()) throw new Exception(json_encode($detail->errors));
                $transaction->commit();
                Yii::$app->session->setFlash('success', 'ສຳເລັດ');
            }
        } catch (Exception $exception) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('danger', $exception->getMessage());
        }

        return $this->redirect(['publish/view', 'id' => $publish->id]);
    }

    public function actionDelete($id)
    {
        $publish = $this->findModel($id);
        $publish->deleted = 1;
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (!$publish->save()) throw new Exception(json_encode($publish->errors));

            $details = $publish->getPublishDetails()->all();
            foreach ($details as $detail) {
                $essay = Essay::find()
                    ->where([
                        'id' => $detail->essay_id
                    ])
                    ->one();
                if (isset($essay)) {
                    $essay->status_id = 5;
                    if (!$essay->save())
                        throw new Exception(json_encode($essay->errors));
                }
            }
            $transaction->commit();
            Yii::$app->session->setFlash('success', 'ສຳເລັດ');
        } catch (Exception $ex) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('danger', $ex->getMessage());
        }
        return $this->redirect(['index']);
    }
}
