<?php

/**
 * Created by PhpStorm.
 * User: adsavin
 * Date: 10/13/18
 * Time: 12:55 AM
 */

namespace app\controllers;


use app\models\Category;
use app\models\Employee;
use app\models\Essay;
use app\models\History;
use app\models\SearchDate;
use app\models\Section;
use app\models\SectionSub;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class StatisticController extends Controller
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('danger', 'ທ່ານບໍ່ໄດ້ຮັບອະນຸຍາດໃຫ້ເຂົ້າເຖິງໜ້ານີ້');
            return $this->redirect(['site/index']);
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $param = Yii::$app->request->queryParams;
        $model = new SearchDate();
        if (isset($param['SearchDate'])) {
            $model->fromdate = $param['SearchDate']['fromdate'];
            $model->todate = $param['SearchDate']['todate'];
        } else {
            if (!isset($model->fromdate)) $model->fromdate = date('Y-m-d');
            if (!isset($model->todate)) $model->todate = date('Y-m-d');
        }
        $models = Essay::find()
            ->where('deleted=0 and date(received_date) between :fromdate and :todate', [
                ':fromdate' => $model->fromdate,
                ':todate' => $model->todate
            ])
            ->all();
        $essaybycheckers = History::find()->alias('h')
            ->select('h.employee_id, count(h.id) comment')
            ->join('left join', 'essay e', 'e.id = h.essay_id and e.deleted=:deleted')
            ->join('left join', 'employee m', 'm.id = h.employee_id and m.deleted=:deleted')
            ->where("h.status_id in (:s1,:s2) and h.deleted=:deleted and lower(h.sr)=:sr", [
                ':s1' => 2,
                ':s2' => 4,
                ':deleted' => 0,
                ':sr' => 'r'
            ])
            ->andWhere("date(h.created_date) between :fromdate and :todate", [
                ':fromdate' => $model->fromdate,
                ':todate' => $model->todate,
            ])
            ->groupBy('h.employee_id')
            ->orderBy('count(h.id) desc')
            ->all();
        $editessays = History::find()
            ->where('status_id=3 and lower(sr)=:sr and deleted=0', [
                ':sr' => 'r'
            ])
            ->andWhere("date(created_date) between :fromdate and :todate", [
                ':fromdate' => $model->fromdate,
                ':todate' => $model->todate,
            ])
            ->all();
        $sectionessays = Yii::$app->db
            ->createCommand('select s.id, s.name, count(e.section_id) count
             from section s 
             left join essay e on e.section_id = s.id and e.deleted=0
             where date(e.last_update) between :fromdate and :todate
             group by e.section_id
             ', [
                ":fromdate" => $model->fromdate,
                ":todate" => $model->todate
            ])
            ->queryAll();

        return $this->render('index', [
            'model' => $model,
            'models' => $models,
            'essaybycheckers' => $essaybycheckers,
            'editessays' => $editessays,
            'sectionessays' => $sectionessays
        ]);
    }

    public function actionAll($fromdate = null, $todate = null)
    {
        if (!isset($fromdate)) $fromdate = date('Y-m-d');
        if (!isset($todate)) $todate = date('Y-m-d');
        $models = Essay::find()
            ->where('deleted=0 and date(received_date) between :fromdate and :todate', [
                ':fromdate' => $fromdate,
                ':todate' => $todate
            ])
            ->all();
        $title = 'ສະຖິຕິ ບົດຄວາມ ທັງໝົດ';
        return $this->render('list', [
            'title' => $title,
            'models' => $models
        ]);
    }

    public function actionPendings($fromdate = null, $todate = null)
    {
        if (!isset($fromdate)) $fromdate = date('Y-m-d');
        if (!isset($todate)) $todate = date('Y-m-d');
        $models = Essay::find()
            ->where('deleted=0 and date(received_date) between :fromdate and :todate', [
                ':fromdate' => $fromdate,
                ':todate' => $todate
            ])
            ->andWhere(['status_id' => 2])
            ->all();
        $title = 'ສະຖິຕິ ບົດຄວາມ ຄ້າງນຳ ບກ';
        return $this->render('list', [
            'title' => $title,
            'models' => $models
        ]);
    }

    public function actionReturn($fromdate = null, $todate = null)
    {
        if (!isset($fromdate)) $fromdate = date('Y-m-d');
        if (!isset($todate)) $todate = date('Y-m-d');
        $models = Essay::find()
            ->where('deleted=0 and date(received_date) between :fromdate and :todate', [
                ':fromdate' => $fromdate,
                ':todate' => $todate
            ])
            ->andWhere(['status_id' => 3])
            ->all();
        $title = 'ສະຖິຕິ ບົດຄວາມ ສົ່ງຄືນເຈົ້າຂອງປັບປຸງ';
        return $this->render('list', [
            'title' => $title,
            'models' => $models
        ]);
    }

    public function actionPublish($fromdate = null, $todate = null)
    {
        if (!isset($fromdate)) $fromdate = date('Y-m-d');
        if (!isset($todate)) $todate = date('Y-m-d');
        $models = Essay::find()
            ->where('deleted=0 and date(received_date) between :fromdate and :todate', [
                ':fromdate' => $fromdate,
                ':todate' => $todate
            ])
            ->andWhere(['status_id' => 5])
            ->all();
        $title = 'ສະຖິຕິ ບົດຄວາມ ສາມາດລົງໄດ້';
        return $this->render('list', [
            'title' => $title,
            'models' => $models
        ]);
    }

    public function actionMale($fromdate = null, $todate = null)
    {
        if (!isset($fromdate)) $fromdate = date('Y-m-d');
        if (!isset($todate)) $todate = date('Y-m-d');
        $models = Essay::find()
            ->where('deleted=0 and date(received_date) between :fromdate and :todate', [
                ':fromdate' => $fromdate,
                ':todate' => $todate
            ])
            ->andWhere(['gender' => 'M'])
            ->all();
        $title = 'ສະຖິຕິ ບົດຄວາມ ຂອງເພດຊາຍ';
        return $this->render('list', [
            'title' => $title,
            'models' => $models
        ]);
    }

    public function actionFemale($fromdate = null, $todate = null)
    {
        if (!isset($fromdate)) $fromdate = date('Y-m-d');
        if (!isset($todate)) $todate = date('Y-m-d');
        $models = Essay::find()
            ->where('date(received_date) between :fromdate and :todate', [
                ':fromdate' => $fromdate,
                ':todate' => $todate
            ])
            ->andWhere(['gender' => 'F'])
            ->all();
        $title = 'ສະຖິຕິ ບົດຄວາມ ຂອງເພດຍິງ';
        return $this->render('list', [
            'title' => $title,
            'models' => $models
        ]);
    }

    public function actionUser($id, $fromdate = null, $todate = null)
    {
        $employee = Employee::find()->where(['id' => $id, 'deleted' => 0])->one();
        if (!isset($employee)) {
            Yii::$app->session->setFlash('danger', 'ລະຫັດ ບກ ບໍ່ຖືກຕ້ອງ');
            return $this->redirect(['statistic/index']);
        }
        if (!isset($fromdate)) $fromdate = date('Y-m-d');
        if (!isset($todate)) $todate = date('Y-m-d');
        $models = History::find()->alias('h')
            ->join('join', 'essay e', 'e.id = h.essay_id and e.deleted=0')
            ->join('join', 'employee m', 'm.id = h.employee_id and m.deleted=0')
            ->where("h.status_id in (:s1,:s2) and h.deleted=:deleted and h.sr=:sr", [
                ':s1' => 2,
                ':s2' => 4,
                ':deleted' => 0,
                ':sr' => 'R'
            ])
            ->andWhere("date(h.created_date) between :fromdate and :todate", [
                ':fromdate' => $fromdate,
                ':todate' => $todate,
            ])
            ->andWhere(['h.employee_id' => $employee->id])
            ->all();
        $title = 'ສະຖິຕິ ບັນນາ ຂອງ ' . $employee->name;
        return $this->render('listbyuser', [
            'title' => $title,
            'models' => $models
        ]);
    }


    public function actionCategory($id, $fromdate = null, $todate = null)
    {
        $category = Category::find()->where(['id' => $id])->one();
        if (!isset($category)) {
            Yii::$app->session->setFlash('danger', 'ປະເພດບົດຄວາມ ບໍ່ຖືກຕ້ອງ');
            return $this->redirect(['statistic/index']);
        }
        if (!isset($fromdate)) $fromdate = date('Y-m-d');
        if (!isset($todate)) $todate = date('Y-m-d');
        $models = Essay::find()
            ->where('date(created_date) between :fromdate and :todate', [
                ':fromdate' => $fromdate,
                ':todate' => $todate
            ])
            ->andWhere(['category_id' => $id])
            ->andWhere(['deleted' => 0])
            ->all();
        $title = 'ສະຖິຕິ ບົດຄວາມ ປະເພດ ' . $category->name;
        return $this->render('list', [
            'title' => $title,
            'models' => $models
        ]);
    }

    public function actionAdjust($fromdate, $todate)
    {
        if (!isset($fromdate)) $fromdate = date('Y-m-d');
        if (!isset($todate)) $todate = date('Y-m-d');
        $models = Yii::$app->db->createCommand('select 
            e.id
            , e.title
            , count(h.id) count
        from essay e
        join history h on e.id = h.essay_id
        where date(h.created_date) between :fromdate and :todate
        and h.status_id = 3 and lower(sr) = :sr
        group by h.essay_id order by 3 desc', [
            ':fromdate' => $fromdate,
            ':todate' => $todate,
            ':sr' => 'r'
        ])->queryAll();

        $title = 'ສະຖິຕິ ບົດຄວາມ ສົ່ງຄືນປັບປຸງ';
        return $this->render('listreturn', [
            'title' => $title,
            'models' => $models
        ]);
    }

    public function actionSection($id)
    {
        $section = Section::find()->where(['id' => $id])->one();
        if (!isset($section))
            throw new NotFoundHttpException('The requested page does not exist.');

        if (isset($section->sectionSubs) && count($section->sectionSubs)) {
            $models = Yii::$app->db->createCommand('select 
            s.id
            , s.name
            , count(e.id) count
        from section_sub s
        left join essay e on e.section_sub_id = s.id
        where e.section_id = :id and e.deleted=0
        group by s.id', [
                ':id' => $section->id
            ])
                ->queryAll();
        } else {
            $models = Yii::$app->db->createCommand('select 
            place id,
            place name,
            count(id) count
        from essay
        where source=:source and deleted=0
        group by place', [
                ':source' => $section->name
            ])
                ->queryAll();
        }
        $genders = Yii::$app->db->createCommand('select 
            gender
            , count(id) count
        from essay
        where section_id = :id and deleted=0
        group by gender order by gender', [
            ':id' => $section->id
        ])
            ->queryAll();
        $title = 'ສະຖິຕິ ບົດຄວາມ ພາກສ່ວນ';
        return $this->render('listsection', [
            'section' => $section,
            'title' => $title,
            'models' => $models,
            'genders' => $genders
        ]);
    }

    public function actionSectionsub($id)
    {
        $sectionsub = SectionSub::find()->where('id=:id or name=:id', [':id' => $id])->one();
        if (isset($sectionsub)) {
            $models = Essay::find()->where(['deleted' => 0, 'section_sub_id' => $sectionsub->id])->all();
            $title = 'ສະຖິຕິ ບົດຄວາມ ກົມກອງ ' . $sectionsub->name;
        } else {
            $models = Essay::find()->where(['deleted' => 0, 'place' => $id])->all();
            $title = 'ສະຖິຕິ ບົດຄວາມ ກົມກອງ ' . $id;
        }
        $genders = Yii::$app->db->createCommand('select 
            gender
            , count(id) count
        from essay
        where (section_sub_id = :id or place=:id) and deleted=0
        group by gender order by gender', [
            ':id' => $id
        ])
            ->queryAll();
        $title .= ' - ຈຳນວນທັງໝົດ ' . number_format(count($models)) . ' ບົດ';

        return $this->render('listsectionsub', [
            'title' => $title,
            'models' => $models,
            'genders' => $genders
        ]);
    }
}
