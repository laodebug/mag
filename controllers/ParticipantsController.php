<?php

namespace app\controllers;

use app\models\Meeting;
use app\models\Participants;
use app\models\ParticipantsSearch;
use app\models\Student;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ParticipantsController implements the CRUD actions for Participants model.
 */
class ParticipantsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST']
                ],
            ],
        ];
    }

    /**
     * Lists all Participants models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ParticipantsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Participants model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Participants model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Participants the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Participants::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new Participants model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Participants();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Participants model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Participants model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->updateAttributes(['deleted' => 1]);

        return $this->redirect(['index']);
    }

    public function actionGet($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->get($id);
    }

    private function get($id)
    {
        $meeting = Meeting::find()->where(['id' => $id])->one();
        if (!isset($meeting))
            return null;

        $models = Participants::find()
            ->alias('p')
            ->select("s.code, s.firstname, s.lastname")
            ->addSelect([
                'checkin' => "date_format(p.checkin, '%H:%i:%s')",
                'checkout' => "date_format(p.checkout, '%H:%i:%s')",
            ])
            ->join('join', 'student s', 's.id = p.student_id and s.deleted=0')
            ->where(['p.meeting_id' => $id, 'p.deleted' => 0])
            ->orderBy('checkin desc')
            ->asArray()
            ->all();
        return $models;
    }

    public function actionJoin($id, $code)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $this->enableCsrfValidation = false;
        $meeting = Meeting::find()->where(['id' => $id])->one();
        $student = [];
        if (isset($meeting)) {
            $student = Student::find()->where(['code' => $code, 'deleted' => 0])->asArray()->one();
            if (isset($student)) {
                $model = Participants::find()->where(['meeting_id' => $id, 'student_id' => $student['id']])->one();
                if (!isset($model)) {
                    $model = new Participants();
                    $model->meeting_id = $id;
                    $model->deleted = 0;
                    $model->student_id = $student['id'];
                    $model->checkin = date('Y-m-d H:i:s');
                    $student['checkin'] = $model->checkin;
                } else {
                    $model->checkout = date('Y-m-d H:i:s');
                    $student['checkout'] = $model->checkout;
                    $student['checkin'] = $model->checkin;
                }
                $model->save();
            }
        }
        return [
            'student' => $student,
            'students' => $this->get($id)
        ];
    }
}
