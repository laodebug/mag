<?php

return [
    'adminEmail' => 'admin@example.com',
    'sliderheight' => '400px',
    'roles' => [
        "ທົ່ວໄປ" => "ທົ່ວໄປ",
        "ກອງເລຂາ" => "ກອງເລຂາ",
        "ຫົວໜ້າ" => "ຫົວໜ້າ",
        "ຜູ້ດູແລລະບົບ" => "ຜູ້ດູແລລະບົບ",
    ],
    'ckoptions' => [
        'kcfinder' => true,
        'kcfOptions' => [
            'uploadURL' => '@web/upload',
            'uploadDir' => '@webroot/upload',
            'access' => [  // @link http://kcfinder.sunhater.com/install#_access
                'files' => [
                    'upload' => true,
                    'delete' => true,
                    'copy' => true,
                    'move' => true,
                    'rename' => true,
                ],
                'dirs' => [
                    'create' => true,
                    'delete' => true,
                    'rename' => true,
                ],
            ],
            'types' => [  // @link http://kcfinder.sunhater.com/install#_types
                'files' => [
                    'type' => '',
                ],
            ],
        ],
    ]
];
